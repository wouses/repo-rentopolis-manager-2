<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
  
   /**
   * startRestApi()
   *
   * Initialize the json header and and returns an array representation of a json format. 
   * If specified, a success initial value will be assigned to the corresponding json attribute.
   */
  protected function startRestApi($success = false){
    header('Content-Type: application/json;charset=utf-8');
    $json = array("success" => $success, "result" => array());
    return $json;
  }
  
  /**
   * endRestApi()
   *
   * Outputs the encoded json given parameter and terminates the Yii application. 
   */
  protected function endRestApi($json){
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }
  
  public static function initExcelLibrary($writer = "Excel2007.php"){
    spl_autoload_unregister(array('YiiBase','autoload'));
    require_once(dirname(__FILE__) . "/../extensions/libs/phpexcel/Classes/PHPExcel.php");
    require_once(PHPEXCEL_COMPONENTS_ROOT . "Writer/" . $writer);
    spl_autoload_register(array('YiiBase','autoload'));
  }
  
  /**
   * GitHub reference link: https://github.com/tazotodua/useful-php-scripts/
   * FOLLOWLOCATION problem and Remote urls are automatically re-corrected
   *
   * I modified the input parameters, it now handle the array form for the get and post parameters.
   */
  protected function getRemoteData($url, $post_paramtrs = false, $get_paramtrs = false) {
    $c = curl_init();
    
    if($post_paramtrs !== false) $post_paramtrs = http_build_query($post_paramtrs);
    if($get_paramtrs !== false) $url .= '?'.http_build_query($get_paramtrs);
    
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    if ($post_paramtrs) {
      curl_setopt($c, CURLOPT_POST, TRUE);
      curl_setopt($c, CURLOPT_POSTFIELDS, $post_paramtrs);
    }
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
    curl_setopt($c, CURLOPT_COOKIE, 'CookieName1=Value;');
    curl_setopt($c, CURLOPT_MAXREDIRS, 10);
    $follow_allowed = (ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
    if ($follow_allowed) {
      curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
    }
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
    curl_setopt($c, CURLOPT_REFERER, $url);
    curl_setopt($c, CURLOPT_TIMEOUT, 60);
    curl_setopt($c, CURLOPT_AUTOREFERER, true);
    curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
    $data = curl_exec($c);
    $status = curl_getinfo($c);
    curl_close($c);
    preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si', $status['url'], $link);
    $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si', '$1=$2'.$link[0].
                         '$3$4$5', $data);
    $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si', '$1=$2'.$link[1].
                         '://'.$link[3].
                         '$3$4$5', $data);
    if ($status['http_code'] == 200) {
      return $data;
    }
    elseif($status['http_code'] == 301 || $status['http_code'] == 302) {
      if (!$follow_allowed) {
        if (empty($redirURL)) {
          if (!empty($status['redirect_url'])) {
            $redirURL = $status['redirect_url'];
          }
        }
        if (empty($redirURL)) {
          preg_match('/(Location:|URI:)(.*?)(\r|\n)/si', $data, $m);
          if (!empty($m[2])) {
            $redirURL = $m[2];
          }
        }
        if (empty($redirURL)) {
          preg_match('/href\=\"(.*?)\"(.*?)here\<\/a\>/si', $data, $m);
          if (!empty($m[1])) {
            $redirURL = $m[1];
          }
        }
        if (!empty($redirURL)) {
          $t = debug_backtrace();
          return call_user_func($t[0]["function"], trim($redirURL), $post_paramtrs);
        }
      }
    }
    return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:".json_encode($status).
      "<br/><br/>Last data got<br/>:$data";
  }
  
}