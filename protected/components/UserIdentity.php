<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity{

	private $_attribute = "username";

	public function authenticate($withoutPassword = false){	
		$user = User::model()->findByAttributes(array('username' => $this->username));
		
		if($user===null){
			$this->errorCode=self::ERROR_USERNAME_INVALID;
    }else if(!$user->verifyPassword($this->password, $user->password) && !$withoutPassword){
			$this->errorCode=self::ERROR_PASSWORD_INVALID;		
		}else{
			/**************
			  From what I've seen from Yii behavior,
			  after each login only initialised properties can be stored in cookie.
			  These informations are useful in the auto-login context.
			**************/
			$this->setState('lastLogIn', '');
			$this->setState('token', '');
			
			$this->errorCode=self::ERROR_NONE;
		}
		
		return !$this->errorCode;
	}
	
	/*public function getId(){
		return $this->id;
	}*/
	
	public function setAttribute($value){
		$this->_attribute = $value;
	}
}