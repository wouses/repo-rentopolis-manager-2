<?php
/* #TODOREF.
 * the most part of actions in the following controller are API Rest, create a template pattern wich make the charset initialization
 * and the application termination.
 */
class AnnouncementController extends Controller{
  
	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

  /* 
   * Main action in the controller.
   */

	public function actionIndex(){
		$this->render('index');
	}

  public function actionTestImport(){
    /*$beds24 = new Beds24();
    $beds24->importAnnouncements();*/
  }

  public function actionFixPropertyOwner(){
    //SELECT * FROM `AnnouncementSettings` WHERE id_field = 355
    $json = parent::startRestApi();
    $result = array();

    $settings = AnnouncementSettings::model()->findAllByAttributes(array("id_field" => 355));
    foreach($settings as $setting){
      $host = $setting->announcement()->host();
      $fullName = $host->getFullName();
      $setting->value = $fullName;
      $setting->isValid = 1;
      $setting->update();
      $result[] = $setting->attributes;
    }

    $json["success"] = true;
    $json["result"] = $result;
    echo CJavaScript::jsonEncode($json);
    Yii::app()->end();
  }

  /*
   * GET ALL BUSINESS ANNOUNCEMENTS
   *
   * API Rest Link: /announcement/announcements
   * Output: JSON
   *
   * The only parameter needed is the business code, retrived from the session.
   */

  public function actionBusinessAnnouncements(){
    header('Content-Type: application/json;charset=utf-8');
    $json = array("success" => false, "result" => array());
    $business = 1;

    $criteria = new CDbCriteria();
    $criteria->with = array("address");
    $criteria->condition = "id_business = :business";
    $criteria->params = array(":business" => $business);
    $criteria->order = "title ASC";

    $results = Announcement::model()->findAll($criteria);
    if(!empty($results)) $json["success"] = true;

    foreach($results as $result){
      $announcement = $result->attributes;
      $address = $result->address();
      if(!empty($address)){
        $announcement["address"] = $address->attributes;
      }else{
        $announcement["address"]["street"] = "";
        $announcement["address"]["houseNumber"] = "";
        $announcement["address"]["city"] = "";
        $announcement["address"]["zip"] = "";
        $announcement["address"]["state"] = "";
        $announcement["address"]["country"] = "";
      }
      $json["result"][] = $announcement;
    }
    
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }
  
  public function actionChannelManagersConfiguration(){
    $json = parent::startRestApi();
    $listing = isset($_GET["listing"]) ? $_GET["listing"] : null ;
    
    $configuration = self::getAnnouncementChannelManagerConfiguration($listing);
    
    $json["result"] = $configuration;
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }
  
  public function actionSetChannelManagersConfiguration(){
    $json = parent::startRestApi();
    $listing = isset($_GET["listing"]) ? $_GET["listing"] : null ;
    $configuration = isset($_POST["configuration"]) ? json_decode($_POST["configuration"]) : array() ;
    $result = array();
    
    foreach($configuration as $cfgChannelManager){
      $channelManager = $cfgChannelManager->name;
      $isActive = $cfgChannelManager->activated;
      
      if(intval($isActive) == 0){
        /*clear database*/
        AnnouncementChannelManager::model()->deleteAllByAttributes(array(
          "id_channelManager" => $cfgChannelManager->channelManagerId,
          "id_announcement" => $cfgChannelManager->propertyId
        ));
      }else if(intval($isActive) == 1){
        /**/
        //$result[] = "$channelManager is $isActive";
        $associations = AnnouncementChannelManager::model()->findAllByAttributes(array(
          "id_channelManager"=>$cfgChannelManager->channelManagerId,
          "id_announcement" => $cfgChannelManager->propertyId
        ));
        
        if(count($associations)>0) continue;
        
        $channelManager = ChannelManager::model()->findByAttributes(array(
          "id_channelManager" => $cfgChannelManager->channelManagerId
        ));
        
        if(!empty($channelManager->getAttribute("class"))){
          $code = $channelManager->id_channelManager;
          $class = $channelManager->getAttribute("class");
          $instanceId = $channelManager->getAttribute("id_channelManager");
          
          $announ = Announcement::model()->findByAttributes(array("id_announcement" => $listing));
          $title = $announ->title;
          $remoteChm = new $class($instanceId);
          
          if(empty($announ->wubook_lcode))
            $remoteAnnouncements = $remoteChm->retrieveAnnouncement($title);
          else
            $remoteAnnouncements = $remoteChm->retrieveAnnouncement($title, array("lcode" => $announ->wubook_lcode));
          
          foreach($remoteAnnouncements as $remote){
            $remoteId = $remote->id;
            $association = new AnnouncementChannelManager();
            $association->id_channelManager = $channelManager->id_channelManager;
            $association->id_announcement = $listing;
            $association->alias = $remote->id;
            $association->aliasDescription = $remote->title;
            $association->save();
          }
        }
      }
    }
    
    $json["success"] = true;
    $json["result"] = $result;
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }

  /*
   * GET ALL HOMEOWNER ANNOUNCEMENT
   *
   * API Rest Link: /announcement/homeowner
   * Output: JSON
   *
   * The only parameter needed is the business code, retrived from the session.
   */

  public function actionHomeowner(){
    $json = parent::startRestApi();

    $user = User::model()->findByAttributes(array("username" => Yii::app()->user->id));
    $bp = $user->businessPartner();
    
    $criteria = new CDbCriteria();
    $criteria->condition = "id_host = :host";
    $criteria->params = array(":host" => $bp->id_businessPartner);
    $criteria->order = "t.title ASC";
    
    $announcements = Announcement::model()->findAll($criteria);
    //$announcements = Announcement::model()->findAllByAttributes(array("id_host" => $bp->id_businessPartner));

    foreach($announcements as $announcement){
      $json["result"][] = $announcement->attributes;
    }

    parent::endRestApi($json);
  }
  
  /*
   * GET ALL RESERVATIONS FROM AN ANNOUNCEMENT. 
   * the system check that the announcement business code matches with the current user business code.
   * If specified, the system retrives all reservations with a checkin inside in the following ranges
   *
   * - [from, to]
   * - [from, undefined)
   * - (undefined, to]
   *
   * Another optional parameter, withIn, can be used to specify that the reservation must be inside the rage.
   * So in this case the following rule must be respected: from <= checkIn <= checkOut <= to.
   * The checkIn <= checkOut is implicit, only the Model check this rule.
   *
   * API Rest Link: /announcement/<listing>/reservations?<from>&<to>&<withIn>
   * Output: JSON
   *
   * Required Parameters:
   * - listing code, GET METHOD
   *
   * Optional Parameters:
   * - from date, GET METHOD, check in date
   * - to date, GET METHOD, check out date
   */
  public function actionReservations(){
    header('Content-Type: application/json;charset=utf-8');
    $json = array("success" => false, "result" => array());
    $business = 2;
    
    $listing = isset($_GET["listing"]) ? $_GET["listing"] : null ;
    
    $from = isset($_GET["from"]) ? $_GET["from"] : null ;
    $to =  isset($_GET["to"]) ? $_GET["to"] : null ;
    $withIn =  isset($_GET["withIn"]) && $_GET["withIn"] == "1" ? true : false ;
    
    $criteria = new CDbCriteria();
    $criteria->alias = "t";
    $criteria->with = array(
      "announcement" => array("alias" => "a"), 
      "announcement.business" => array("alias" => "b")
    );
    $criteria->condition = "b.id_business = :business AND t.id_announcement = :listing";
    $criteria->params = array(":business" => $business, ":listing" => $listing);
    
    if($from !== null){
      $criteria->condition .= " AND checkInDate >= :from";
      $criteria->params[":from"] = $from;
    }
    
    if($to !== null){
      if($from !== null && $withIn){
        $criteria->condition .= " AND checkOutDate <= :to";
        $criteria->params[":to"] = $to;
      }else{
        $criteria->condition .= " AND checkInDate <= :to";
        $criteria->params[":to"] = $to; 
      }
    }

    $results = Reservation::model()->findAll($criteria);
    
    if(!empty($results)) $json["success"] = true;
    foreach($results as $result){
      $json["result"] = $result->attributes;
    }
    
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }
  
  /*
   * GET ALL CONTRACT COMMISSION FROM AN ANNOUNCEMENT. 
   * the system check that the announcement business code matches with the current user business code.
   * If specified, the system retrieves the contract close to a date with the fromValidityDate prior or equal to the requested date.
   *
   * API Rest Link: /announcement/<listing>/announcementContracts
   * Output: JSON
   *
   * Required Parameters:
   * - listing code, GET METHOD
   *
   * Optional Parameters:
   * - from date, GET METHOD, validity check for the contract
   */
  public function actionAnnouncementContracts(){
    header('Content-Type: application/json;charset=utf-8');
    $json = array("success" => false, "result" => array());
    $business = 2;
    
    $listing = isset($_GET["listing"]) ? $_GET["listing"] : null ;
    $from = isset($_GET["from"]) ? $_GET["from"] : null ;
    
    $result = self::getAnnouncementContracts($listing, $from);
    
    if($result){
      $json["success"] = true;
      $json["result"] = $result->attributes;
      $contracts = $result->contracts();
      $n = 0;
      foreach($contracts as $contract){
        if($from !== null && $n > 0) break;
        $json["result"]["contracts"][] = $contract->attributes;
        $n++;
        $commissions = $contract->commissions();
        foreach($commissions as $commission){
          $data = $commission->attributes;
          $data["serviceName"] = $commission->service()->name;
          $json["result"]["contracts"][$n-1]["commissions"][] = $data;
        }
      } 
    }
 
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }

  /*
   * CREATE AN ADDRESS FOR A SPECIFIED ANNOUNCEMENT
   * 
   * Update an address for an announcement specified with a given listing code.
   * If the Announcement doesn't has an address yet, it will be created.
   *
   * URL: /announcement/<listing>/updateAddress
   *
   * Required Parameters:
   * - listing code, GET METHOD
   */
  
  public static function actionUpdateAddress($listing, $from = null){
    header('Content-Type: application/json;charset=utf-8');
    $json = array("success" => false);
    $business = 1;
    
    $listing = isset($_GET["listing"]) ? $_GET["listing"] : null;
    
    if(!empty($listing)){
      
      $street = isset($_POST["street"]) ? $_POST["street"] : "";
      $houseNumber = isset($_POST["houseNumber"]) ? $_POST["houseNumber"] : "";
      $city = isset($_POST["city"]) ? $_POST["city"] : "";
      $zip = isset($_POST["zip"]) ? $_POST["zip"] : "";
      $state = isset($_POST["state"]) ? $_POST["state"] : "";
      $country = isset($_POST["country"]) ? $_POST["country"] : "";
      $directions = isset($_POST["directions"]) ? $_POST["directions"] : "";

      $criteria = new CDbCriteria();
      $criteria->with = array("address");
      $criteria->condition = "id_announcement = :listing AND id_business = :business";
      $criteria->params = array(":listing" => $listing, ":business" => $business);

      $announcement = Announcement::model()->find($criteria);
      $address = $announcement->address();
      if(empty($address)){
        $address = new Address();
      }

      $address->street = $street;
      $address->houseNumber = $houseNumber;
      $address->city = $city;
      $address->zip = $zip;
      $address->state = $state;
      $address->country = $country;
      $address->directions = $directions;
      
      $new = false;
      if($address->isNewRecord)
        $new = true;

      if($address->save()){
        $json["success"] = true;
        if($new){
          $announcement->id_address = $address->id_address;
          if(!$announcement->save()){
            $address->delete();
            $json["success"] = false;
            $json["error"] = "An error occurred during announcement updating";
          }
        }
      }else{
        $json["success"] = false;
        $json["error"] = "An error occurred during address updating";
      }
    }
    
    if($json["success"]){
      $json["result"] = $address->attributes;
    }
    
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }
  
  public function actionGetSettingsLayoutForm(){
    $json = parent::startRestApi();
    
    $formFields = array();
    $criteria = new CDbCriteria();
    $criteria->order = "pos ASC";
    $fields = AnnouncementSettingsFormBuilder::model()->findAll($criteria);
    foreach($fields as $field){
      $formFields[] = $field->attributes;
    }
    
    $json["result"] = $formFields;
    $json["success"] = true;
    parent::endRestApi($json);
  }
  
  public function actionUpdateSettingsLayoutForm(){
    $json = parent::startRestApi();
    
    if(!isset($_POST["formLayout"])) parent::endRestApi($json);
    if(!isset($_POST["toCancel"])) parent::endRestApi($json);
    
    $layout = json_decode($_POST["formLayout"]);
    $toCancel = json_decode($_POST["toCancel"]);

    foreach($toCancel as $field){
      if(isset($field->id_formField))
        AnnouncementSettingsFormBuilder::model()->deleteAllByAttributes(array("id_formField"=>$field->id_formField));
    }

    $i=0;
    foreach($layout as $field){
      if(isset($field->id_formField))
        $formBuilderField = AnnouncementSettingsFormBuilder::model()->findByAttributes(array("id_formField" => $field->id_formField));
      else
        $formBuilderField = new AnnouncementSettingsFormBuilder();
      
      $formBuilderField->label = $field->label;
      $formBuilderField->name = $field->name;
      $formBuilderField->description = $field->description;
      $formBuilderField->pos = $i;
      $formBuilderField->save();
      $i++;
    }
    
    $json["success"] = true;
    parent::endRestApi($json);
  }
  
  public function actionGetAnnouncementSettings(){
    $json = parent::startRestApi();

    if(!isset($_GET["listing"])) parent::endRestApi($json);
    
    $listing = $_GET["listing"];
    //$announcement = Announcement::model()->findByAttributes(array("id_announcement" => $listing));
    //$fields = $announcement->settingsFields();
    $criteria = new CDbCriteria();
    $criteria->order = "t.pos ASC";
    $fields = AnnouncementSettingsFormBuilder::model()->findAll($criteria);
    
    $result = array();
    foreach($fields as $field){
      $fieldStructure = $field->attributes;
      $instance = AnnouncementSettings::model()->findByAttributes(array(
        "id_field" => $field->id_formField,
        "id_announcement" => $listing
      ));
      
      if(!empty($instance))
        $tmp = array_merge($fieldStructure, $instance->attributes);
      else
        $tmp = array_merge($fieldStructure, array("value" => "", "isValid" => 0));
      
      $tmp["isValid"] = intval($tmp["isValid"]) == 1;
      $result[] = $tmp;
    }
    
    $json["result"] = $result;
    $json["success"] = true;
    parent::endRestApi($json);
  }

  public function actionUpdateAnnouncementSettings(){
    $json = parent::startRestApi();

    if(!isset($_GET["listing"])) parent::endRestApi($json);
    if(!isset($_POST["settings"])) parent::endRestApi($json);

    $listing = $_GET["listing"];
    $settings = json_decode($_POST["settings"]);
    AnnouncementSettings::model()->deleteAllByAttributes(array("id_announcement" => $listing));
    
    $instances = array();
    foreach($settings as $settingField){
      $instance = new AnnouncementSettings();
      $instance->id_field = $settingField->id_formField;
      $instance->id_announcement = $listing;
      $instance->value = $settingField->value;
      $instance->isValid = intval($settingField->isValid);
      
      $instance->save();
      
      $instances[] = $instance;
    }

    $json["success"] = true;
    parent::endRestApi($json);
  }

  public function actionGetAnnouncementChannelRules(){
    $json = parent::startRestApi();
    
    if(!isset($_GET["listing"])) parent::endRestApi($json);
    $listing = $_GET["listing"];
    $announcement = Announcement::model()->findByAttributes(array("id_announcement" => $listing))->attributes;
    
    $criteria = new CDbCriteria();
    $criteria->condition = "t.id_announcement = :listing";
    $criteria->params = array(":listing" => $listing);
    $criteria->order = "validFrom DESC";
    
    $rs = AnnouncementChannelRules::model()->findAll($criteria);
    $rules = array();
    foreach($rs as $rule){
      $rules[] = $rule->attributes;
    }
    
    $announcement["rules"] = $rules;
    
    $json["result"] = $announcement;
    $json["success"] = true;
    parent::endRestApi($json);
  }
  
  public function actionDeleteLastAnnouncementChannelRule(){
    $json = parent::startRestApi();
    
    if(!isset($_GET["listing"])) parent::endRestApi($json);
    $listing = $_GET["listing"];
    
    $criteria = new CDbCriteria();
    $criteria->condition = "t.id_announcement = :listing";
    $criteria->params = array(":listing" => $listing);
    $criteria->order = "validFrom DESC";
    
    $rs = AnnouncementChannelRules::model()->findAll($criteria);
    $rs[0]->delete();
    
    $json["success"] = true;
    parent::endRestApi($json);
  }
  
  public function actionSaveLastAnnouncementChannelRule(){
    $json = parent::startRestApi();
    
    if(!isset($_GET["listing"])) parent::endRestApi($json);
    if(!isset($_POST["newRule"])) parent::endRestApi($json);
    
    $listing = $_GET["listing"];
    $announcementChannelRule = json_decode($_POST["newRule"]);
    
    $acr = new AnnouncementChannelRules();
    $acr->id_announcement = $listing;
    $acr->cleaning = $announcementChannelRule->cleaning;
    $acr->expediaCleaning = $announcementChannelRule->expediaCleaning;
    $acr->bookingcomCommission = $announcementChannelRule->bookingcomCommission;
    $acr->rentopolisCommission = $announcementChannelRule->rentopolisCommission;
    $acr->validFrom = $announcementChannelRule->validFrom;
    
    if(!$acr->save()) parent::endRestApi($json);
    
    $json["result"] = $acr;
    $json["success"] = true;
    parent::endRestApi($json);
  }

  /*
   * getAnnouncementContracts(integer id)
   *
   * id: announcement Id.
   *
   * Lazy loading method, with this static operation we can obtain all announcement contract
   * in one query.
   *
   */

  public static function getAnnouncementContracts($listing, $from = null){
    if(empty($listing)) return null;
    
    $criteria = new CDbCriteria();
    $criteria->alias = "t";
    $criteria->condition = "t.id_announcement = :listing";
    $criteria->with = array(
        "contracts" => array("alias" => "c"),
        "contracts.commissions",
        "contracts.commissions.service"
    );
    $criteria->params = array(":listing" => $listing);
    $criteria->order = "fromValidityDate DESC";
    
    if($from !== null){
      $criteria->condition .= " AND c.fromValidityDate <= :from";
      $criteria->params[":from"] = $from;
    }
    
    $result = Announcement::model()->find($criteria);
    return $result;
  }
  
  /*  getAnnouncementChannelManagerConfiguration(listing)
   *
   *  Get Announcement Channel Manager configuration
   */
  
  public static function getAnnouncementChannelManagerConfiguration($listing){
    $configuration = array();
    $channelManagers = ChannelManager::model()->findAll();
    foreach($channelManagers as $channelManager){
      $channelManagerId = $channelManager->id_channelManager;
      $found = AnnouncementChannelManager::model()->findAllByAttributes(array(
          "id_channelManager" => $channelManagerId,
          "id_announcement" => $listing
      ));
      $configuration[] = array(
        "channelManagerId" => $channelManager->id_channelManager,
        "propertyId" => $listing,
        
        
        "className" => $channelManager->getAttribute("class"),
        "name" => $channelManager->name,
        "activated" => count($found) > 0,
        "disabled" => empty($channelManager->getAttribute("class")),
        "hasClass" => !empty($channelManager->getAttribute("class")),
      );
    }
    return $configuration;
  }
  
}