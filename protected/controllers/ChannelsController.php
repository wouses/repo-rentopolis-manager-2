<?php

class ChannelsController extends Controller{
  
	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

  /* 
   * Main action in the controller.
   */

	public function actionIndex(){
		$this->render('index');
	}
  
  public function actionAll(){
    $json = parent::startRestApi();
    
    $tblReservation = Reservation::model()->tableName();
    $tblCMChannel = ChannelManagerChannelAssociation::model()->tableName();
    $tblChannel = Channel::model()->tableName();
    //$json["results"] = array();
    
    //get all channels in order to appearance in confirmed reservation
    $queryChannelsReservations = "
      SELECT `proj`.id, `proj`.channelName, `proj`.total, count(`ChannelRules`.id_channel) as `rulesNumber`, `proj`.image 
            FROM `ChannelRules` RIGHT JOIN ( 
              SELECT count(`$tblCMChannel`.`id_channelAssociation`) as `total`, 
                  `$tblChannel`.`channelName` as `channelName`, `$tblChannel`.`id_channel` as `id`, `$tblCMChannel`.`referement`,
                  $tblChannel.`image` as `image`
              FROM `$tblReservation` 
                JOIN `$tblCMChannel` ON `$tblReservation`.id_channel = `$tblCMChannel`.id_channelAssociation 
                JOIN `$tblChannel` ON `$tblCMChannel`.`id_channel` = `$tblChannel`.`id_channel` 
              WHERE `$tblReservation`.`id_reservationStatus` = 1 AND `$tblReservation`.`isBlockedDates` = 0
              GROUP BY `$tblCMChannel`.`id_channel` ORDER BY `total` DESC ) as `proj` 
          ON `ChannelRules`.id_channel = `proj`.id
      GROUP BY `proj`.id
      ORDER BY `proj`.total DESC";

    /*$queryChannelsReservations = "
      SELECT count($tblCMChannel.`id_channelAssociation`) as `total`, 
            $tblChannel.`channelName` as `channelName`, 
            $tblChannel.`id_channel` as `id`,
            $tblChannel.`image` as `image`
      FROM $tblReservation 
              JOIN $tblCMChannel ON $tblReservation.id_channel = $tblCMChannel.id_channelAssociation 
              JOIN $tblChannel ON $tblCMChannel.`id_channel` = $tblChannel.`id_channel` 
      WHERE $tblReservation.`id_reservationStatus` = 1 AND $tblReservation.`isBlockedDates` = 0
      GROUP BY $tblCMChannel.`id_channel` 
      ORDER BY `total` DESC";*/
    
    $channels = Yii::app()->db->createCommand($queryChannelsReservations)->queryAll();

    foreach($channels as $channel){
      $json["result"][] = array(
        "id" => $channel["id"], 
        "image" => $channel["image"], 
        "channelName" => $channel["channelName"],
        "total" => $channel["total"],
        "rulesNumber" => $channel["rulesNumber"]
      );
    }
    
    /*$queryChannelsNoReservations = "
      SELECT $tblChannel.`id_channel` as `id`, $tblChannel.`channelName` as `channelName`, $tblChannel.`image` as image
      FROM $tblChannel 
      WHERE $tblChannel.`id_channel` NOT IN (
        SELECT $tblChannel.`id_channel` 
        FROM $tblReservation 
                JOIN $tblCMChannel ON $tblReservation.id_channel = $tblCMChannel.id_channelAssociation 
                JOIN $tblChannel ON $tblCMChannel.`id_channel` = $tblChannel.`id_channel` 
        WHERE $tblReservation.`id_reservationStatus` = 1 AND $tblReservation.`isBlockedDates` = 0
    ) ORDER BY $tblChannel.image DESC";*/
    
    $queryChannelsNoReservations = "
      SELECT `proj`.id, `proj`.channelName, count(`ChannelRules`.id_channel) as `rulesNumber`,  `proj`.image
      FROM `ChannelRules` RIGHT JOIN ( 

        SELECT DISTINCT $tblChannel.`id_channel` as `id`, $tblChannel.`channelName` as `channelName`, $tblChannel.`image` as `image`
          FROM $tblChannel 
          WHERE $tblChannel.`id_channel` NOT IN (
            SELECT $tblChannel.`id_channel` 
            FROM $tblReservation 
                    JOIN $tblCMChannel ON Reservation.id_channel = $tblCMChannel.id_channelAssociation 
                    JOIN $tblChannel ON $tblCMChannel.`id_channel` = $tblChannel.`id_channel` 
            WHERE $tblReservation.`id_reservationStatus` = 1 AND $tblReservation.`isBlockedDates` = 0
        ) ORDER BY $tblChannel.image DESC) as `proj` 

        ON `ChannelRules`.id_channel = `proj`.id
    GROUP BY `proj`.id
    ORDER BY `rulesNumber` DESC";
    
    $channels = Yii::app()->db->createCommand($queryChannelsNoReservations)->queryAll();

    foreach($channels as $channel){
      $json["result"][] = array(
        "id" => $channel["id"], 
        "image" => $channel["image"], 
        "channelName" => $channel["channelName"],
        "total" => 0,
        "rulesNumber" => $channel["rulesNumber"]
      );
    }


    $json["success"] = true;
    parent::endRestApi($json);
  }

  public function actionSaveChannelRule(){
    $json = parent::startRestApi();

    $channelId = isset($_GET["channelId"]) ? $_GET["channelId"] : null;
    $channel = isset($_POST["channel"]) ? json_decode($_POST["channel"]) : null;

    if(empty($channelId) || empty($channel))
      parent::endRestApi($json);

    $newRule = $channel->newRule;
    
    $channelRules = new ChannelRules();
    $channelRules->id_channel = $channel->id;
    $channelRules->cleaningIncluded = intval($newRule->cleaningIncluded);
    $channelRules->channelCommissionIncluded = intval($newRule->channelCommissionIncluded);
    $channelRules->defaultCommission = intval($newRule->defaultCommission);
    $channelRules->validFrom = $newRule->validFrom;

    if(!$channelRules->save()){
      $json["result"] = $channelRules->getErrors();
      parent::endRestApi($json);
    }
    
    $json["result"] = $channelRules->attributes;
    $json["success"] = true;
    parent::endRestApi($json);
  }

  public function actionRules(){
    $json = parent::startRestApi();

    $channelId = isset($_GET["channelId"]) ? $_GET["channelId"] : null;
    if(empty($channelId))
      parent::endRestApi($json);

    $criteria = new CDbCriteria();
    $criteria->condition = "t.id_channel = :channelId";
    $criteria->params = array(":channelId" => $channelId);
    $criteria->order = "validFrom DESC";

    $rs = ChannelRules::model()->findAll($criteria);

    $rules = array();
    foreach($rs as $rule){
      $instance = $rule->attributes;
      $instance["cleaningIncluded"] = intval($instance["cleaningIncluded"]) == 1;
      $instance["channelCommissionIncluded"] = intval($instance["channelCommissionIncluded"]) == 1;
      $rules[] = $instance;
    }

    $json["result"] = $rules;
    $json["success"] = true;
    parent::endRestApi($json);
  }

  public function actionDeleteLastRule(){
    $channelId = isset($_GET["channelId"]) ? $_GET["channelId"] : null;
    if(empty($channelId))
      parent::endRestApi($json);

    $criteria = new CDbCriteria();
    $criteria->condition = "t.id_channel = :channelId";
    $criteria->params = array(":channelId" => $channelId);
    $criteria->order = "validFrom DESC";

    $rs = ChannelRules::model()->findAll($criteria);
    if(count($rs) == 0) parent::endRestApi($json);
    $rs[0]->delete();

    $json["success"] = true;
    parent::endRestApi($json);
  }

  public function filters(){
    return array('accessControl');
  }

  public function accessRules(){
    return array(
      array(
        'allow',
        'actions' => array( 'index', 'all', 'rules' ),
        'users'=>array( '*' )
      ),
      array(
        'allow',
        'actions' => array( 'saveChannelRule', 'deleteLastRule'),
        'users'=>array( '@' )
      ),
      array( 'deny' ),
    );
  }
}