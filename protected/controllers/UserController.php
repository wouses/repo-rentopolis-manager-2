<?php

class UserController extends Controller{

	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionIndex(){
    $this->render('index');
	}
  
  public function actionCreateUser(){
    $json = parent::startRestApi();
    
    $sql = "SELECT DISTINCT `bp`.id_businessPartner as `id`, 
                   `bp`.name as `name`, `bp`.surname as `surname`, 
                   `bp`.email as `email` 
            FROM `BusinessPartner` as `bp` 
              JOIN `Announcement` as `ann` ON `bp`.`id_businessPartner` = `ann`.`id_host`";
    $results = Yii::app()->db->createCommand($sql)->queryAll();
    
    $final = array();
    foreach($results as $result){
      $record = $result;
      $record["username"] = str_replace(' ', '', strtolower($record["name"])).".".str_replace(' ', '', strtolower($record["surname"]));
      $record["password"] = '$2y$13$775ZlZ1VQGeVojX3JBk9pe0/L/9LrEXE9G6TPooOWApd1HR2eutze';
        
      $bp = User::model()->findByAttributes(array("id_businessPartner" => $record["id"]));      
      if($bp === null){
        $user = new User();
        $user->id_businessPartner = $record["id"];
        $user->username = $record["username"];
        $user->password = $record["password"];
        $user->approvedByAdministrators = 1;
        $user->email = $record["email"];
        $user->role = "homeowner";
        if(!$user->save())
          $final[] = $user->getErrors();
      }
    }
    
    $json["result"] = $final;
    parent::endRestApi($json);
  }
  
  public function actionLogin(){
    $this->render('login');
  }
  
  public function actionAddr(){
    $json = parent::startRestApi();
    $json["result"] = $_SERVER;
    parent::endRestApi($json);
  }
  
  public function actionSignIn(){
    $json = parent::startRestApi();
        
    $user = new User("signIn");
    $user->username = $_POST["username"];
    $user->password = $_POST["password"];
    
    /*if($user->username !== "marco.rentopolis" && $user->username !== "ezio.galloni"){
      $json["error"] = "Siamo spiacenti, per manutenzione, questa sera il sito è temporaneamente fuori servizio.";
      parent::endRestApi($json);
    }*/
    
    if($user->validate()){
      if($user->login()){
        $json["success"] = true;
        $json["role"] = User::model()->findByAttributes(array("username"=>$user->username))->role;
      }
    }else{
      $json["error"] = json_encode($user->getErrors());
    }
    
    $json["hashed"] = $user->hashPassword($user->password);
    $json["hashed2"] = $user->verifyPassword($user->password, $json["hashed"]);
    
    parent::endRestApi($json);
  }
  
}