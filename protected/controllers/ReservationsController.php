<?php
/* #TODOREF.
 * the most part of actions in the following controller are API Rest, create a template pattern wich make the charset initialization
 * and the application termination.
 */
class ReservationsController extends Controller{
  
	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

  /* 
   * Main action in the controller.
   */

	public function actionIndex(){
		$this->render('index');
	}
  
  /*
   * GET ALL BUSINESS RESERVATIONS
   *
   * Get all reservations corresponding to the current business
   *
   * API Rest Link: /reservation/businessReservations
   * Output: JSON
   *
   * The only parameter needed is the business code, retrived from the session.
   */
  
  public function actionBusinessReservations(){
    header('Content-Type: application/json;charset=utf-8');
    $json = array("success" => false, "result" => array());
    $business = 2;
    
    $criteria = new CDbCriteria();
    $criteria->with = array("announcement");
    $criteria->condition = "id_business = :business";
    $criteria->params = array(":business" => $business);
    
    $results = Reservation::model()->findAll($criteria);
    if(!empty($results)) $json["success"] = true;

    foreach($results as $result){
      $json["result"][] = $result->attributes;
    }
    
    echo CJavaScript::jsonEncode($json);
		Yii::app()->end();
  }

  public function actionMasterReport(){
    $criteria = new CDbCriteria();
    $criteria->join = "JOIN BusinessPartner as bp ON t.id_businessPartner = bp.id_businessPartner";
    $criteria->condition = "role = 'homeowner'";
    $criteria->order = "bp.name ASC";
    $users = User::model()->findAll($criteria);
    //$result = "Check-in;Check-out;Stay Nights;Guest Name;Number of guests;Property;Provider;Booking Source;Guest Rent;\n";
    $result = array(array(
      "Check-in", "Check-out", "Stay Nights", "Guest Name", "Number of Guests", 
      "Property", "Provider", "Booking Source", "Guest Rent", 
      "Per Night", "Guest Fees", "Commission Net", "VAT", "Owner Fees", "Net to Owner" 
    ));
    
    $totals = array("", "", "0.00", "", "0,00", "", "", "", "0,00", "0,00", "", "", "", "", "0,00");
    
    $fp = fopen('masterReport.csv', 'w');
    foreach($users as $user){
      $announcements = Announcement::model()->findAllByAttributes(array("id_host" => $user->id_businessPartner));
      foreach($announcements as $announcement){
        $criteria = new CDbCriteria();
        $criteria->condition = "id_announcement = :annId AND ".
                                "checkinDate >= '2016-01-01 00:00:00' AND ".
                                "checkinDate < '2016-02-01 00:00:00' AND id_reservationStatus = 1 AND isblockedDates = 0";
        $criteria->params = array(":annId" => $announcement->id_announcement);
        $reservations = Reservation::model()->findAll($criteria);
        foreach($reservations as $reservation){
          $record = array();
          $csvNet = number_format($reservation->net, 2, '.', '');
          $csvNights = $reservation->getTotalNights();
          $csvHostEarnings = $reservation->getHostEarnings();
          $csvAvgNight = bcdiv($csvNet, $csvNights, 2);
          
          $record[] = $reservation->checkinDate;
          $record[] = $reservation->checkoutDate;
          $record[] = $csvNights;
          $record[] = $reservation->guest()->getFullName();
          $record[] = $reservation->resGuestInfos()->adultsNumber;
          $record[] = $announcement->title;
          $record[] = $user->businessPartner()->getFullName();
          $record[] = $reservation->channel()->channel()->channelName;
          $record[] = $csvNet;
          $record[] = $csvAvgNight;
          $record[] = ""; //guest commission
          $record[] = ""; //commission net
          $record[] = "0.00"; 
          $record[] = "-24.40";
          $record[] = $csvHostEarnings;
          
          $result[] = $record;
          
          $totals[2] = bcadd($totals[2], $csvNights, 2);
          $totals[4] = bcadd($totals[4], $reservation->resGuestInfos()->adultsNumber, 2);
          $totals[8] = bcadd($totals[8], $reservation->net, 2);
          $totals[9] = "0,00";
          $totals[14] = bcadd($totals[14], $csvHostEarnings, 2);
        }
      }
    }

    $totals[9] = bcdiv($totals[8], $totals[2], 2);
    
    $result[] = $totals;
    
    foreach ($result as $fields) {
      fputcsv($fp, $fields);
    }
    
    die("in teoria va bene");
  }
  
  public function actionTest(){
    $reservation = new Reservation();
    $reservation->id_announcement = 2;
    $reservation->id_channel = 4;
    $reservation->id_guest = 8;
    $reservation->id_propertyOwner = 2;
    $reservation->id_currency = 1;
    $reservation->id_manager = 2;
    $reservation->id_reservationStatus = 1;
    $reservation->checkinDate = 1;
    $reservation->checkinHour = 1;
    $reservation->checkoutDate = 1;
    $reservation->checkoutHour = 1;
    $reservation->gross = 400;
    $reservation->net = 390;
    $reservation->channelCommission = 10;
    
    if(!$reservation->save()){
      print_r($reservation->getErrors());
    }
    die();
  }
  
  /*
   * TODO REFACTOR 
   */
  public function actionCalculatedCommissions(){
    $json = parent::startRestApi();
    
    YiiBase::import("application.controllers.AnnouncementController");
    $business = 2;
    
    $reservationId = isset($_GET["reservation"]) ? $_GET["reservation"] : null ;
    $reservation = Reservation::model()->findByAttributes(array("id_reservation" => $reservationId));
    if($reservation === null) { parent::endRestApi($json); }
    
    $from = (new DateTime($reservation->creationDate))->format("Y-m-d");
    $announcementContracts = AnnouncementController::getAnnouncementContracts($reservation->id_announcement, $from);

    $instances = array(); $ids = array();
    if(!empty($announcementContracts)){
      $json["success"] = true;
      $contract = $announcementContracts["contracts"][0];
      foreach($contract["commissions"] as $commission){
        $instance = $commission->commissionInstance();
        if($instance !== null){
          $instances[] = $instance->attributes;
          if($instance->id_reservation == $reservation->id_reservation){
            $ids[] = $instance->id_commission;
          }
        }
      }
      
      if(count($ids) > 0)
        for($i = 0; $i < count($instances); $i++)
          if(in_array($instance->id_commission, $ids) && $instance->id_reservation === "null")
            array_splice($instances, $i--, 1);

      $json["result"] = $instances;
    }
    
    parent::endRestApi($json);
  }
  
  public function actionHomeowner(){
    $json = parent::startRestApi();
    $user = User::model()->findByAttributes(array("username" => Yii::app()->user->id));
    $bp = $user->businessPartner();

    if($bp === null) parent::endRestApi($json);

    $from = isset($_GET["from"]) ? new DateTime($_GET["from"]) : null;
    $to = isset($_GET["to"]) ? new DateTime($_GET["to"]) : null;
    $months = isset($_GET["months"]) ? $_GET["months"] : null;
    $single = isset($_GET["listing"]) ? $_GET["listing"] : null;
    $includeEnquiry = isset($_GET["includeEnquiry"]) ? $_GET["includeEnquiry"] : null;
    $includePortal = isset($_GET["includePortal"]) ? $_GET["includePortal"] : null;
      
    if(!empty($single)){
      $announcement = Announcement::model()->findByAttributes(array("id_announcement" => $single));
      $json["listingCode"] = $single;
      $json["listingTitle"] = $announcement->title;
    }
    
    //check if month split flag is set, if settes represents the number of months.
    if(!empty($months)){
      $currentMonth = new DateTime($from->format("Y-m-01"));
      $interval = new DateInterval("P1M");
      $found = array();
      $json["fromto"] = array();
      
      for($i = 0; $i < $months; $i++){
        $mDays = cal_days_in_month(CAL_GREGORIAN, $currentMonth->format("m"), $currentMonth->format("Y"));
        $f = new DateTime($currentMonth->format("Y-m-d"));
        $t = new DateTime($currentMonth->format("Y-m-$mDays"));
        
        $json["fromto"][] = $f->format("Y-m-d")." ".$t->format("Y-m-d");
        
        $found[] = $this->getHomeOwnerAnnouncements($bp, $single, $includeEnquiry, $f, $t);
        $currentMonth->add($interval);//nextMonth
      }
    }else
      $found = $this->getHomeOwnerAnnouncements($bp, $single, $includeEnquiry, $from, $to);

    $reservations = array();
    $announcements = array();

    //reorder by announcements.
    foreach($found as $rs){
      
      if(is_array($rs)){
        $range = array();
        foreach($rs as $r){
          $range[] = $this->convertReservationInRange($r, $includePortal);
        }
      }else{
        $range = $this->convertReservationInRange($rs, $includePortal);
      }
      
      $ranges[] = $range;
    }
    
    $json["success"] = true;
    $json["result"] = $ranges;
    
    parent::endRestApi($json);
  }
  
  public function actionPropertyManager(){
    $json = parent::startRestApi();
    
    $from = isset($_GET["from"]) ? new DateTime($_GET["from"]) : null;
    $to = isset($_GET["to"]) ? new DateTime($_GET["to"]) : null;
    $includeEnquiry = isset($_GET["includeEnquiry"]) ? $_GET["includeEnquiry"] : null;
    $includePortal = isset($_GET["includePortal"]) ? $_GET["includePortal"] : null;
    
    $criteria = new CDbCriteria();
    $criteria->order = "title ASC";
    
    $annuncements = Announcement::model()->findAll($criteria);
    $reservations = array();
    
    $countera = 0;
    foreach($annuncements as $announcement){
      $rs = $this->getHomeOwnerAnnouncements($announcement->host(), $announcement->id_announcement, $includeEnquiry, $from, $to);
      
      $reservations[] = array(
        "listing" => $announcement->id_announcement,
        "title" => $announcement->title,
        "reservations" => $this->convertReservationsInRanges($rs)
      );
    }
      
    $json["success"] = true;
    $json["result"] = $reservations;
    parent::endRestApi($json);
  }
  
  public function convertReservationsInRanges($rs, $includePortal = 1){
    $converted = array();
    foreach($rs as $r){
      $converted[] = $this->convertReservationInRange($r, $includePortal);
    }
    return $converted;
  }

  public function actionTestForo(){
    $rs = Reservation::model()->findByAttributes(array("id_reservation" => 6411));
    $announcement = $rs->announcement();//Announcement::model()->findByAttributes(array("id_announcement" => 463));
    $announcementRule = $announcement->getChannelsRule($rs->bookingTime);
    
    $rentopolisCommission = $announcementRule->rentopolisCommission;
    
    $channelCommission = $rs->channelCommission;
    $hownerNet = $rs->net;// - $rs->channelCommission;
    $hownerPercentage = bcmul(bcdiv(bcsub("100", $rentopolisCommission), "100", 4), $hownerNet, 4);
    $hownerEarnings = bcsub($hownerPercentage, "24.40", 2);
    
    var_dump($hownerEarnings);
    die();
  }
  
  public function convertReservationInRange($rs, $includePortal = 1){
    
    $channelName = $rs->channel()->getChannelName();
    $channelImage = $rs->channel()->getChannelImage();
    $announcement = $rs->announcement();
    
    if(!empty($rs->bookingTime)){
      $announcementRule = $announcement->getChannelsRule($rs->bookingTime);
    }else{
      $announcementRule = $announcement->getChannelsRule($rs->checkinDate);
    }
    
    if(empty($announcementRule)) die($rs->checkinDate.", ".$rs->id_reservation);
    
    $rentopolisCommission = $announcementRule->rentopolisCommission;
    
    $channelCommission = $rs->channelCommission;
    $hownerNet = $rs->net;// - $rs->channelCommission;
    $hownerPercentage = bcmul(bcdiv(bcsub("100", $rentopolisCommission), "100", 4), $hownerNet, 4);
    $hownerEarnings = bcsub($hownerPercentage, "24.40", 2);
    
    if(intval($rs->isBlockedDates) != 1)
      $extra = $rs->reservationStatus()->name;
    else{
      if($rs->guest() !== null && (strpos(strtolower($rs->guest()->name), "owner") !== false ||
                                   strpos(strtolower($rs->guest()->surname), "owner") !== false)){
        $extra = "BLOCKED_BY_OWNER";
      }else
        $extra = "BLOCKED_BY_AGENCY";
    }

    $datediff = strtotime((new DateTime($rs->checkoutDate))->format("Y-m-d")) - strtotime((new DateTime($rs->checkinDate))->format("Y-m-d"));
    $nights = floor($datediff/(60*60*24));

    $range = array(
      "code" => $rs->id_reservation,
      "propertyId" => $announcement->id_announcement,
      "propertyTitle" => rawurlencode($announcement->title),
      "chmCode" => $rs->channelManagerReferement,

      "checkin" => (new DateTime($rs->checkinDate))->format("Y-m-d"),
      "checkout" => (new DateTime($rs->checkoutDate))->format("Y-m-d"),
      "checkinTime" => (new DateTime($rs->checkinDate))->format("H:i"),
      "checkoutTime" => (new DateTime($rs->checkoutDate))->format("H:i"),
      "nights" => $nights,

      "earnings" => $hownerEarnings,

      "start" => $rs->getFirstNight("Y-m-d"), 
      "end" => $rs->getLastNight("Y-m-d"),

      "isBlockedDates" => intval($rs->isBlockedDates) == 1,
      "id_reservationStatus" => $rs->id_reservationStatus,
      "price" => number_format($rs->net, 2, '.', ''),
      "channelCommission" => number_format($rs->channelCommission, 2, '.', ''),
      "cleaningFee" => number_format($rs->cleaningFee, 2, '.', ''),
      "touristTax" => number_format($rs->touristTax, 2, '.', ''),
      
      "total" => bcadd(
                  number_format($rs->net, 2, '.', ''),
                  /*bcadd(
                    number_format($rs->net, 2, '.', ''), 
                    number_format($rs->cleaningFee, 2, '.', '')
                  ),*/
                  number_format($rs->channelCommission, 2, '.', ''), 2
                 ),
      
      "rrNote" => str_replace('&quot;', ' ', $rs->rrNote),
      "cfNote" => str_replace('&quot;', ' ', $rs->cfNote),
      "ccNote" => str_replace('&quot;', ' ', $rs->ccNote),
      "ttNote" => str_replace('&quot;', ' ', $rs->ttNote),

      //"note" => rawurlencode($rs->note),
      "note" => str_replace('&quot;', ' ', $rs->note),
      
      "extra" => $extra,
      "description" => ( !empty($rs->guest()) ? rawurlencode($rs->guest()->getFullName()) : "---")
    );
    
    $guest = $rs->guest();
    if($guest !== null){
      $range["guest"] = array("name" => $guest->name, "surname" => $guest->surname, 
                              "telephone" => $guest->telephone, "email" => $guest->email);
    }
    
    $range["resGuestInfos"] = $rs->resGuestInfos()->attributes;

    if(intval($includePortal) != 0) 
      $range["source"] = array("name" => $channelName, "url" => $channelImage);
    
    return $range;
  }

  public function actionTestWubook(){
    /*$wuBook = new WuBook();
    
    $wuAnnouncementt = Announcement::model()->findByAttributes(array("id_announcement" => 538));

    $wuAnnouncements = array($wuAnnouncementt);
    
    foreach($wuAnnouncements as $wuannouncement){
      $json["result"] = $wuBook->import(array("lcode"=>$wuannouncement->wubook_lcode));
      $result = json_encode($json);
      $log = new LogImportOperations();
      $log->result = $result;
      $log->save();
    }*/
  }

  /*1: confirmed
    2: waiting for approval (only wubook reservations)
      3: refused (only wubook reservations)
      4: accepted (only wubook reservations)
        5: cancelled
          6: (probably not used anymore): cancelled with penalty*/
  
  public function actionTestWubookk(){
    $wuBook = new WuBook();
    $ret = $wuBook->retrieveAnnouncement("Villa l'Avvenire T - Siena");
    
    echo "<pre>";
    var_dump($ret);
    die("</pre>");
  }

  public function actionTestImport(){
    $json = parent::startRestApi();
  
    //466 - argelati
    //511 - baracca
    //542 - bertani a
    //539 - bertani b
    //544 - Bettinetti
    
    //462 - Bolivar
    //495 - Buonarroti
    //546 - Castelfidardo
    //493 - Cavaleri
    //492 - Comelico
    
    //471 - Da Procida
    //467 - Ponti
    
    //549 - Cristoforo Colombo
    //547 - Mac Mahon
    //538 - Vicolo
    //551 - Via della Giuiana
    //548 - Aosta
    
    //510 - Gandhi
    //474 - Garibaldi 108
    //527 - Lanzone
    
    //505 - 506 - Marco Polo 1/2
    //513 - Mazzo
    //500 - Melchiorre Gioia

    $criteria = new CDbCriteria();
    //$criteria->condition = "id_announcement IN (466, 511, 542, 539, 544) AND checkoutDate >= '2016-02-01 00:00:00'";
    
    $criteria->condition = 
      //"id_announcement = 538 AND checkoutDate >= '2016-02-01 00:00:00' AND (id_reservationStatus = 1 OR id_reservationStatus = 6)";
    "id_announcement = 550 and (id_reservationStatus = 1 or id_reservationStatus = 6) and checkinDate >= '2015-01-01 00:00:00'";
      
      /*"id_announcement = 538 AND checkinDate >= '2016-02-14 00:00:00' AND
      (id_reservationStatus = 1 OR id_reservationStatus = 6)";*/
    
    $reservations = Reservation::model()->findAll($criteria);
    
    $counter = 0;
    $totalErrors = 0 ;
    
    foreach($reservations as $reservation){
      $idChm = 2;
      $remoteChannelManager = new VREasy($idChm);
      $error = false;
      $counter++;
      
      if(!$reservation->isSynched($idChm)){
        
        /*print_r($reservation->attributes);
        die();*/
        //die($reservation->id_reservation);
        
        $remoteReservation = $remoteChannelManager->convertToRemoteReservation($reservation, false);
        
        $resulted = $remoteChannelManager->createRemoteReservation($remoteReservation);
        $result = $resulted["received"];
        if(!isset($result->id)) $error = true;
        else $reservation->updateMapSynch($idChm, $result->id, 1);
        
        $logData["sended"] = json_encode($resulted["sended"]);
        $logData["message"] = json_encode($resulted["received"]);
        if($error){
          $log = new LogErrorOperationSynch();
          $generalError = true;
          $log->attributes = $logData;
          $log->id_reservation = $reservation->id_reservation;

          $log->save();
        }

      }
      /*else{
        $remoteReservation = $remoteChannelManager->convertToRemoteReservation($reservation, false);
        
        $resulted = $remoteChannelManager->updateRemoteReservation($remoteReservation, $reservation);
        $result = $resulted["received"];
        if(!isset($result->id)) $error = true;
        else $reservation->updateMapSynch($idChm, $result->id, 1);
        
        $logData["sended"] = json_encode($resulted["sended"]);
        $logData["message"] = json_encode($resulted["received"]);
        
      }*/

      
      
      if($counter % 5 === 0){
        sleep(1);
      }
    }
    
    if($totalErrors > 0){
      echo "c'è stato qualche problema.";
    }else{
      echo "Tutto ok.";
    }
    die();
    //isSynchedWithChm
    
    parent::endRestApi($json);
  }

  public function actionTestUpdate(){
    $json = parent::startRestApi();

    $announcements = Announcement::model()->findAll();
    
    foreach($announcements as $announcement){
      $host = $announcement->host();
      if(!empty($host->email)){
        $settings = AnnouncementSettings::model()->findByAttributes(array(
                      "id_announcement" => $announcement->id_announcement, "id_field" => 404));
        
        if($settings == null){
          $settings = new AnnouncementSettings();
          $settings->id_announcement = $announcement->id_announcement;
          $settings->id_field = 404;
          $settings->save();
        }
        
        $settings->value = $host->email;
        $settings->save();
      }
      
      if(!empty($host->telephone)){
        $settings = AnnouncementSettings::model()->findByAttributes(array(
                      "id_announcement" => $announcement->id_announcement, "id_field" => 405));

        if($settings == null){
          $settings = new AnnouncementSettings();
          $settings->id_announcement = $announcement->id_announcement;
          $settings->id_field = 405;
          $settings->save();
        }
        
        $settings->value = $host->telephone;        
        $settings->save();
      }
    }
    
    parent::endRestApi($json);
  }
  
  public function contains($str, array $arr){
    foreach($arr as $a) {
      if (stripos($str,$a) !== false) return true;
    }
    return false;
  }

  public function actionSaveReservation(){
    
    YiiBase::import("application.controllers.AnnouncementController");
    $json = parent::startRestApi();
    $errors = array();
    $errorMessage = "";
    $data = array();
    $user = User::model()->findByAttributes(array("username" => Yii::app()->user->id));
    $role = $user->role;
    
    /*required fields*/
    if(!empty($_POST["checkin"])) $data["checkinDate"] = $_POST["checkin"]; else $errors[] = "checkin";
    if(!empty($_POST["checkout"])) $data["checkoutDate"] = $_POST["checkout"]; else $errors[] = "checkout";
    if(!empty($_POST["propertyId"])) $data["id_announcement"] = $_POST["propertyId"]; else $errors[] = "propertyId";

    $checkout = (new DateTime($_POST["checkout"]));
    $checkin = (new DateTime($_POST["checkin"]));
    
    if(strtotime($checkout->format("Y-m-d")) <= strtotime($checkin->format("Y-m-d"))){
      $errors[] = "checkin";
      $errors[] = "checkout";
    }

    if(!empty($_POST["id_reservationStatus"]) && intval($_POST["id_reservationStatus"]) > -1) 
      $data["id_reservationStatus"] = $_POST["id_reservationStatus"]; 
    else 
      $errors[] = "status";

    /*optional fields*/
    $data["enablePricingUpdates"] = false;
    if(isset($_POST["enablePricingUpdates"])) $data["enablePricingUpdates"] = $_POST["enablePricingUpdates"] != 0 ? true : false;
    if(isset($_POST["guest"])) $data["guest"] = $_POST["guest"];
    if(isset($_POST["price"]) && !empty($data["enablePricingUpdates"])) $data["net"] = $_POST["price"];
    if(isset($_POST["channelCommission"]) && !empty($data["enablePricingUpdates"])) $data["channelCommission"] = $_POST["channelCommission"];
    if(isset($_POST["cleaningFee"]) && !empty($data["enablePricingUpdates"])) $data["cleaningFee"] = $_POST["cleaningFee"];
    if(isset($_POST["touristTax"]) && !empty($data["enablePricingUpdates"])) $data["touristTax"] = $_POST["touristTax"];
    if(isset($_POST["rrNote"])) $data["rrNote"] = CHtml::encode($_POST["rrNote"]);
    if(isset($_POST["ccNote"])) $data["ccNote"] = CHtml::encode($_POST["ccNote"]);
    if(isset($_POST["cfNote"])) $data["cfNote"] = CHtml::encode($_POST["cfNote"]);
    if(isset($_POST["ttNote"])) $data["ttNote"] = CHtml::encode($_POST["ttNote"]);
    if(isset($_POST["note"])) $data["note"] = CHtml::encode($_POST["note"]);
    
    $isNewReservation = true;
    $data["isNewReservation"] = true;

    if(!empty($_POST["code"])){
      $data["id"] = $_POST["code"];
      $isNewReservation = false;
      $data["isNewReservation"] = false;
    }
    
    if(!empty($data["guest"])){
      $data["guest"]["name"] = CHtml::encode($data["guest"]["name"]);
      $data["guest"]["surname"] = CHtml::encode($data["guest"]["surname"]);
      $data["guest"]["telephone"] = isset($data["guest"]["telephone"]) ? CHtml::encode($data["guest"]["telephone"]) : "";
      $data["guest"]["email"] = isset($data["guest"]["email"]) ? CHtml::encode($data["guest"]["email"]) : "";
    }

    $announcement = Announcement::model()->findByAttributes(array("id_announcement" => $_POST["propertyId"]));

    /* This method can be called from different user roles.
     * In case that an homeowner wants to bloack some dates,
     * the following condition will be more restricted.
     */
    $criteria = new CDbCriteria();
    $equal = ""; //$role == "homeowner" ? "=" : "" ;

    /* check reservations announcement overlap, only if the actual reservation in not cancelled */
    if(intval($data["id_reservationStatus"]) != 5 && $isNewReservation){
      $criteria->condition = "t.id_announcement = :listingId AND t.id_reservationStatus <> 5 AND t.id_reservationStatus <> 4 AND (
        (:from >= DATE(checkinDate) AND :from <$equal DATE(checkoutDate)) OR
        (:to >$equal DATE(checkinDate) AND :to <= DATE(checkoutDate)) OR
        (:from <= DATE(checkinDate) AND :to >= DATE(checkoutDate))
      )";
      $criteria->params = array(
        ":listingId" => $announcement->id_announcement,
        ":from" => $checkin->format("Y-m-d"),
        ":to" => $checkout->format("Y-m-d")
      );

      $overlaps = Reservation::model()->findAll($criteria);
      
      if(count($overlaps)>0){
        $first = $overlaps[0]->attributes;
        
        if(!in_array("status", $errors)){
          if(intval($data["id_reservationStatus"]) == 1)
            $endErrorMessage = " della tua prenotazione";
          else if(intval($data["id_reservationStatus"]) == 6)
            $endErrorMessage = " del tuo blocco";
          
          $errorMessage = 
            "Una prenotazione con checkin il ".$first["checkinDate"]." e checkout il ".$first["checkoutDate"].
            " impedisce la creazione ".$endErrorMessage;
        }
          
        
        $errors[] = "checkin";
        $errors[] = "checkout";
      }
    }

    if(count($errors) > 0){
      $json["success"] = false;
      
      $json["errors"] = $errors;
      $json["errorMessage"] = $errorMessage;
      
      parent::endRestApi($json);
    }

    $data["resGuestInfos"] = $_POST["resGuestInfos"];

    if(isset($_POST["checkinTime"])) 
      $data["checkinDate"] .= " ".$_POST["checkinTime"];
    else
      $data["checkinDate"] .= " ".$announcement->checkin;

    if(isset($_POST["checkoutTime"])) 
      $data["checkoutDate"] .= " ".$_POST["checkoutTime"];
    else
      $data["checkoutDate"] .= " ".$announcement->checkout;

    $reservation = null;
    $reservationOldValues = null;
    
    $exceptions = array(
      "block", "hold", "owner", "propr", "blocc", "close", "chius", " end", "end ", "fine ", " fine", 
      "rentopolis", "retopolis", "contratt", "contract"
    );
    $status = ReservationStatus::model()->findByAttributes(array("id_reservationStatus" => $data["id_reservationStatus"]));
    if($status->name == "UNAVAILABLE" || 
       $this->contains($data["guest"]["name"], $exceptions) ||
       $this->contains($data["guest"]["surname"], $exceptions)){
      $data["isBlockedDates"] = 1;
    }else{
      $data["isBlockedDates"] = 0;
    }
    
    
    if($data["isNewReservation"]){
      /*CREATE*/
      $isWubook=AnnouncementChannelManager::model()->findByAttributes(array(
        "id_announcement"=>$data["id_announcement"], 
        "id_channelManager"=>3));
      /*if($isWubook !== null){
        $json["success"] = false;
        parent::endRestApi($json);
      }*/

      $resGuestInfos = new ResGuestInfos();
      $resGuestInfos->setAttributes($data["resGuestInfos"]);

      $guestbp = new BusinessPartner();
      $guest = new Guest();
      $guest->travels = 1;
      $guestbp->name = isset($data["guest"]) ? $data["guest"]["name"]  : "";
      $guestbp->surname = isset($data["guest"]) ? $data["guest"]["surname"]  : "";
      $guestbp->telephone = isset($data["guest"]) ? $data["guest"]["telephone"]  : "";
      $guestbp->email = isset($data["guest"]) ? $data["guest"]["email"]  : "";

      if($status->name == "UNAVAILABLE"){
        //$guestbp->name = "Rentopolis block";
        //$guestbp->surname = "";
        if($role == "homeowner"){
          $guestbp->name = "Owner block";
        }
      }

      $reservation = new Reservation();
      $reservation->setAttributes($data);
      $reservation->bookingTime = (new DateTime(null, new \DateTimeZone("UTC")))->format("Y-m-d H:i:s"); 
      $reservation->setIsNewReservation(true);
      if($data["enablePricingUpdates"])
        $reservation->flagPu = 1;
      
      if($guestbp->validate() && $guest->validate() && $reservation->validate() && $resGuestInfos->validate()){
        $resGuestInfos->save();
        $reservation->id_resGuestInfos = $resGuestInfos->id_resGuestInfos;

        $guestbp->save();

        $guest->id_businessPartner = $guestbp->id_businessPartner;
        $guest->save();
        
        $reservation->id_guest = $guestbp->id_businessPartner;
        
        //if($data["id_reservationStatus"])
        
        //
        //17 Not Specified
        //20 Direct Booking

        $chId = 17;
        if($status->name == "CONFIRMED"){
          $chId = 20;
        }
        
        $configuration = AnnouncementController::getAnnouncementChannelManagerConfiguration($announcement->id_announcement);
        
        $firstActivated = 0;
        foreach($configuration as $config){
          if($config["activated"]){
            $firstActivated = $config["channelManagerId"];
            break;
          }
        }
        
        //fake check for old VREasy
        //if(intval($configuration[0]["channelManagerId"]) == 2){
        if(intval($firstActivated) == 2){
          $reservation->id_channel = 18; //stay for old VREasy non specified source.
        }else{
          foreach($configuration as $config){
            if($config["activated"]){
              $className = $config["className"];
              $chm = new $className($config["channelManagerId"]);
              $now = new DateTime();
              if($chm->isConnectedChannel($chId, (new DateTime(null, new \DateTimeZone("UTC")))->format("Y-m-d H:i:s"))){
                $assoc = ChannelManagerChannelAssociation::model()->findByAttributes(array(
                  "id_channel" => $chId,
                  "id_channelManager" => $config["channelManagerId"]
                ));
                
                $reservation->id_channel = $assoc->id_channelAssociation; 
                break;
              }
            }
          }
        }
        
        if($reservation->save()) $json["success"] = true;
      }
    }else{
      /*UPDATE*/
      $reservationOldValues = new Reservation();
      $reservation = Reservation::model()->findByAttributes(array("id_reservation" => $data["id"]));

      $reservationOldValues->setAttributes($reservation->getAttributes());
      $reservation->setAttributes($data);

      $reservation->setIsNewReservation(false);
      if($data["enablePricingUpdates"])
        $reservation->flagPu = 1;

      $resGuestInfos = $reservation->resGuestInfos();
      $resGuestInfos->setAttributes($data["resGuestInfos"]);
      $resGuestInfos->update();
      
      $guest = $reservation->guest();
      $guest->name = $data["guest"]["name"];
      $guest->surname = $data["guest"]["surname"];
      $guest->email = $data["guest"]["email"];
      $guest->telephone = $data["guest"]["telephone"];
      $guest->save();
      
      if($reservation->update()) $json["success"] = true;
    }

    if($json["success"] == true) $reservation->synchReservation($reservationOldValues, $data["enablePricingUpdates"]);
    //$synchResult = $this->synchReservation($reservation);

    $json["result"] = $reservation->attributes;
    $json["result"]["isNew"] = $reservation->getIsNewReservation();

    parent::endRestApi($json);
    /*$data = array(
      "property_id" => "35812",
      "listor_id" => 1,
      "accommodation_cost" => 100.00, 
      "status" => "CONFIRMED", 
      "checkin" => "2016-04-25",
      "checkout" => "2016-04-30",
      "guest"=>array(
        "fname"=>"Luca",
        "lname"=>"Bongiovi"
      )
    );
    $vreasy = new VREasy();
    
    $json["result"] = $vreasy->createRemoteReservation($data);
    $json["success"] = true;
    
    parent::endRestApi($json);*/
  }
  
  public function actionBusinessStockChart(){
    $json = parent::startRestApi();
    
    $bc = BusinessChart::model()->findByAttributes(array("description" => "STOCK_GROSS_NET"));
    $json["result"] = $bc->datas;
    
    parent::endRestApi($json);
  }

  public function actionTestCancellation(){
    $json = parent::startRestApi();

    $vreasy = new VREasy();
    $json["result"] = $vreasy->import(array("cleaningMode"=>1));
    
    parent::endRestApi($json);
  }

  public function actionTestChannels(){
    $json = parent::startRestApi();

    $channelManager = new VREasy(5);
    $json["result"] = $channelManager->retrieveChannels();
    
    foreach($json["result"]["channels"] as $channelRetrieved){
      $channel = Channel::model()->findByAttributes(array("channelName" => $channelRetrieved->name));
      if($channel === null){
        $channel = new Channel();
        $channel->channelName = $channelRetrieved->name;
        if(!isset($channelRetrieved->logo_image->url)){
          $channel->image = "";
        }else{
          $channel->image = $channelRetrieved->logo_image->url;
        }
        $channel->save();
      }
      
      if(empty($channel->image) && isset($channelRetrieved->logo_image->url)){
        $channel->image = $channelRetrieved->logo_image->url;
        $channel->save();
      }

      $channelId = $channel->id_channel;
      $listorId = $channelRetrieved->id;
      $channelChannelManager = ChannelManagerChannelAssociation::model()->findByAttributes(array(
        "id_channelManager" => $channelManager->getChannelManagerId(),
        "id_channel" => $channelId,
        "referement" => $listorId
      ));

      if($channelChannelManager === null){
        $channelChannelManager = new ChannelManagerChannelAssociation();
        $channelChannelManager->id_channel = $channelId;
        $channelChannelManager->id_channelManager = $channelManager->getChannelManagerId();
        $channelChannelManager->referement = $listorId;
        $channelChannelManager->save();
      }
    }

    parent::endRestApi($json);
  }
  
  public function actionTestPayments(){
    $json = parent::startRestApi();
    $jsonString = '{"id":922322,"adults":2,"kids":0,"additional_description":"Reservation source: Booking.com reservation #193408757 The following information was provided by Booking.com at the time the reservation was created: *** Reservation information *** Reservation made on: 2015-03-31 at 21:13:50 Currency: EUR Total reservation amount: 495.000 Total reservation commission amount: 89.100 *** Customer information *** Name: Elisabeth Bartoli Email: elisabeth.bartoli@equip-prod.com Address: Zip \/ Post code: City: . Country: France Company: Phone: 0628470578 Remarks: *** Room guest information *** Room arrival date: 2015-10-05 Room departure date: 2015-10-08 Check-in: 2015-10-05 Check-out: 2015-10-08 Guest name: Elisabeth Bartoli Number of guests: 2 Smoking preference: Non-smoking Remarks: *** Room\/facilities information *** Room facilities: bollitore t\u00e8 \/ macchina caff\u00e8, doccia, vasca, TV, aria condizionata, frigorifero, scrivania, zona soggiorno, WC, lavatrice, riscaldamento, cucina, vasca o doccia, TV a schermo piatto, divano, vista, parquet o pavimento in legno, zona pranzo, bollitore elettrico, utensili da cucina, armadio\/guardaroba, forno, piano cottura, bidet, vista giardino, bagno aggiuntivo, asciugamani, biancheria per la casa, tavolo da pranzo, piani superiori accessibili tramite ascensore, appartamento privato in edificio Room information: La tariffa di questa camera non include nessun pasto. Condizioni per i bambini e per il letto supplementare : I bambini sono i benvenuti. Il numero massimo consentito di letti supplementari \u00e8 1. Deposito\/Pagamento Anticipato: Non verr\u00e0 addebitato alcun deposito. Termini di cancellazione: In caso di cancellazione o modifiche effettuate fino a 14 giorni prima della data prevista di arrivo non viene addebitato alcun costo. In caso di cancellazione o modifiche effettuate oltre tale termine o di mancata presentazione viene addebitato il 100% del costo della prima notte. Room extra information: Appartamento con vista, area salotto e divano. *** Room pricing information *** Currency: EUR Total price: 495.000 Commission: 89.100 Nightly prices: 2015-10-05: 165.000 (rate: 4774628 \"Standard Rate\") 2015-10-06: 165.000 (rate: 4774628 \"Standard Rate\") 2015-10-07: 165.000 (rate: 4774628 \"Standard Rate\")","accommodation_cost":450,"currency_id":1,"host_id":704,"guest_id":2226181,"status":"CONFIRMED","checkin":"2015-10-05T14:30:00+02:00","checkout":"2015-10-08T11:00:00+02:00","created_at":"2015-03-31 19:29:35","listor_id":129,"gateway_listor_id":134,"portal_uid":3075867,"subscription_id":0,"user_id":1619784,"language_id":1,"updated_at":"2015-11-09 00:32:54","auto_email_enabled":1,"auto_sms_enabled":1,"auto_task_enabled":1,"auto_email_run_at":"2015-03-31 19:29:36","auto_sms_run_at":"2015-03-31 19:29:37","auto_task_run_at":"2015-03-31 19:29:38","custom_eta":"14:30:00","custom_etd":"11:00:00","guest":{"guest_id":2226181,"fname":"Elisabeth","lname":"Bartoli","email":"elisabeth.bartoli@equip-prod.com","phone":"+33628470578","updated":"2015-10-05 12:56:16","home_address":{},"created":"2015-03-31 19:29:35"},"listor":{"id":129,"name":"BookingHomie","url":"http:\/\/bookinghomie.com\/","listor_payments_id":0,"ical_regex":null,"public_url_regex":null,"has_auth_questions":0,"badge":"SILVER","category":"CHANNEL","online_booking_supported":"FULL","prices_sync_supported":"FULL","content_sync_supported":"FULL","availability_sync_supported":"FULL","pm_final_price_formula":null,"guest_final_price_formula":null,"hidden_at":null,"has_api_access":0,"api_role_id":null,"granularity_per_destination_supported":0,"push_prices_from_ui_supported":0,"edit_listing_from_ui_supported":0,"extra_data":{},"variables":[],"custom_variables":[],"payment":{},"title":{},"description_title":{},"description":{},"about_title":{},"about":{},"sync_task_types":[],"destinations":[],"logo_image":{},"image":{},"pm_final_price_m":1,"pm_final_price_b":0,"guest_final_price_m":1,"guest_final_price_b":0},"payments":[{"id":29129,"payment_type_id":1663,"payer_user_id":1674250,"payee_user_id":1619784,"task_id":null,"reservation_id":922322,"due_date":"2015-10-23","description":null,"status":"unpaid","person_multi":1,"night_multi":1,"discount_percentage":0,"amount":495,"total":495,"amount_paid":0,"payment_type":{"id":1663,"name":"Rent payment","user_id":1619784,"position":19},"currency_id":1}],"property_id":23235}';
    
    $jsonString = json_decode($jsonString);

    $channel = Channel::model()->findByAttributes(array("channelName" => $jsonString->listor->name));
    $announcementAssoc = AnnouncementChannelManager::model()->findByAttributes(array(
      "alias" => $jsonString->property_id,
      "id_channelManager" => 2,
    ));
    $announcement = $announcementAssoc->announcement();
    $jsonString->id_announcement = $announcement->id_announcement;
    $jsonString->isBlockedDates = 0;

    $vreasy = new VREasy();

    $json["announcement"] = $announcement->title;
    $json["result"] = $vreasy->convertRetrievedReservation($jsonString);

    parent::endRestApi($json);
  }

  public function actionTestProperties(){
    $json = parent::startRestApi();
    
    $vreasy = new VREasy();
    $vreasy->importAnnouncements();
    
    $notSynched = array();
    $announcements = Announcement::model()->findAll();
    foreach($announcements as $announcement){
      if(intval($announcement->VREasyKey) == 0){
        $notSynched[] = $announcement->title;
      }
    }
    
    $json["success"] = count($notSynched) == 0;
    $json["result"] = array("notSynched" => $notSynched);
    
    parent::endRestApi($json);
  }
  
  public static function getBoh(){
    return 3;
  }
  
  public function getHomeOwnerAnnouncements($businessPartner, $singleAnnouncement = null, $includeEnquiry = 1, $from = null, $to = null){
    $idBp = $businessPartner->id_businessPartner;
    $rss = array("requests" => array());
    
    $from = new DateTime($from->format("Y-m-d"));
    $to = new DateTime($to->format("Y-m-d"));
    
    if(!empty($from) && !empty($to)){
      $one = new DateInterval('P1D');
      //$from->sub($one);
      //$to->add($one);

      $fromYmd = $from->format("Y-m-d");
      $toYmd = $to->format("Y-m-d");
    }

    $statuses = array(6, 1);

    if(intval($includeEnquiry) == 1) 
      array_push($statuses, 4);

    $reservations = array();

    for($counter = 0; $counter < count($statuses); $counter++){
      $criteria = new CdbCriteria();
      $criteria->with = array(
        "announcement",
        "announcement.host" => array("alias" => "hs")
      );

      $criteria->condition = "t.id_reservationStatus = ".$statuses[$counter]." AND hs.id_businessPartner = :idBp";
      //$criteria->params = array(":status" => $statuses[$counter]);
      $criteria->params = array(":idBp" => $idBp);
      $criteria->order = "t.checkinDate ASC";

      if(!empty($from) && !empty($to)){
        $criteria->condition .= " AND (
        (t.checkinDate >= :from AND t.checkinDate <= :to) OR
        (t.checkoutDate >= :from AND t.checkoutDate <= :to) OR 
        (t.checkinDate <= :from AND t.checkoutDate >= :to)
        )";
        $criteria->params[":from"] = $fromYmd;
        $criteria->params[":to"] = $toYmd;
      }

      if(!empty($singleAnnouncement)){
        $criteria->condition .= " AND t.id_announcement = :listing";
        $criteria->params[":listing"] = $singleAnnouncement;
      }
      
      $results = Reservation::model()->findAll($criteria);
      /*$ss = array();
      foreach($results as $r) $ss[] = $r->attributes;
      $rss["requests"][] = array(":listing"=> $singleAnnouncement, "condition" => $criteria->condition, "results" => $ss); */
      
      $reservations = array_merge($reservations, $results);
    }

    /*echo json_encode($rss);
    die();*/
    
    return $reservations;
  }

  public function actionCleanReservationGuests(){
    $json = parent::startRestApi();

    $guests = Guest::model()->findAll();

    
    $removedGuests = 0;
    $removedBps = 0;
    $i = 0;
    /*foreach($guests as $guest){
      if($removedGuests+$removedBps == 10) break;
      $bp = $guest->businessPartner();
      if($bp != null){
        $reservation = $bp->guestReservation();
        if($reservation === null){
          $bp->delete();
          $guest->delete();
          $removedBps++;
          $removedGuests++;
        }
      }else{
        $guest->delete();
        $removedGuests++;
      }
      $i++;
    }*/

    $json["success"] = true;
    $json["total"] = count($guests);
    $json["removedGuests"] = $removedGuests;
    $json["removedBps"] = $removedBps;

    parent::endRestApi($json);
  }

  public function filters(){
    return array('accessControl');
  }

  public function accessRules(){
    return array(
      array(
        'allow',
        'actions' => array( 'index', "saveReservation", 
                            'testImport', 'testCancellation', 'testPayments', 
                            'testProperties', 'testIcs', 'testChannels', 'testUpdate', 'testWubook', 'testWubookk', 'testForo' ),
        'users'=>array( '*' )
      ),
      array(
        'allow',
        'actions' => array( 
          'homeowner', 'propertymanager', 'businessReservations', 
          'businessStockChart', 'masterReport',
          'calculatedCommissions', 'cleanReservationGuests' ),
        'users'=>array( '@' )
      ),
      array( 'deny' ),
    );
  }
}