<?php

class ChartController extends Controller{
  
	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

  /* 
   * Main action in the controller.
   */

	public function actionIndex(){
		$this->render('index');
	}

  public function actionHomeownerChart(){
    $json = $this->startRestApi();
    $type = $_GET["type"];
    $bpId = $_GET["id"];

    $bhc = BusinessHostsCharts::model()->findByAttributes(array("description" => $type, "id_businessPartner" => $bpId));
    $json["success"] = $bhc !== null;
    $json["results"] = $bhc !== null? $bhc->datas : "[]" ;
    $this->endRestApi($json);
  }
}