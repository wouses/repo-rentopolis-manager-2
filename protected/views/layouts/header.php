<?php
/*$this Controller*/
$controllerId = $this->id;
$actionId = $this->action->id;
?>

<ul class="manager-menu">
  <li ng-repeat="section in menuData.sections" class="parent-list-item {{section.className || ''}}" ng-class="{'parentActive' : isSectionSelected(section)}">
    <h2 class="menu-heading md-subhead" ng-if="section.type === 'heading'" id="heading_{{section.name}}">
      {{section.name}}
    </h2>
    <menu-link section="section" ng-if="section.type === 'link'"></menu-link>

    <menu-toggle section="section" ng-if="section.type === 'toggle'"></menu-toggle>

    <ul ng-if="section.children" class="menu-nested-list">
      <li ng-repeat="child in section.children" ng-class="{'childActive' : isSectionSelected(child)}">
        <menu-link section="child" ng-if="child.type === 'link'"></menu-link>

        <menu-toggle section="child" ng-if="child.type === 'toggle'"></menu-toggle>
      </li>
    </ul>
  </li>
</ul>

<!--<ul>
  <li class="parent-list-item">
    <menu-link section="section" class="ng-scope ng-isolate-scope">
      <a class="md-button md-default-theme active" ng-href="#/" href="#/">
        <span class="ng-binding ng-scope">
          Home
        </span>
      </a>
    </menu-link>
  </li>
  <li class="parent-list-item">
    <menu-link section="section" class="ng-scope ng-isolate-scope">
      <a class="md-button md-default-theme active" ng-href="#/announcements" href="#/announcements">
        <span class="ng-binding ng-scope">
          Annunci
        </span>
      </a>
    </menu-link>
  </li>
  <li class="parent-list-item">
    <menu-link section="section" class="ng-scope ng-isolate-scope">
      <a class="md-button md-default-theme active" ng-href="#/business-partners" href="#/business-partners">
        <span class="ng-binding ng-scope">
          Business Partners
        </span>
      </a>
    </menu-link>
  </li>
  <li class="parent-list-item">
    <menu-link section="section" class="ng-scope ng-isolate-scope">
      <a class="md-button md-default-theme active" ng-href="#/reservations" href="#/reservations">
        <span class="ng-binding ng-scope">
          Prenotazioni
        </span>
      </a>
    </menu-link>
  </li>
  <li class="parent-list-item">
    <menu-link section="section" class="ng-scope ng-isolate-scope">
      <a class="md-button md-default-theme active" ng-href="#/channels" href="#/channels">
        <span class="ng-binding ng-scope">
          Canali
        </span>
      </a>
    </menu-link>
  </li>
  <li class="parent-list-item">
    <menu-link section="section" class="ng-scope ng-isolate-scope">
      <a class="md-button md-default-theme active" ng-href="#/channel-managers" href="#/channel-managers">
        <span class="ng-binding ng-scope">
          Channel Managers
        </span>
      </a>
    </menu-link>
  </li>
</ul>-->
