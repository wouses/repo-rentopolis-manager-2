<?php /* @var $this Controller */?>
<!DOCTYPE html>
<html ng-app="managerApp">
<head>
  
  <?php
    $role = "";
    $urCode = 0;
    if(isset(Yii::app()->user) && 
       isset(Yii::app()->user->id)){
      $user = User::model()->findByAttributes(array("username" => Yii::app()->user->id));
      $role = $user->role;
      $urCode = $user->businessPartner()->id_businessPartner;
    }
  ?>
  
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
  <meta name="viewport" content="initial-scale=1" />

  <!-- backend meta data  -->
  <?php
    $statuses = array(
      array(
        "id_reservationStatus" => -1, 
        "name" => "NOSELECTION", 
        "description" => "Select a status"
      )
    );
    $allSt = ReservationStatus::model()->findAll();
    foreach($allSt as $status){
      if($status->id_reservationStatus>8) continue;
      $statuses[] = $status->attributes;
    }
  ?>
	<meta name="reservationStatuses" content="<?= htmlentities(json_encode($statuses)); ?>" />

  <!-- synchronized token -->
  <meta name="csrf-token-name" content="<?= Yii::app()->getRequest()->csrfTokenName; ?>" />
  <meta name="csrf-token-value" content="<?= Yii::app()->getRequest()->getCsrfToken(); ?>" />

  <!-- general css -->
  <!--<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.9.4/angular-material.min.css">-->
  <!--<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/0.11.2/angular-material.min.css">-->
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=RobotoDraft:300,400,500,700,400italic">
  <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  
  <link rel="stylesheet" type="text/css" href="/css/angular-tooltips.css">
  <link rel="stylesheet" type="text/css" href="/css/md-data-table.min.css">
  
  <!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">-->

  <!-- blueprint CSS framework -->
	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
  <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/Calendar.css?v=1.2"/>

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">-->

  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib/jquery-ui.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::app()->params['googleApiKey']?>&libraries=places&language=it"></script>
  
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/i18n.js"></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/Calendar.js?v=1.2"></script>
  
  <!--<script src="<?= Yii::app()->request->baseUrl; ?>/js/lib/angular-locale_it-it.js"></script>-->
  
  <!--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>-->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.min.js"></script>-->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/angular_material/0.11.2/angular-material.min.js"></script>-->
  <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-aria.min.js"></script>-->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
  
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular-sanitize.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.6/angular-messages.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib/time-picker/smarttime.js"></script>

  <script type="text/javascript">
    ur = '<?= $role ?>';
    urCode = <?= $urCode; ?>
  </script>

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <!--<script src="https://code.highcharts.com/modules/data.js"></script>-->
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>
  <!--<script src="https://code.highcharts.com/stock/highstock.js"></script>-->
  <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
  
  <script src="/js/lib/lodash.compat.min.js"></script>
  <script src="/js/lib/angular-map/angular-google-maps.min.js"></script>
  <script src="/js/lib/angular-agGrid/dist/angular-grid.js"></script>
  <!--<link rel="stylesheet" type="text/css" href="/js/lib/angular-agGrid/dist/angular-grid.css">
  <link rel="stylesheet" type="text/css" href="/js/lib/angular-agGrid/dist/theme-fresh.css">  -->

  <style>
    body {
      font-family: RobotoDraft;
    }

    body > .md-dialog-container {
      height: 100vh !important;
    }

    .popup-admin-tabs md-tab-item{
      max-width: 500px !important;
    }

    .site-sidenav, .site-sidenav.md-locked-open-add-active, .site-sidenav.md-locked-open {
      width: 250px;
      min-width: 250px;
      max-width: 250px;
      overflow:hidden;
    }

    .site-sidenav md-content{
      overflow:hidden;
    }

    .md-toolbar-tools h1 {
      font-size: inherit;
      font-weight: inherit;
      margin: inherit;
      color: rgba(255,255,255,0.87);
    }

    menu-link a.md-button {
      width:100%;
    }

    a.manager-logo { text-decoration:none; }

    .manager-logo h1 {
      color: white;
      text-align: center;
      font-weight: 400;
      font-size: 26px;
    }

    .manager-header, .manager-navigation-content{background-color: #62C1D3 !important;}
    .manager-menu{padding-left:0px;}
    
    menu-link a{width:236px !important;}
    menu-link a span{ color:white; }
    menu-link{width:264px !important;height: 1px !important;}
    menu-link a.md-button:hover {
      background-color: rgba(255, 255, 255, 0.4) !important;
    }
    
    .md-dialog-container{z-index: 2000}
    .md-datepicker-calendar-pane.md-pane-open,
    .md-select-menu-container.md-active.md-clickable{z-index:2002;}
    
    .ag-fresh .ag-cell{
      text-align:center;
    }
    
    md-grid-list.listings md-grid-tile-body{ text-align:center; } 
    md-grid-list.listings md-grid-tile md-grid-tile-footer, md-grid-list.listings md-grid-tile md-grid-tile-header{ color:gray; }
    
    /*Smart time*/
    /* SMART TIME styles */
  .smart-time {
    position: relative; }

  .smt-input {
    width: 56px;
    height: 22px;
    line-height: 22px;
    font-size: 1em;
    padding: 4px 10px;
    border: 1px solid #eeeeee;
    outline: none; }

  .smt-suggestions {
    position: absolute;
    z-index: 1;
    border: 1px solid #eee;
    top: 33px;
    left: 0;
    width: 76px;
    background-color: #fff;
    box-shadow: 1px 3px 4px rgba(140, 140, 140, 0.4); }
    .smt-suggestions ul {
      list-style: none;
      padding: 2px 2px;
      margin: 0; }
      .smt-suggestions ul li {
        display: block;
        padding: 2px 2px;
        line-height: 1.2em;
        height: 1.2em;
        font-size: 0.9em;
        cursor: pointer; }
        .smt-suggestions ul li.selected {
          background-color: rgba(102, 173, 255, 0.9); }
        .smt-suggestions ul li:hover {
          background-color: #eeeeee; }
  
    /*.md-dialog-container input.smt-input{font-size:14px;}*/
    .admin-dialog input.smt-input{
      /*background: url(https://cdn3.iconfinder.com/data/icons/linecons-free-vector-icons-pack/32/clock-16.png) no-repeat left 7px;*/
      /*background: url(https://cdn4.iconfinder.com/data/icons/48-bubbles/48/36.Watch-16.png) no-repeat left 6px;
      padding-left: 20px;*/
      /*padding-bottom:9px;*/
      font-size:15px;
      /*margin-left:40px;*/
    }
    .admin-dialog div[smt-value]{margin-top:10px;}

    .admin-dialog .md-datepicker-input-container{margin-left:0px;}
    .admin-dialog .md-button.md-icon-button{margin-right:0px;}
    
    .admin-dialog md-input-container.md-icon-float,
    .admin-dialog md-input-container.md-icon-float.md-input-focused{margin-top:-16px;}
    
    .admin-dialog md-input-container.checkin-container.md-input-has-value label:not(.md-no-float),
    .admin-dialog md-input-container.checkin-container:not(.md-input-invalid).md-input-focused label,
    .admin-dialog md-input-container.checkout-container.md-input-has-value label:not(.md-no-float),
    .admin-dialog md-input-container.checkout-container:not(.md-input-invalid).md-input-focused label{
      top: -60px;
      left: 56px;
      font-weight:normal;
    }
    
    .admin-dialog md-input-container.checkin-container label:not(.md-no-float),
    .admin-dialog md-input-container.checkout-container label:not(.md-no-float){
      top: -64px;
      left: 54px;
    }
    
    .admin-dialog md-tab-content {padding-top:25px}
    .admin-dialog md-tab-content md-input-container{margin:0}
    
    /*.admin-dialog md-input-container{padding-bottom:15px;}*/
    
  </style>
  
  <script>
    window.googleApiKey = '<?= Yii::app()->params["googleApiKey"]; ?>';
    locate = "it";
    jsViewsVersion = "<?= Yii::app()->params["jsViewsVersion"]; ?>";
  </script>
  
  <script>
  <?php
    if($role != "homeowner"):
  ?>
    var menuData = {
      sections:[
        { name: 'Home', type: 'link', id: 'home', state: '' }, 
        { name: 'Reservations', type: 'link', id: 'admin', state: 'admin' }, 
        { name: 'Properties', type: 'link', id: 'announcements', state: 'announcements' }, 
        { name: 'Business Partners', type: 'link', id: 'business-partners', state: 'business-partners' }, 
        //{ name: 'Reservations', type: 'link', id: 'reservations', state: 'reservations' }, 
        { name: 'Calendars', type: 'link', id: 'calendars', state: 'calendars' }, 
        { name: 'Channels', type: 'link', id: 'channels', state: 'channels' }, 
        { name: 'Charts', type: 'link', id: 'business-charts', state: 'business-charts' }, 
        { name: 'Channel Managers', type: 'link', id: 'channel-managers', state: 'channel-managers' }
      ]
    };
  <?php else : ?>
    var menuData = {
      sections:[
        { name: 'Grafici', type: 'link', id: 'homeowner-charts', state: 'homeowner-charts' },
        { name: 'Prenotazioni', type: 'link', id: 'homeowner', state: 'homeowner' }
      ]
    };
  <?php endif; ?>
  </script>
  
	<title>Manager</title>
</head>

<body layout="column" ng-controller="SystemController">
  <?php
    /*$this: current controller instance*/
    $actionId = $this->action->id;
    $controllerId = $this->id;
  ?>
  <?php if($controllerId != "user" && $actionId != "login"): ?>  
    <div layout="row" flex>
      <md-sidenav layout="column" class="md-sidenav-left md-whiteframe-z2 site-sidenav" md-component-id="left" md-is-locked-open="$mdMedia('gt-sm')">
        <header class="nav-header manager-header">
          <a  class="manager-logo">
            <img style="margin:0 auto;width:304px;margin-left:-32px !important;" src="/images/logo_rentopolis2.png" alt="">
            <!--<img style="margin:0 auto;width:304px;" src="https://cdn4.iconfinder.com/data/icons/seo-and-data/500/globe-tools-settings-512.png" alt="">-->
            <!--<img style="margin:0 auto;width:228px;" src="https://d13yacurqjgara.cloudfront.net/users/113259/screenshots/1188527/settings_big_1x.png" alt="">-->
            <!--<h1 class="docs-logotype md-heading">Management</h1>-->
          </a>
        </header>
        <md-content role="navigation" flex class="md-default-theme manager-navigation-content">
          <?php $this->renderPartial("//layouts/header"); ?>
        </md-content>
      </md-sidenav>
      <div layout="column" flex id="content">
        <md-content id="main-scrollable-view" layout="column" flex class="md-padding">
          <!-- ANGULAR SINGLE PAGE TEMPLATING -->
          <div ng-view></div>
        </md-content>
      </div>
    </div>
  <?php else: ?>
    <?= $content; ?>
  <?php endif; ?>
  <!-- Application -->
  <script src="/js/app/config.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/directives.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/services.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>

  <!-- Controllers -->
  <script src="/js/app/controllers/system-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/dashboard-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/announcement-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/reservation-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/homeowner-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/homeowner-charts-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/homeowner-stats-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/calendars-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/channel-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/channelManager-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/businessPartner-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/user-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/propertymanager-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/app/controllers/businessCharts-controller.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script type="text/javascript" src="/js/angular-tooltips.js?v=<?= Yii::app()->params["jsVersion"] ?>"></script>
  <script src="/js/lib/md-data-table.min.js"></script>
</body>
</html>
