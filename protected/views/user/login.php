<?php
  /* @var $this SiteController */
?>

<style>
  body{background:url(/images/city.jpg) 50% 50% no-repeat;}
  
  .form-container{
    height:100%;
    width:100%;
    position:relative;
  }
  
  .form-container form{
    width:350px;
    position:absolute;
    top:160px;
    left:50%;
    margin-left:-175px;
  }
  
  .form-container form md-content{
    background: transparent;
  }
  
  .form-container form md-input-container input, .form-container form md-input-container label{
    color:white;
    font-weight:bold;
    font-size:17px;
  }
  
  .form-container form input:-webkit-autofill{
    background-color:transparent !important;
  }
  
  md-input-container:not(.md-input-invalid).md-input-focused .md-input{
    border-color: #fff;
  }
  
  md-input-container:not(.md-input-invalid).md-input-focused label{color: #fff;}
  
  md-progress-circular.md-progressive-white .md-inner .md-left .md-half-circle{
    border-left-color:white;
  }
  
  md-progress-circular.md-progressive-white .md-inner .md-left .md-half-circle, md-progress-circular.md-hue-3 .md-inner .md-right .md-half-circle{
    border-top-color:white;
  }
  
  md-progress-circular.md-progressive-white .md-inner .md-right .md-half-circle {
    border-right-color: white;
}
  
</style>

<div class="form-container">
  <form autocomplete="off" ng-controller="UserController as ctrl" style="margin-bottom:0px;">
    <!--<md-toolbar>
      <div class="md-toolbar-tools" style="background-color:#62C1D3;">
        <h2>
          <span style="color:white">Sign In</span>
        </h2>
      </div>
    </md-toolbar>-->
    <md-content>
      <md-input-container flex>
        <label>Username</label>
        <input ng-model="ctrl.user.username" type="search" autocomplete="off">
      </md-input-container>
      <input style="display:none">
      <md-input-container flex>
        <label>Password</label>
        <input ng-model="ctrl.user.password" type="password" autocomplete="off">
      </md-input-container>
      <md-button class="md-raised" style="float:right;" ng-show="!ctrl.ajaxCalling" ng-click="ctrl.signIn()">Sign In</md-button>
      <md-progress-circular class="md-hue-3 md-progressive-white" style="float:right;margin-top:-20px;" ng-show="ctrl.ajaxCalling" md-diameter="40px"></md-progress-circular>
    </md-content>
    <!--<?= CPasswordHelper::hashPassword("Vialestelvio45"); ?>-->
  </form>
</div>