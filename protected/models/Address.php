<?php

/**
 * This is the model class for table "Address".
 *
 * The followings are the available columns in table 'Address':
 * @property integer $id_address
 * @property integer $id_country
 * @property string $street
 * @property string $houseNumber
 * @property string $city
 * @property string $zip
 * @property string $state
 * @property string $country
 * @property string $directions
 * @property string $lat
 * @property string $lng
 *
 * The followings are the available model relations:
 * @property Country $idCountry
 * @property Announcement[] $announcements
 */
class Address extends CActiveRecord{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'Address';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			/*array('id_address', 'required'),*/
			array('id_address, id_address, id_country', 'numerical', 'integerOnly'=>true),
			array('street, houseNumber, city, zip, state, country', 'length', 'max'=>100),
			array('directions', 'length', 'max'=>500),
			array('lat, lng', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_address, id_country, street, houseNumber, city, zip, state, country, lat, lng', 'safe', 'on'=>'search'),
		);
	}

  public function beforeSave() {
    $latlng = $this->geocodeLatLng($this->getFormattedAddress());
    $this->lat = $latlng[0];
    $this->lng = $latlng[1];
    
    return parent::beforeSave();
  }
  
  public function getFormattedAddress(){
    return $this->street." ".$this->houseNumber.", ".$this->zip." ".$this->city.", ".$this->state.", ".$this->state;
  }
  
  public static function geocodeLatLng($address){
		$address = str_replace(" ", "+", $address);
		
		$url = "https://maps.google.com/maps/api/geocode/json?address=".$address."&sensor=false&language=en&key=".Yii::app()->params['googleApiKey'];
		$result = file_get_contents($url);
		
    $json = json_decode($result);
		
		$result = $json->results[0];

		$lat = $result->geometry->location->lat;
		$lng = $result->geometry->location->lng;
		return array($lat, $lng);
	}
  
	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCountry' => array(self::BELONGS_TO, 'Country', 'id_country'),
			'announcements' => array(self::HAS_MANY, 'Announcement', 'id_address'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id_address' => 'Id Address',
			'id_country' => 'Id Country',
			'line1' => 'Line1',
			'line2' => 'Line2',
			'lat' => 'Lat',
			'lng' => 'Lng',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_address',$this->id_address);
		$criteria->compare('id_country',$this->id_country);
		$criteria->compare('line1',$this->line1,true);
		$criteria->compare('line2',$this->line2,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lng',$this->lng,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Address the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
