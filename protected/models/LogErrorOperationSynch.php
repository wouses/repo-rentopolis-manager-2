<?php

/**
 * This is the model class for table "LogErrorOperationSynch".
 *
 * The followings are the available columns in table 'LogErrorOperationSynch':
 * @property integer $id_operation
 * @property integer $id_channelManager
 * @property integer $id_reservation
 * @property string $sended
 * @property string $message
 */
class LogErrorOperationSynch extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LogErrorOperationSynch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_channelManager, id_reservation, sended, message', 'required'),
			array('id_channelManager, id_user, id_reservation', 'numerical', 'integerOnly'=>true),
			array('sended, message', 'safe'),
			array('utcDate', 'default', 'value' => new CDbExpression('UTC_TIMESTAMP()')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_operation, id_channelManager, id_reservation, sended, message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_operation' => 'Id Operation',
			'id_channelManager' => 'Id Channel Manager',
			'id_reservation' => 'Id Reservation',
			'sended' => 'Sended',
			'message' => 'Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_operation',$this->id_operation);
		$criteria->compare('id_channelManager',$this->id_channelManager);
		$criteria->compare('id_reservation',$this->id_reservation);
		$criteria->compare('sended',$this->sended,true);
		$criteria->compare('message',$this->message,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogErrorOperationSynch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
