<?php

/**
 * This is the model class for table "ReservationMapSynch".
 *
 * The followings are the available columns in table 'ReservationMapSynch':
 * @property integer $id_reservationMapSynch
 * @property integer $id_reservation
 * @property string $VREASY_2
 * @property string $WUBOOK_3
 * @property string $VREASY_5
 *
 * The followings are the available model relations:
 * @property Reservation $idReservation
 */
class ReservationMapSynch extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ReservationMapSynch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_reservation, VREASY_2, WUBOOK_3, VREASY_5', 'required'),
			array('id_reservation', 'numerical', 'integerOnly'=>true),
			array('VREASY_2, WUBOOK_3, VREASY_5', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_reservationMapSynch, id_reservation, VREASY_2, WUBOOK_3, VREASY_5', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reservation' => array(self::BELONGS_TO, 'Reservation', 'id_reservation'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_reservationMapSynch' => 'Id Reservation Map Synch',
			'id_reservation' => 'Id Reservation',
			'VREASY_2' => 'Vreasy 2',
			'WUBOOK_3' => 'Wubook 3',
			'VREASY_5' => 'Vreasy 5',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_reservationMapSynch',$this->id_reservationMapSynch);
		$criteria->compare('id_reservation',$this->id_reservation);
		$criteria->compare('VREASY_2',$this->VREASY_2,true);
		$criteria->compare('WUBOOK_3',$this->WUBOOK_3,true);
		$criteria->compare('VREASY_5',$this->VREASY_5,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReservationMapSynch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
