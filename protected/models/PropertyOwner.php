<?php

/**
 * This is the model class for table "PropertyOwner".
 *
 * The followings are the available columns in table 'PropertyOwner':
 * @property integer $id_propertyOwner
 * @property integer $id_businessPartner
 * @property integer $defaultLeaseType
 *
 * The followings are the available model relations:
 * @property BusinessPartner $idBusinessPartner
 */
class PropertyOwner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PropertyOwner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_propertyOwner', 'required'),
			array('id_propertyOwner, id_businessPartner, defaultLeaseType', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_propertyOwner, id_businessPartner, defaultLeaseType', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBusinessPartner' => array(self::BELONGS_TO, 'BusinessPartner', 'id_businessPartner'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_propertyOwner' => 'Id Property Owner',
			'id_businessPartner' => 'Id Business Partner',
			'defaultLeaseType' => 'Default Lease Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_propertyOwner',$this->id_propertyOwner);
		$criteria->compare('id_businessPartner',$this->id_businessPartner);
		$criteria->compare('defaultLeaseType',$this->defaultLeaseType);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PropertyOwner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
