<?php

/**
 * This is the model class for table "ChannelManagerChannelAssociation".
 *
 * The followings are the available columns in table 'ChannelManagerChannelAssociation':
 * @property integer $id_channelAssociation
 * @property integer $id_channelManager
 * @property integer $id_channel
 * @property string $referement
 *
 * The followings are the available model relations:
 * @property Channel $idChannel
 * @property ChannelManager $idChannelManager
 * @property Reservation[] $reservations
 */
class ChannelManagerChannelAssociation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ChannelManagerChannelAssociation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_channelAssociation', 'required'),
			array('id_channelAssociation, id_channelManager, id_channel', 'numerical', 'integerOnly'=>true),
			array('referement', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_channelAssociation, id_channelManager, id_channel, referement', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idChannelManager' => array(self::BELONGS_TO, 'ChannelManager', 'id_channelManager'),
			'channel' => array(self::BELONGS_TO, 'Channel', 'id_channel'),
			'channelManager' => array(self::BELONGS_TO, 'ChannelManager', 'id_channelManager'),
			'reservations' => array(self::HAS_MANY, 'Reservation', 'id_channel'),
		);
	}

  public function getChannelName(){
    return $this->channel()->channelName;
  }
  
  public function getChannelImage(){
    return $this->channel()->image;
  }
  
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_channelAssociation' => 'Id Channel Association',
			'id_channelManager' => 'Id Channel Manager',
			'id_channel' => 'Id Channel',
			'referement' => 'Referement',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_channelAssociation',$this->id_channelAssociation);
		$criteria->compare('id_channelManager',$this->id_channelManager);
		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('referement',$this->referement,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChannelManagerChannelAssociation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
