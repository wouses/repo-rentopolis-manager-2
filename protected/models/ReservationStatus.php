<?php

/**
 * This is the model class for table "ReservationStatus".
 *
 * The followings are the available columns in table 'ReservationStatus':
 * @property integer $id_reservationStatus
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Reservation[] $reservations
 */
class ReservationStatus extends CActiveRecord{

	public function tableName(){
		return 'ReservationStatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){

		return array(
			array('id_reservationStatus', 'required'),
			array('id_reservationStatus', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('description', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_reservationStatus, name', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reservations' => array(self::HAS_MANY, 'Reservation', 'id_reservationStatus'),
		);
	}

	public function attributeLabels(){
		return array(
			'id_reservationStatus' => 'Id Reservation Status',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_reservationStatus',$this->id_reservationStatus);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

  public static function isBookableStatus($id){
    return ($id != 1 && $id != 6);
  }
  
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReservationStatus the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
