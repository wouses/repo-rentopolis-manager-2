<?php

/**
 * This is the model class for table "AnnouncementSettings".
 *
 * The followings are the available columns in table 'AnnouncementSettings':
 * @property integer $id_instance
 * @property integer $id_field
 * @property integer $id_announcement
 * @property string $value
 * @property integer $isValid
 *
 * The followings are the available model relations:
 * @property AnnouncementSettingsFormBuilder $idField
 * @property Announcement $idAnnouncement
 */
class AnnouncementSettings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'AnnouncementSettings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_field, id_announcement', 'required'),
			array('id_instance, id_field, id_announcement, isValid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_instance, id_field, id_announcement, value, isValid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'field' => array(self::BELONGS_TO, 'AnnouncementSettingsFormBuilder', 'id_field'),
			'announcement' => array(self::BELONGS_TO, 'Announcement', 'id_announcement'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_instance' => 'Id Instance',
			'id_field' => 'Id Field',
			'id_announcement' => 'Id Announcement',
			'value' => 'Value',
			'isValid' => 'Is Valid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_instance',$this->id_instance);
		$criteria->compare('id_field',$this->id_field);
		$criteria->compare('id_announcement',$this->id_announcement);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('isValid',$this->isValid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnnouncementSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
