<?php

/**
 * This is the model class for table "BusinessPartner".
 *
 * The followings are the available columns in table 'BusinessPartner':
 * @property integer $id_businessPartner
 * @property integer $id_business
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $telephones
 * @property integer $sex
 * @property string $birthDate
 * @property string $systemNote
 *
 * The followings are the available model relations:
 * @property Announcement[] $announcements
 * @property Announcement[] $announcements1
 * @property Business $idBusiness
 * @property Commission[] $commissions
 * @property Guest[] $guests
 * @property PropertyOwner[] $propertyOwners
 * @property Reservation[] $reservations
 * @property Reservation[] $reservations1
 */
class BusinessPartner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BusinessPartner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_businessPartner', 'required'),
			array('id_businessPartner, id_business, sex', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>75),
			array('surname', 'length', 'max'=>90),
			array('email, telephone, birthDate, systemNote', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_businessPartner, id_business, name, surname, email, telephone, sex, birthDate, systemNote', 'safe', 'on'=>'search'),
		);
	}

  public function getFullName(){
    $name = $this->name;
    $surname = $this->surname;
    $string = "";
    
    if(!empty($name) && !empty($surname))
      $string = $name." ".$surname;
    else if(!empty($name))
      $string = $name;
    else if(!empty($surname))
      $string = $surname;
    
    return $string;
  }
  
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'announcements' => array(self::HAS_MANY, 'Announcement', 'id_reservationManager'),
			'announcements1' => array(self::HAS_MANY, 'Announcement', 'id_host'),
			'announcementsOwned' => array(self::HAS_MANY, 'Announcement', 'id_host'),
			'idBusiness' => array(self::BELONGS_TO, 'Business', 'id_business'),
			'commissions' => array(self::HAS_MANY, 'Commission', 'id_partner'),
			'guest' => array(self::HAS_ONE, 'Guest', 'id_businessPartner'),
			'user' => array(self::HAS_ONE, 'User', 'id_businessPartner'),
			'propertyOwner' => array(self::HAS_ONE, 'PropertyOwner', 'id_businessPartner'),
			'guestReservation' => array(self::HAS_MANY, 'Reservation', 'id_guest'),
			'reservations1' => array(self::HAS_MANY, 'Reservation', 'id_propertyOwner'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_businessPartner' => 'Id Business Partner',
			'id_business' => 'Id Business',
			'name' => 'Name',
			'surname' => 'Surname',
			'email' => 'Email',
			'telephone' => 'Telephone',
			'sex' => 'Sex',
			'birthDate' => 'Birth Date',
			'systemNote' => 'System Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_businessPartner',$this->id_businessPartner);
		$criteria->compare('id_business',$this->id_business);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('birthDate',$this->birthDate,true);
		$criteria->compare('systemNote',$this->systemNote,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BusinessPartner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
