<?php

/**
 * This is the model class for table "ChannelManager".
 *
 * The followings are the available columns in table 'ChannelManager':
 * @property integer $id_channelManager
 * @property string $chmName
 * @property string $name
 * @property string $class
 * @property string $websiteUri
 *
 * The followings are the available model relations:
 * @property AnnouncementChannelManager[] $announcementChannelManagers
 * @property BusinessChannelManager[] $businessChannelManagers
 * @property ChannelManagerChannelAssociation[] $channelManagerChannelAssociations
 */
class ChannelManager extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'ChannelManager';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_channelManager', 'required'),
			array('id_channelManager', 'numerical', 'integerOnly'=>true),
			array('chmName', 'length', 'max'=>50),
			array('name, class', 'length', 'max'=>45),
			array('apiKey', 'length', 'max'=>100),
			array('password', 'length', 'max'=>100),
			array('websiteUri', 'length', 'max'=>60),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_channelManager, name, class, websiteUri', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'announcementChannelManagers' => array(self::HAS_MANY, 'AnnouncementChannelManager', 'id_channelManager'),
			'businessChannelManagers' => array(self::HAS_MANY, 'BusinessChannelManager', 'id_channelManager'),
			'channelManagerChannelAssociations' => array(self::HAS_MANY, 'ChannelManagerChannelAssociation', 'id_channelManager'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_channelManager' => 'Id Channel Manager',
			'name' => 'Name',
			'class' => 'Class',
			'websiteUri' => 'Website Uri',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_channelManager',$this->id_channelManager);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('websiteUri',$this->websiteUri,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChannelManager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
