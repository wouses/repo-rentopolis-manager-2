<?php

/**
 * This is the model class for table "AnnouncementChannelManager".
 *
 * The followings are the available columns in table 'AnnouncementChannelManager':
 * @property integer $id_announcementChannelManager
 * @property integer $id_channelManager
 * @property integer $id_announcement
 * @property string $remoteReferement
 *
 * The followings are the available model relations:
 * @property Announcement $idAnnouncement
 * @property ChannelManager $idChannelManager
 */
class AnnouncementChannelManager extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'AnnouncementChannelManager';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_channelManager, id_announcement, id_announcementChannelManager', 'numerical', 'integerOnly'=>true),
			array('alias, aliasDescription', 'length', 'max'=>90),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_announcementChannelManager, id_channelManager, id_announcement, remoteReferement', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'announcement' => array(self::BELONGS_TO, 'Announcement', 'id_announcement'),
			'channelManager' => array(self::BELONGS_TO, 'ChannelManager', 'id_channelManager'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_announcementChannelManager' => 'Id Announcement Channel Manager',
			'id_channelManager' => 'Id Channel Manager',
			'id_announcement' => 'Id Announcement',
			'remoteReferement' => 'Remote Referement',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_announcementChannelManager',$this->id_announcementChannelManager);
		$criteria->compare('id_channelManager',$this->id_channelManager);
		$criteria->compare('id_announcement',$this->id_announcement);
		$criteria->compare('remoteReferement',$this->remoteReferement,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnnouncementChannelManager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
