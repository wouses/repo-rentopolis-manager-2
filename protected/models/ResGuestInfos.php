<?php

/**
 * This is the model class for table "ResGuestInfos".
 *
 * The followings are the available columns in table 'ResGuestInfos':
 * @property integer $id_resGuestInfos
 * @property integer $id_guest
 * @property integer $stayTaxCurrency
 * @property integer $childsNumber
 * @property integer $adultsNumber
 * @property integer $babiesNumber
 * @property string $reservationComment
 * @property integer $stayTax
 *
 * The followings are the available model relations:
 * @property Reservation[] $reservations
 */
class ResGuestInfos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ResGuestInfos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_resGuestInfos', 'required'),
			array('id_resGuestInfos, stayTaxCurrency, childsNumber, adultsNumber, babiesNumber', 'numerical', 'integerOnly'=>true),
			array('reservationComment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_resGuestInfos, stayTaxCurrency, childsNumber, adultsNumber, babiesNumber, reservationComment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reservations' => array(self::HAS_MANY, 'Reservation', 'id_resGuestInfos'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_resGuestInfos' => 'Id Res Guest Infos',
			//'id_guest' => 'Id Guest',
			'stayTaxCurrency' => 'Stay Tax Currency',
			'childsNumber' => 'Childs Number',
			'adultsNumber' => 'Adults Number',
			'babiesNumber' => 'Babies Number',
			'reservationComment' => 'Reservation Comment',
			//'stayTax' => 'Stay Tax',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_resGuestInfos',$this->id_resGuestInfos);
		//$criteria->compare('id_guest',$this->id_guest);
		$criteria->compare('stayTaxCurrency',$this->stayTaxCurrency);
		$criteria->compare('childsNumber',$this->childsNumber);
		$criteria->compare('adultsNumber',$this->adultsNumber);
		$criteria->compare('babiesNumber',$this->babiesNumber);
		$criteria->compare('reservationComment',$this->reservationComment,true);
		//$criteria->compare('stayTax',$this->stayTax);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ResGuestInfos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
