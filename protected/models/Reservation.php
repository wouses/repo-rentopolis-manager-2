<?php

/**
 * This is the model class for table "Reservation".
 *
 * The followings are the available columns in table 'Reservation':
 * @property integer $id_reservation
 * @property integer $id_announcement
 * @property integer $id_channel
 * @property integer $id_guest
 * @property integer $id_propertyOwner
 * @property integer $id_resGuestInfos
 * @property integer $id_currency
 * @property integer $id_manager
 * @property integer $id_reservationStatus
 * @property string $channelManagerReferement
 * @property string $checkinDate
 * @property integer $checkinHour
 * @property string $checkoutDate
 * @property integer $checkoutHour
 * @property string $cleaningFee
 * @property string $net
 * @property string $channelCommission
 * @property string $isBlockedDates
 * @property string $flagPu
 *
 * The followings are the available model relations:
 * @property Commission[] $commissions
 * @property CommissionStatus[] $commissionStatuses
 * @property Announcement $idAnnouncement
 * @property BusinessPartner $idGuest
 * @property BusinessPartner $idPropertyOwner
 * @property ChannelManagerChannelAssociation $idChannel
 * @property ResGuestInfos $idResGuestInfos
 * @property Currency $idCurrency
 * @property ReservationStatus $idReservationStatus
 * @property User $idManager
 * @property Tranches[] $tranches
 */
class Reservation extends CActiveRecord{

  private $guestInfos = null;
  private $gbpInstance = null;
  private $isNewReservation = null;
  private $chmId;

  /**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'Reservation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_announcement, id_channel, id_guest, id_resGuestInfos, id_currency, id_manager, id_reservationStatus, noGuestWarning', 'numerical', 'integerOnly'=>true),
			array('channelManagerReferement', 'length', 'max'=>100),
			array('net, cleaningFee, channelCommission', 'length', 'max'=>10),

      array('bookingTime', 'length', 'max'=>19),

      array('lastModified', 'length', 'max'=>19),
      array('mapSynch', 'length', 'max'=>200),
			array('mapSynch', 'default', 'value'=> '{}'),

      array('importedDate', 'length', 'max'=>19),
			array('importedDate', 'default', 'value'=>new CDbExpression('NOW()')),

      array('noGuestWarning', 'default', 'value'=>0),
      
      array('isBlockedDates', 'default', 'value'=>0),
      array('net, cleaningFee, channelCommission, touristTax', 'default', 'value'=>0),      
      array('rrNote, cfNote, ccNote, ttNote, note', 'default', 'value' => ''),

      array('flagPu', 'default', 'value'=>0),

      array('id_currency', 'default', 'value'=>1),

			array('checkinDate, checkoutDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_reservation, id_announcement, id_channel, id_guest, id_resGuestInfos, id_currency, id_manager, id_reservationStatus, channelManagerReferement, checkinDate, checkoutDate, net, cleaningFee, channelCommission', 'safe', 'on'=>'search'),
		);
	}

  public function beforeSave() {
    if($this->isNewRecord){
      if(!empty($this->getGuestInfos())){
        $resGuestInfos = new ResGuestInfos();
        $resGuestInfos->setAttributes($this->getGuestInfos());
        if($resGuestInfos->save()){
          $this->id_resGuestInfos = $resGuestInfos->id_resGuestInfos;
        }
      }
    }
    
    return parent::beforeSave();
  }

  /* afterSave()
   * 
   * Normalize commissionInstances for the correct contract.
   * This method creates all commission instances. A factory method will be called.
   */
  public function afterSave(){
    parent::afterSave();
    $disabled = true; //todo refactor
    if ($this->isNewRecord && !$disabled){
      YiiBase::import("application.controllers.AnnouncementController");

      $announcementId = $this->announcement()->id_announcement;
      $fromIso = (new DateTime())->format('Y-m-d');//TODO REFACTORING: change this with the creation date retrieved from the channel managers

      $announcement = AnnouncementController::getAnnouncementContracts($announcementId, $fromIso);
      if($announcement === null){ 
        /* TODO: LOG AN IMPORTANT ERROR */
      }else{
        $contract = $announcement->contracts()[0]; //the first contract is the needed commissions set.
        $commissions = $contract->commissions();

        foreach($commissions as $commission){
          $commissionInstance = CommissionInstance::generate($commission, $this);
          if(isset($commissionInstance["errors"])){ /*TODO LOG: LOG AN IMPORTANT ERROR*/ }
        }
      }
    }
  }

  /* Reservation detail check
   * -If comparedAttributes is specified only those attributes will compared with them.
   * -
   */
  
  public function hasChanges($comparedAttributes = array()){
    $reservation = $this->newInstance;
    
    if($reservation->channel()->channelManager()->chmName == "WUBOOK"){
      //from wubook we want only detect changes in cancellation status.
      if($reservation->id_reservationStatus == 5 && $this->id_reservationStatus != $reservation->id_reservationStatus){
        return true;
      }else{
        return false;
      }
    }
    
    if(count($comparedAttributes) > 0){
      $hasChanges = false;
      foreach($comparedAttributes as $toCompare){
        if(in_array($toCompare, array("checkinDate", "checkoutDate"))){
          $formatted = (new DateTime($reservation->$toCompare))->format("Y-m-d H:i:s");
          $hasChanges = $hasChanges || ($this->$toCompare != $formatted);
        }else if(in_array($toCompare, array("net", "cleaningFee", "channelCommission"))){
          $hasChanges = $hasChanges || (floatval($this->$toCompare) != floatval($reservation->$toCompare));
        }else{
          $hasChanges = $hasChanges || ($this->$toCompare != $reservation->$toCompare);
        }
        if($hasChanges) return true;
      }
      return false;
    }
    
    $checkinFormatted = (new DateTime($reservation->checkinDate))->format("Y-m-d H:i:s");
    $checkoutFormatted = (new DateTime($reservation->checkoutDate))->format("Y-m-d H:i:s");

    $hasChanges = $this->checkinDate != $checkinFormatted;

    if(intval($this->flagPu) == 0){
      $hasChanges = $hasChanges || floatval($this->net) != floatval($reservation->net);
      $hasChanges = $hasChanges || floatval($this->cleaningFee) != floatval($reservation->cleaningFee);
      $hasChanges = $hasChanges || floatval($this->channelCommission) != floatval($reservation->channelCommission);
    }

    $hasChanges = $hasChanges || $this->checkoutDate != $checkoutFormatted;
    $hasChanges = $hasChanges || $this->id_reservationStatus != $reservation->id_reservationStatus;
    $hasChanges = $hasChanges || $this->id_announcement != $reservation->id_announcement;
    $hasChanges = $hasChanges || $this->id_channel != $reservation->id_channel;

    $hasChanges = $hasChanges || $this->note != $reservation->note;
    
    /*Reservation guest check*/
    $hasChanges = $hasChanges || $this->guest()->name != $reservation->getGuestInstance()->name;
    $hasChanges = $hasChanges || $this->guest()->surname != $reservation->getGuestInstance()->surname;
    $hasChanges = $hasChanges || $this->guest()->telephone != $reservation->getGuestInstance()->telephone;
    $hasChanges = $hasChanges || $this->guest()->email != $reservation->getGuestInstance()->email;
    
    $hasChanges = $hasChanges || $this->isBlockedDates != $reservation->isBlockedDates;
    
    return $hasChanges;
  }

  public function isImportable(){
    //we can import only confirmed, enquiry an unavailable ranges.
    return 
      ($this->id_reservationStatus == 1 || $this->id_reservationStatus == 4 || $this->id_reservationStatus == 6);
  }

  private $newInstance;
  public function updateNewAttributes(){
    //only if the price details wasn't updated before. It is an important flow.
    if(intval($this->flagPu) == 0){
      $this->net = $this->newInstance->net;
      $this->cleaningFee = $this->newInstance->cleaningFee;
      $this->channelCommission = $this->newInstance->channelCommission;
    }
    
    $this->checkinDate = $this->newInstance->checkinDate;
    $this->checkoutDate = $this->newInstance->checkoutDate;
    $this->id_reservationStatus = $this->newInstance->id_reservationStatus;
    $this->id_announcement = $this->newInstance->id_announcement;
    $this->id_channel = $this->newInstance->id_channel;
    
    $guest = $this->guest();
    $newGuest = $this->newInstance->getGuestInstance();
    
    //if($guest->getFullName() != $newGuest->getFullName()){
    $guest->name = !empty($newGuest->name) ? $newGuest->name : "" ;
    $guest->surname = !empty($newGuest->surname) ? $newGuest->surname : "" ;
    $guest->telephone = !empty($newGuest->telephone) ? $newGuest->telephone : "" ;
    $guest->email = !empty($newGuest->email) ? $newGuest->email : "";
    //}
    
    $this->note = $this->newInstance->note;
    
    $this->isBlockedDates = $this->newInstance->isBlockedDates;
  }
  
  public function setNewInstance($reservation){
    if(strpos($reservation->checkinDate, "T")){
      $date = $reservation->checkinDate;
      $reservation->checkinDate = substr($date, 0, 10)." ".substr($date, 11, 8);
    }

    if(strpos($reservation->checkoutDate, "T")){
      $date = $reservation->checkoutDate;
      $reservation->checkoutDate = substr($date, 0, 10)." ".substr($date, 11, 8);
    }
    
    $this->newInstance = $reservation;
  }
  
  public function saveGuest(){
    /*Save the business Partner*/
    $bp = $this->getGuestInstance();
    $bp->save();
    $this->id_guest = $bp->id_businessPartner;
    $this->update();
    
    /*Save Guest association for the Business Partner*/
    $guest = new Guest();
    $guest->id_businessPartner = $bp->id_businessPartner;
    $guest->travels = 1;
    $guest->save();
  }
  
  public function setGuestInfos($gi){
    $this->guestInfos = $gi;
  }
  
  public function getGuestInfos(){
    return $this->guestInfos;
  }
  
  public function setGuestInstance($instance){
    $this->gbpInstance = $instance;
  }
    
  public function getGuestInstance(){
    return $this->gbpInstance;
  }
  
  public function getChannelManagerName(){
    $channel = $this->channel();
    if($channel === null) return null;
    $channelManager = $channel->channelManager();
    if($channelManager === null) return null;
    
    return $channelManager->chmName;
  }
  
  public function setChmId($id){
    $this->chmId = $id;
  }
  
  public function getChmId(){
    return $this->chmId;
  }
  
  public function setIsNewReservation($flag){
    $this->isNewReservation = $flag;
  }
  
  public function getIsNewReservation(){
    return $this->isNewReservation;
  }
  
  public function getGuest(){
    $guest = $this->guest();
    if($guest !== null)
      return $guest->guest();
    return null;
  }
  
  public function getFirstNight($format = null){
    $firstNight = new DateTime($this->checkinDate);
    
    if(!empty($format)){
      return $firstNight->format($format);
    }
    
    return $firstNight;
  }

  public function getLastNight($format = null){
    $one = new DateInterval("P1D");
    
    $lastNight = new DateTime($this->checkoutDate);
    $lastNight->sub($one);
    
    if(!empty($format)){
      return $lastNight->format($format);
    }
    
    return $lastNight;
  }
  
  public function getTotalNights(){
    $dt1 = new DateTime((new DateTime($this->checkinDate))->format("Y-m-d"));
    $dt2 = new DateTime((new DateTime($this->checkoutDate))->format("Y-m-d"));
    $interval = $dt2->diff($dt1);
    
    return $interval->format('%a');
  }
  
  public function updateReservationPaymentsInRulesRespect($gross, $isToUpdate=true){
    $announcement = $this->announcement();
    $channel = $this->channel()->channel();
    $bookingTime = $this->bookingTime;
    
    $announcementRule = $announcement->getChannelsRule($bookingTime);
    $channelRule = $channel->getChannelRule($bookingTime);
    
    if($channelRule == null)
      $channelRule = ChannelRules::getDefaultRule();
    
    //Expedia channel ; id = 10
    //Booking.com channel ; id = 9
    
    $cleaning = $announcementRule->cleaning;
    if(intval($channel->id_channel) == 10) $cleaning = $announcementRule->expediaCleaning;

    if(intval($channelRule->cleaningIncluded))
      $rent = bcsub($gross, $cleaning, 4);
    else
      $rent = bcadd($gross, "0.0000", 4);

    $relativeCommission = $channelRule->defaultCommission;
    if(intval($channel->id_channel) == 9 && floatval($announcementRule->bookingcomCommission) > 0)
      $relativeCommission = $announcementRule->bookingcomCommission;

    $percentageCommission = bcdiv($relativeCommission, "100", 4);
    
    if(intval($channelRule->channelCommissionIncluded) == 1){
      $commission = bcmul($rent, $percentageCommission, 4);
      $rent = bcsub($rent, $commission, 4);
    }else{ //commission in excluded
      $commission = bcsub(bcdiv($rent, bcsub("1.0000", $percentageCommission, 4), 4), $rent, 4);
    }

    $payments = array(
      "net" => $rent,
      "cleaningFee" => $cleaning,
      "channelCommission" => $commission 
    );
    
    if($isToUpdate){
      $this->net = $payments["net"];
      $this->cleaningFee = $payments["cleaningFee"];
      $this->channelCommission = $payments["channelCommission"];
    }

    return $payments;
  }

  public function getHostEarnings(){
    $announcement = $this->announcement();

    if(!empty($this->bookingTime)){
      $announcementRule = $announcement->getChannelsRule($this->bookingTime);
    }else{
      $announcementRule = $announcement->getChannelsRule($this->checkinDate);
    }

    $rentopolisCommission = $announcementRule->rentopolisCommission;

    $hownerNet = $this->net;// - $rs->channelCommission;
    $hownerPercentage = bcmul(bcdiv(bcsub("100", $rentopolisCommission), "100", 4), $hownerNet, 4);
    $hownerEarnings = bcsub($hownerPercentage, "24.40", 2);

    return $hownerEarnings;
  }

  public function updateMapSynch($conf, $value, $isSynched){
    $decoded = json_decode($this->mapSynch);
    $decoded->$conf = array("code" => $value, "isSynched" => $isSynched, "utcDate" => gmdate("Y-m-d H:i:s", time()));
    $this->mapSynch = json_encode($decoded);
    $this->save();
  }

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'commissions' => array(self::HAS_MANY, 'Commission', 'id_reservation'),
			'commissionInstance' => array(self::HAS_MANY, 'CommissionInstance', 'id_reservation'),
      'announcement' => array(self::BELONGS_TO, 'Announcement', 'id_announcement'),
      'guest' => array(self::BELONGS_TO, 'BusinessPartner', 'id_guest'),
			//'guest' => array(self::MANY_ONE, 'Guest', 'BusinessPartner(id_announcement, id_contract)'),
      //'propertyOwner' => array(self::BELONGS_TO, 'BusinessPartner', 'id_propertyOwner'),
			'channel' => array(self::BELONGS_TO, 'ChannelManagerChannelAssociation', 'id_channel'),
			'resGuestInfos' => array(self::BELONGS_TO, 'ResGuestInfos', 'id_resGuestInfos'),
			'currency' => array(self::BELONGS_TO, 'Currency', 'id_currency'),
			'reservationStatus' => array(self::BELONGS_TO, 'ReservationStatus', 'id_reservationStatus'),
			'manager' => array(self::BELONGS_TO, 'User', 'id_manager'),
			'tranches' => array(self::HAS_MANY, 'Tranches', 'id_reservation'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id_reservation' => 'Id Reservation',
			'id_announcement' => 'Id Announcement',
			'id_channel' => 'Id Channel',
			'id_guest' => 'Id Guest',
			//'id_propertyOwner' => 'Id Property Owner',
			'id_resGuestInfos' => 'Id Res Guest Infos',
			'id_currency' => 'Id Currency',
			'id_manager' => 'Id Manager',
			'id_reservationStatus' => 'Id Reservation Status',
			'channelManagerReferement' => 'Channel Manager Riferement',
			'checkinDate' => 'Checkin Date',
			//'checkinHour' => 'Checkin Hour',
			'checkoutDate' => 'Checkout Date',
			//'checkoutHour' => 'Checkout Hour',
			'net' => 'Net Earnings',
			'cleaningFee' => 'Reservation Cleaning Fee',
			'channelCommission' => 'Channel Commission',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_reservation',$this->id_reservation);
		$criteria->compare('id_announcement',$this->id_announcement);
		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('id_guest',$this->id_guest);
		//$criteria->compare('id_propertyOwner',$this->id_propertyOwner);
		$criteria->compare('id_resGuestInfos',$this->id_resGuestInfos);
		$criteria->compare('id_currency',$this->id_currency);
		$criteria->compare('id_manager',$this->id_manager);
		$criteria->compare('id_reservationStatus',$this->id_reservationStatus);
		$criteria->compare('channelManagerReferement',$this->channelManagerReferement,true);
		$criteria->compare('checkinDate',$this->checkinDate,true);
		//$criteria->compare('checkinHour',$this->checkinHour);
		$criteria->compare('checkoutDate',$this->checkoutDate,true);
		//$criteria->compare('checkoutHour',$this->checkoutHour);
		$criteria->compare('cleaningFee',$this->cleaningFee,true);
		$criteria->compare('net',$this->net,true);
		$criteria->compare('channelCommission',$this->channelCommission,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

  public function synchReservation($reservationOldValues, $synchPricing, $chmException = array()){
    $listing = $this->id_announcement;
    $configuration = AnnouncementController::getAnnouncementChannelManagerConfiguration($listing);

    /*$decoded = json_decode($this->mapSynch);
    foreach($decoded as $chm => $ist){
      $this->updateMapSynch($chm, $ist->code, 0);
      $this->update();
    }*/
    
    $generalError = false;
    //var_dump($chmException);
    
    foreach($configuration as $conf){
      if($conf["hasClass"] && $conf["activated"]){
        $chm = ChannelManager::model()->findByAttributes(array("id_channelManager" => $conf["channelManagerId"]));
        
        if(in_array($chm->id_channelManager, $chmException)) continue;

        $className = $chm->getAttribute("class");
        $instanceNumber = $chm->getAttribute("id_channelManager");

        $remoteChannelManager = new $className($instanceNumber); //new instance for VREasy, WuBook, etc...
        
        //bypass the reference parameter object
        $toConvert = new Reservation();
        $toConvert->setAttributes($this->getAttributes());
        $toConvert->id_reservation = $this->id_reservation;
        
        $remoteReservation = $remoteChannelManager->convertToRemoteReservation($toConvert, $synchPricing);
        
        $result = null;
        $resulted = null;
        
        if(Yii::app() instanceof CConsoleApplication){
          $user = null;
        }else{
          $user = User::model()->findByAttributes(array("username" => Yii::app()->user->id));
        }
        
        $logData = array(
          "id_channelManager"=> $conf["channelManagerId"], 
      -    "id_reservation" => $this->id_reservation
        );
        
        if($user !== null){
          $logData["id_user"] = $user->id_user;
        }
        
        $error = false;
        
        //if($this->getIsNewReservation()){
        if(!$this->isSynched($conf["channelManagerId"])){
          $resulted = $remoteChannelManager->createRemoteReservation($remoteReservation);
          
          $result = $resulted["received"];
          if(!isset($result->id)) $error = true;
          else $this->updateMapSynch($conf["channelManagerId"], $result->id, 1);
        }else{
          $resulted = $remoteChannelManager->updateRemoteReservation($remoteReservation, $reservationOldValues);
          
          $result = $resulted["received"];
          if(!isset($result->id)) $error = true;
          else $this->updateMapSynch($conf["channelManagerId"], $result->id, 1);
        }

        $logData["sended"] = json_encode($resulted["sended"]);
        $logData["message"] = json_encode($resulted["received"]);
        if($error){
          $log = new LogErrorOperationSynch();
          $generalError = true;
        }else{
          $log = new LogOperationSynch();
        }
        
        $log->attributes = $logData;
        $log->id_reservation = $this->id_reservation;
        
        $log->save();
          //LogErrorOperationSynch
      }
    }
    
    if(empty($this->channelManagerReferement)){
      $map = json_decode($this->mapSynch);
      $chmId = $this->channel()->id_channelManager;
      if(isset($map->$chmId) && isset($map->$chmId->code)){
        $this->channelManagerReferement = $map->$chmId->code;
        $this->update();
      }
    }
    
    /*return if some error occurred*/
    return $generalError;
  }
  
  public function isSynched($chm){
    $decoded = json_decode($this->mapSynch);
    $chm = strval($chm);

    return isset($decoded->$chm->code) || 
      (($this->channel()->id_channelManager == $chm) && !empty($this->channelManagerReferement)); 
  }
  
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reservation the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
