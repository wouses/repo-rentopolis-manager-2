<?php

/**
 * This is the model class for table "ChannelRules".
 *
 * The followings are the available columns in table 'ChannelRules':
 * @property integer $id_channelRule
 * @property integer $id_channel
 * @property integer $cleaningIncluded
 * @property integer $channelCommissionIncluded
 * @property integer $defaultCommission
 *
 * The followings are the available model relations:
 * @property Channel $idChannel
 */
class ChannelRules extends CActiveRecord{
	
  public static function getDefaultRule(){
    $chr = new ChannelRules();
    $chr->cleaningIncluded = 0;
    $chr->channelCommissionIncluded = 1;
    $chr->defaultCommission = 0;
    return $chr;
  }
  
  /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ChannelRules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_channel, cleaningIncluded, channelCommissionIncluded, defaultCommission, validFrom', 'required'),
			array('id_channel, cleaningIncluded, channelCommissionIncluded, defaultCommission', 'numerical', 'integerOnly'=>true),
			array('validFrom', 'length', 'max' => 20),
      
      // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_channelRule, id_channel, cleaningIncluded, channelCommissionIncluded, defaultCommission', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idChannel' => array(self::BELONGS_TO, 'Channel', 'id_channel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_channelRule' => 'Id Channel Rule',
			'id_channel' => 'Id Channel',
			'cleaningIncluded' => 'Cleaning Included',
			'channelCommissionIncluded' => 'Channel Commission Included',
			'defaultCommission' => 'Default Commission',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_channelRule',$this->id_channelRule);
		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('cleaningIncluded',$this->cleaningIncluded);
		$criteria->compare('channelCommissionIncluded',$this->channelCommissionIncluded);
		$criteria->compare('defaultCommission',$this->defaultCommission);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChannelRules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
