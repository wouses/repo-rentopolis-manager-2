<?php

/**
 * This is the model class for table "PaymentChannelAccount".
 *
 * The followings are the available columns in table 'PaymentChannelAccount':
 * @property integer $id_paymentChannelAccount
 * @property integer $id_business
 * @property string $name
 * @property string $apiUsr
 * @property string $apiPsw
 * @property string $apiLink
 *
 * The followings are the available model relations:
 * @property Business $idBusiness
 * @property Tranches[] $tranches
 */
class PaymentChannelAccount extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PaymentChannelAccount';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_paymentChannelAccount', 'required'),
			array('id_paymentChannelAccount, id_business', 'numerical', 'integerOnly'=>true),
			array('name, apiUsr, apiPsw, apiLink', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_paymentChannelAccount, id_business, name, apiUsr, apiPsw, apiLink', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBusiness' => array(self::BELONGS_TO, 'Business', 'id_business'),
			'tranches' => array(self::HAS_MANY, 'Tranches', 'id_paymentChannelAccount'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_paymentChannelAccount' => 'Id Payment Channel Account',
			'id_business' => 'Id Business',
			'name' => 'Name',
			'apiUsr' => 'Api Usr',
			'apiPsw' => 'Api Psw',
			'apiLink' => 'Api Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_paymentChannelAccount',$this->id_paymentChannelAccount);
		$criteria->compare('id_business',$this->id_business);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('apiUsr',$this->apiUsr,true);
		$criteria->compare('apiPsw',$this->apiPsw,true);
		$criteria->compare('apiLink',$this->apiLink,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentChannelAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
