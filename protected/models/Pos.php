<?php

/**
 * This is the model class for table "Pos".
 *
 * The followings are the available columns in table 'Pos':
 * @property integer $id_pos
 * @property integer $id_bank
 * @property string $name
 * @property string $bankReferement
 *
 * The followings are the available model relations:
 * @property BankAccount $idBank
 * @property Tranches[] $tranches
 */
class Pos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Pos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pos', 'required'),
			array('id_pos, id_bank', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('bankReferement', 'length', 'max'=>90),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pos, id_bank, name, bankReferement', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBank' => array(self::BELONGS_TO, 'BankAccount', 'id_bank'),
			'tranches' => array(self::HAS_MANY, 'Tranches', 'id_pos'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pos' => 'Id Pos',
			'id_bank' => 'Id Bank',
			'name' => 'Name',
			'bankReferement' => 'Bank Referement',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pos',$this->id_pos);
		$criteria->compare('id_bank',$this->id_bank);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('bankReferement',$this->bankReferement,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
