<?php

/**
 * This is the model class for table "CommissionStatus".
 *
 * The followings are the available columns in table 'CommissionStatus':
 * @property integer $id_commissionStatus
 * @property string $name
 *
 * The followings are the available model relations:
 * @property CommissionInstance[] $commissionInstances
 */
class CommissionStatus extends CActiveRecord{
	
  public function tableName(){
		return 'CommissionStatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>15),
			
      array('id_commissionStatus, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		return array(
			'commissionInstances' => array(self::HAS_MANY, 'CommissionInstance', 'id_status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id_commissionStatus' => 'Id Commission Status',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_commissionStatus',$this->id_commissionStatus);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CommissionStatus the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
