<?php

/**
 * This is the model class for table "Contract".
 *
 * The followings are the available columns in table 'Contract':
 * @property integer $id_contract
 * @property string $name
 * @property string $fromValidityDate
 *
 * The followings are the available model relations:
 * @property AnnouncementContract[] $announcementContracts
 * @property Commission[] $commissions
 */
class Contract extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Contract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_contract', 'required'),
			array('id_contract', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>90),
			array('fromValidityDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_contract, name, fromValidityDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'announcementContracts' => array(self::HAS_MANY, 'AnnouncementContract', 'id_contract'),
			'commissions' => array(self::HAS_MANY, 'Commission', 'id_contract'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_contract' => 'Id Contract',
			'name' => 'Name',
			'fromValidityDate' => 'From Validity Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_contract',$this->id_contract);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('fromValidityDate',$this->fromValidityDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
