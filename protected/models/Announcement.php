<?php

/**
 * This is the model class for table "Announcement".
 *
 * The followings are the available columns in table 'Announcement':
 * @property integer $id_announcement
 * @property integer $id_host
 * @property integer $id_address
 * @property integer $id_business
 * @property integer $id_reservationManager
 * @property string $title
 * @property string $channelReferement
 *
 * The followings are the available model relations:
 * @property Address $idAddress
 * @property Business $idBusiness
 * @property BusinessPartner $idReservationManager
 * @property BusinessPartner $idHost
 * @property AnnouncementChannelManager[] $announcementChannelManagers
 * @property AnnouncementContract[] $announcementContracts
 * @property ChannelCancellationPolicy[] $channelCancellationPolicies
 * @property Reservation[] $reservations
 */
class Announcement extends CActiveRecord{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'Announcement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			/*array('id_announcement', 'required'),*/
			array('id_announcement, id_host, id_address, id_business, id_reservationManager', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>75),
			array('beds24ApiKey', 'length', 'min'=>16),
      array('beds24ApiKey', 'unique'),
      array('VREasyKey', 'unique'),
      array('VREasyKey', 'default', "value" => ""),
      
      array('checkin', 'default', "value" => "15:00:00"),
      array('checkout', 'default', "value" => "11:00:00"),
      
      array('checkin', 'length', 'min'=>8),
      array('checkout', 'length', 'min'=>8),
      
      /*array('channelReferement', 'length', 'max'=>90),*/
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_announcement, id_host, id_address, id_business, id_reservationManager, title, channelReferement', 'safe', 'on'=>'search'),
		);
	}

  public function getChannelsRule($datetime){
    $criteria = new CDbCriteria();
    $criteria->condition = "id_announcement = :listing AND validFrom <= :datetime";
    $criteria->params = array(":listing" => $this->id_announcement, ":datetime" => $datetime);
    $criteria->order = "validFrom DESC";
    $rules = AnnouncementChannelRules::model()->findAll($criteria);
    
    if(empty($rules))
      return null;
    else
      return $rules[0];
  }

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'address' => array(self::BELONGS_TO, 'Address', 'id_address'),
			'business' => array(self::BELONGS_TO, 'Business', 'id_business'),
      'contracts' => array(self::MANY_MANY, 'Contract', 'AnnouncementContract(id_announcement, id_contract)'),
			'reservationManager' => array(self::BELONGS_TO, 'BusinessPartner', 'id_reservationManager'),
			'host' => array(self::BELONGS_TO, 'BusinessPartner', 'id_host'),
			'announcementChannelManagers' => array(self::HAS_MANY, 'AnnouncementChannelManager', 'id_announcement'),
			'announcementContracts' => array(self::HAS_MANY, 'AnnouncementContract', 'id_announcement'),
			'channelCancellationPolicies' => array(self::HAS_MANY, 'ChannelCancellationPolicy', 'id_announcement'),
			'reservations' => array(self::HAS_MANY, 'Reservation', 'id_announcement'),
      
      'settingsFields' => array(
        self::HAS_MANY, 'AnnouncementSettings', 'id_announcement', 
        'with'=>'field', 'order'=>'field.pos ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_announcement' => 'Id Announcement',
			'id_host' => 'Id Host',
			'id_address' => 'Id Address',
			'id_business' => 'Id Business',
			'id_reservationManager' => 'Id Reservation Manager',
			'title' => 'Title',
			'channelReferement' => 'Channel Referement',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_announcement',$this->id_announcement);
		$criteria->compare('id_host',$this->id_host);
		$criteria->compare('id_address',$this->id_address);
		$criteria->compare('id_business',$this->id_business);
		$criteria->compare('id_reservationManager',$this->id_reservationManager);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('channelReferement',$this->channelReferement,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Announcement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
