<?php

/**
 * This is the model class for table "Channel".
 *
 * The followings are the available columns in table 'Channel':
 * @property integer $id_channel
 * @property integer $id_defaultCancellationPolicy
 * @property string $channelName
 * @property string $channelPercentageCommission
 * @property integer $withTranches
 *
 * The followings are the available model relations:
 * @property CancellationPolicy $idDefaultCancellationPolicy
 * @property ChannelCancellationPolicy[] $channelCancellationPolicies
 * @property ChannelCommission[] $channelCommissions
 * @property ChannelManagerChannelAssociation[] $channelManagerChannelAssociations
 */
class Channel extends CActiveRecord{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'Channel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_channel, id_defaultCancellationPolicy, withTranches', 'numerical', 'integerOnly'=>true),
			array('channelName', 'length', 'max'=>45),
			array('image', 'length', 'max'=>100),
			//array('channelPercentageCommission', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_channel, id_defaultCancellationPolicy, channelName, withTranches', 'safe', 'on'=>'search'),
		);
	}

  public function getChannelRule($datetime){
    $criteria = new CDbCriteria();
    $criteria->condition = "id_channel = :channelId AND validFrom <= :datetime";
    $criteria->params = array(":channelId" => $this->id_channel, ":datetime" => $datetime);
    $criteria->order = "validFrom DESC";
    $rules = ChannelRules::model()->findAll($criteria);
    
    if(empty($rules))
      return null;
    else
      return $rules[0];
  }
  
	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDefaultCancellationPolicy' => array(self::BELONGS_TO, 'CancellationPolicy', 'id_defaultCancellationPolicy'),
			'channelCancellationPolicies' => array(self::HAS_MANY, 'ChannelCancellationPolicy', 'id_channel'),
			'channelCommissions' => array(self::HAS_MANY, 'ChannelCommission', 'id_channel'),
			'channelManagerChannelAssociations' => array(self::HAS_MANY, 'ChannelManagerChannelAssociation', 'id_channel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id_channel' => 'Id Channel',
			'id_defaultCancellationPolicy' => 'Id Default Cancellation Policy',
			'channelName' => 'Channel Name',
			//'channelPercentageCommission' => 'Channel Percentage Commission',
			'withTranches' => 'With Tranches',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('id_defaultCancellationPolicy',$this->id_defaultCancellationPolicy);
		$criteria->compare('channelName',$this->channelName,true);
		//$criteria->compare('channelPercentageCommission',$this->channelPercentageCommission,true);
		$criteria->compare('withTranches',$this->withTranches);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Channel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
