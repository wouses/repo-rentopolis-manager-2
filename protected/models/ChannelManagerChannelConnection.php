<?php

/**
 * This is the model class for table "ChannelManagerChannelConnection".
 *
 * The followings are the available columns in table 'ChannelManagerChannelConnection':
 * @property integer $id_channelManagerConnection
 * @property integer $id_channelManager
 * @property integer $id_channel
 * @property string $validFrom
 * @property integer $connected
 *
 * The followings are the available model relations:
 * @property ChannelManager $idChannelManager
 * @property Channel $idChannel
 */
class ChannelManagerChannelConnection extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ChannelManagerChannelConnection';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_channelManager, id_channel, connected', 'numerical', 'integerOnly'=>true),
			array('validFrom', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_channelManagerConnection, id_channelManager, id_channel, validFrom, connected', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idChannelManager' => array(self::BELONGS_TO, 'ChannelManager', 'id_channelManager'),
			'channelManager' => array(self::BELONGS_TO, 'ChannelManager', 'id_channelManager'),
			'idChannel' => array(self::BELONGS_TO, 'Channel', 'id_channel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_channelManagerConnection' => 'Id Channel Manager Connection',
			'id_channelManager' => 'Id Channel Manager',
			'id_channel' => 'Id Channel',
			'validFrom' => 'Valid From',
			'connected' => 'Connected',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_channelManagerConnection',$this->id_channelManagerConnection);
		$criteria->compare('id_channelManager',$this->id_channelManager);
		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('validFrom',$this->validFrom,true);
		$criteria->compare('connected',$this->connected);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChannelManagerChannelConnection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
