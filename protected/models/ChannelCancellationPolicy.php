<?php

/**
 * This is the model class for table "ChannelCancellationPolicy".
 *
 * The followings are the available columns in table 'ChannelCancellationPolicy':
 * @property integer $id_channelCancellationPolicy
 * @property integer $id_channel
 * @property integer $id_cancellationPolicy
 * @property integer $id_announcement
 * @property string $fromDate
 *
 * The followings are the available model relations:
 * @property Announcement $idAnnouncement
 * @property CancellationPolicy $idCancellationPolicy
 * @property Channel $idChannel
 */
class ChannelCancellationPolicy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ChannelCancellationPolicy';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_channelCancellationPolicy', 'required'),
			array('id_channelCancellationPolicy, id_channel, id_cancellationPolicy, id_announcement', 'numerical', 'integerOnly'=>true),
			array('fromDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_channelCancellationPolicy, id_channel, id_cancellationPolicy, id_announcement, fromDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idAnnouncement' => array(self::BELONGS_TO, 'Announcement', 'id_announcement'),
			'idCancellationPolicy' => array(self::BELONGS_TO, 'CancellationPolicy', 'id_cancellationPolicy'),
			'idChannel' => array(self::BELONGS_TO, 'Channel', 'id_channel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_channelCancellationPolicy' => 'Id Channel Cancellation Policy',
			'id_channel' => 'Id Channel',
			'id_cancellationPolicy' => 'Id Cancellation Policy',
			'id_announcement' => 'Id Announcement',
			'fromDate' => 'From Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_channelCancellationPolicy',$this->id_channelCancellationPolicy);
		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('id_cancellationPolicy',$this->id_cancellationPolicy);
		$criteria->compare('id_announcement',$this->id_announcement);
		$criteria->compare('fromDate',$this->fromDate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChannelCancellationPolicy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
