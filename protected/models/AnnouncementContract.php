<?php

/**
 * This is the model class for table "AnnouncementContract".
 *
 * The followings are the available columns in table 'AnnouncementContract':
 * @property integer $id_contractAssociation
 * @property integer $id_announcement
 * @property integer $id_contract
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Announcement $idAnnouncement
 * @property Contract $idContract
 */
class AnnouncementContract extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'AnnouncementContract';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_contractAssociation', 'required'),
			array('id_contractAssociation, id_announcement, id_contract, active', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_contractAssociation, id_announcement, id_contract, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idAnnouncement' => array(self::BELONGS_TO, 'Announcement', 'id_announcement'),
			'idContract' => array(self::BELONGS_TO, 'Contract', 'id_contract'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_contractAssociation' => 'Id Contract Association',
			'id_announcement' => 'Id Announcement',
			'id_contract' => 'Id Contract',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_contractAssociation',$this->id_contractAssociation);
		$criteria->compare('id_announcement',$this->id_announcement);
		$criteria->compare('id_contract',$this->id_contract);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnnouncementContract the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
