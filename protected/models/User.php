<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $id_user
 * @property integer $id_business
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $approvedByAdministrators
 *
 * The followings are the available model relations:
 * @property Reservation[] $reservations
 * @property Tranches[] $tranches
 * @property Business $idBusiness
 */
class User extends CActiveRecord{

  public $_identity;
  
	public function tableName(){
		return 'User';
	}

	public function rules(){
		return array(
			array('id_user, id_business, id_businessPartner, approvedByAdministrators', 'numerical', 'integerOnly'=>true),
			array('username, password, email, role', 'length', 'max'=>100),
      array('password', 'authenticate', 'on' => 'signIn'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_user, id_business, username, password, email, approvedByAdministrators', 'safe', 'on'=>'search'),
		);
	}

  public function hashPassword($password){
		return CPasswordHelper::hashPassword($password); //default salt cost = 13
	}

	public function verifyPassword($password, $hash){
		return CPasswordHelper::verifyPassword($password, $hash);
	}
  
  public function authenticate(){
    if(!$this->hasErrors()) {
      $this->_identity=new UserIdentity($this->username, $this->password);

      if(!$this->_identity->authenticate()) {
        $this->addError('password', 'Match error.');
      }
    }
  } 
  
  public function login($forceLogin = false){
		if($this->_identity===null) {
			$this->_identity=new UserIdentity($this->username, $this->password);
			$this->_identity->authenticate($forceLogin);
		}
		
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE) {
			//$duration= $this->stayConnected ? 3600*24*30 : 0; // 30 or 0 days
			//Yii::app()->session['stayConnected'] = $this->stayConnected;
			Yii::app()->user->login($this->_identity);
			return true;
		}else{
			return false;
		}		
	}
  
	public function relations(){
		return array(
			'reservations' => array(self::HAS_MANY, 'Reservation', 'id_manager'),
			'tranches' => array(self::HAS_MANY, 'Tranches', 'id_manager'),
			'idBusiness' => array(self::BELONGS_TO, 'Business', 'id_business'),
			'businessPartner' => array(self::BELONGS_TO, 'BusinessPartner', 'id_businessPartner'),
		);
	}

	public function attributeLabels(){
		return array(
			'id_user' => 'Id User',
			'id_business' => 'Id Business',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'approvedByAdministrators' => 'Approved By Administrators',
		);
	}
  
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_business',$this->id_business);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('approvedByAdministrators',$this->approvedByAdministrators);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
