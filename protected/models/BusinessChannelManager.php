<?php

/**
 * This is the model class for table "BusinessChannelManager".
 *
 * The followings are the available columns in table 'BusinessChannelManager':
 * @property integer $id_businessChannelManager
 * @property integer $id_business
 * @property integer $id_channelManager
 * @property string $apiUsr
 * @property string $apiLink
 * @property string $apiPsw
 *
 * The followings are the available model relations:
 * @property Business $idBusiness
 * @property ChannelManager $idChannelManager
 */
class BusinessChannelManager extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BusinessChannelManager';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_businessChannelManager', 'required'),
			array('id_businessChannelManager, id_business, id_channelManager', 'numerical', 'integerOnly'=>true),
			array('apiUsr, apiLink, apiPsw', 'length', 'max'=>90),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_businessChannelManager, id_business, id_channelManager, apiUsr, apiLink, apiPsw', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBusiness' => array(self::BELONGS_TO, 'Business', 'id_business'),
			'idChannelManager' => array(self::BELONGS_TO, 'ChannelManager', 'id_channelManager'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_businessChannelManager' => 'Id Business Channel Manager',
			'id_business' => 'Id Business',
			'id_channelManager' => 'Id Channel Manager',
			'apiUsr' => 'Api Usr',
			'apiLink' => 'Api Link',
			'apiPsw' => 'Api Psw',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_businessChannelManager',$this->id_businessChannelManager);
		$criteria->compare('id_business',$this->id_business);
		$criteria->compare('id_channelManager',$this->id_channelManager);
		$criteria->compare('apiUsr',$this->apiUsr,true);
		$criteria->compare('apiLink',$this->apiLink,true);
		$criteria->compare('apiPsw',$this->apiPsw,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BusinessChannelManager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
