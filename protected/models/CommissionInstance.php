<?php

/**
 * This is the model class for table "CommissionStatus".
 *
 * The followings are the available columns in table 'CommissionStatus':
 * @property integer $id_commissionInstance
 * @property integer $id_reservation
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Commission[] $commissions
 * @property Reservation $idReservation
 */
class CommissionInstance extends CActiveRecord{
	
	public function tableName(){
		return 'CommissionInstance';
	}

  /* generate(Commission $commission, $net)
   * 
   * Generate a commission instance form a commission rule.
   * An automated cost calculation is made in this point.
   *
   * Commission $commission, the rule to be followed.
   * Reservation $reservation, the reservation instance.
   */
  public static function generate($commission, $reservation, $save = true){
    $commissionInstance = CommissionInstance::model()->findByAttributes(array("id_commission" => $commission->id_commission, "id_reservation" => $reservation->id_reservation));
    if($commissionInstance === null){
      $commissionInstance = new CommissionInstance();
      $commissionInstance->id_commission = $commission->id_commission;
      $commissionInstance->id_reservation = $commission->id_reservation;
    }
    
    if(!empty($commission->staticCommission))
      $commissionInstance->automatedCost = $commission->staticCommission;
    else
      $commissionInstance->automatedCost = bcmul($reservation->net, $commission->percentage);
    
    if($save)
      if(!$commissionInstance->save()){
        /*TOO: LOG AN IMPORTANT ERROR*/
        return array("errors" => $commissionInstance->getErrors());
      }
    
    return $commissionInstance;
  }
  
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_commissionInstance', 'required'),
			array('id_commissionInstance, id_reservation, id_status', 'numerical', 'integerOnly'=>true),
			array('id_status', 'default', 'value' => 2),
      array('name', 'length', 'max'=>45),
      array('automatedCost, paidCost', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_commissionInstance, id_reservation, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'commission' => array(self::BELONGS_TO, 'Commission', 'id_commission'),
			'reservation' => array(self::BELONGS_TO, 'Reservation', 'id_reservation'),
			'status' => array(self::BELONGS_TO, 'CommissionStatus', 'id_commissionStatus'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id_commissionInstance' => 'Id Commission Status',
			'id_reservation' => 'Id Reservation',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_commissionInstance',$this->id_commissionInstance);
		$criteria->compare('id_reservation',$this->id_reservation);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CommissionStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
