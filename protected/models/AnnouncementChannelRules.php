<?php

/**
 * This is the model class for table "AnnouncementChannelRules".
 *
 * The followings are the available columns in table 'AnnouncementChannelRules':
 * @property integer $id_announcementChannelRules
 * @property integer $id_announcement
 * @property integer $cleaning
 * @property integer $expediaCleaning
 * @property integer $bookingcomCommission
 * @property string $validFrom
 *
 * The followings are the available model relations:
 * @property Announcement $idAnnouncement
 */
class AnnouncementChannelRules extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'AnnouncementChannelRules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('id_announcementChannelRules, id_announcement, cleaning, expediaCleaning, bookincomCommission, validFrom', 'required'),
			array('id_announcementChannelRules, id_announcement, cleaning, 
             expediaCleaning, bookingcomCommission, rentopolisCommission', 'numerical', 'integerOnly'=>true),
			array('validFrom', 'length', 'max' => 19),
      // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_announcementChannelRules, id_announcement, cleaning, expediaCleaning, bookingcomCommission, validFrom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idAnnouncement' => array(self::BELONGS_TO, 'Announcement', 'id_announcement'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_announcementChannelRules' => 'Id Announcement Channel Rules',
			'id_announcement' => 'Id Announcement',
			'cleaning' => 'Cleaning',
			'expediaCleaning' => 'Expedia Cleaning',
			'bookincomCommission' => 'Bookincom Commission',
			'validFrom' => 'Valid From',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_announcementChannelRules',$this->id_announcementChannelRules);
		$criteria->compare('id_announcement',$this->id_announcement);
		$criteria->compare('cleaning',$this->cleaning);
		$criteria->compare('expediaCleaning',$this->expediaCleaning);
		$criteria->compare('bookincomCommission',$this->bookincomCommission);
		$criteria->compare('validFrom',$this->validFrom,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnnouncementChannelRules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
