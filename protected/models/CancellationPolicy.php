<?php

/**
 * This is the model class for table "CancellationPolicy".
 *
 * The followings are the available columns in table 'CancellationPolicy':
 * @property integer $id_cancellationPolicy
 * @property string $name
 * @property integer $daysBeforeCheckinLimit1
 * @property string $refundRateBeforeCheckinLimit1
 * @property integer $notRefundableNightsLimit1
 * @property integer $daysBeforeCheckinLimit2
 * @property string $refundRateBeforeCheckinLimit2
 * @property integer $notRefundableNightLimit2
 * @property integer $daysBeforeCheckout
 * @property string $refundRateBeforeCheckout
 * @property integer $staticPenaltyRate
 * @property string $staticPenalty
 * @property integer $fixedThresholdsHour
 * @property string $fixedThresholdsTimezone
 *
 * The followings are the available model relations:
 * @property Channel[] $channels
 * @property ChannelCancellationPolicy[] $channelCancellationPolicies
 */
class CancellationPolicy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CancellationPolicy';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cancellationPolicy', 'required'),
			array('id_cancellationPolicy, daysBeforeCheckinLimit1, notRefundableNightsLimit1, daysBeforeCheckinLimit2, notRefundableNightLimit2, daysBeforeCheckout, staticPenaltyRate, fixedThresholdsHour', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('refundRateBeforeCheckinLimit1, refundRateBeforeCheckinLimit2, refundRateBeforeCheckout, staticPenalty, fixedThresholdsTimezone', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_cancellationPolicy, name, daysBeforeCheckinLimit1, refundRateBeforeCheckinLimit1, notRefundableNightsLimit1, daysBeforeCheckinLimit2, refundRateBeforeCheckinLimit2, notRefundableNightLimit2, daysBeforeCheckout, refundRateBeforeCheckout, staticPenaltyRate, staticPenalty, fixedThresholdsHour, fixedThresholdsTimezone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'channels' => array(self::HAS_MANY, 'Channel', 'id_defaultCancellationPolicy'),
			'channelCancellationPolicies' => array(self::HAS_MANY, 'ChannelCancellationPolicy', 'id_cancellationPolicy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cancellationPolicy' => 'Id Cancellation Policy',
			'name' => 'Name',
			'daysBeforeCheckinLimit1' => 'Days Before Checkin Limit1',
			'refundRateBeforeCheckinLimit1' => 'Refund Rate Before Checkin Limit1',
			'notRefundableNightsLimit1' => 'Not Refundable Nights Limit1',
			'daysBeforeCheckinLimit2' => 'Days Before Checkin Limit2',
			'refundRateBeforeCheckinLimit2' => 'Refund Rate Before Checkin Limit2',
			'notRefundableNightLimit2' => 'Not Refundable Night Limit2',
			'daysBeforeCheckout' => 'Days Before Checkout',
			'refundRateBeforeCheckout' => 'Refund Rate Before Checkout',
			'staticPenaltyRate' => 'Static Penalty Rate',
			'staticPenalty' => 'Static Penalty',
			'fixedThresholdsHour' => 'Fixed Thresholds Hour',
			'fixedThresholdsTimezone' => 'Fixed Thresholds Timezone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_cancellationPolicy',$this->id_cancellationPolicy);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('daysBeforeCheckinLimit1',$this->daysBeforeCheckinLimit1);
		$criteria->compare('refundRateBeforeCheckinLimit1',$this->refundRateBeforeCheckinLimit1,true);
		$criteria->compare('notRefundableNightsLimit1',$this->notRefundableNightsLimit1);
		$criteria->compare('daysBeforeCheckinLimit2',$this->daysBeforeCheckinLimit2);
		$criteria->compare('refundRateBeforeCheckinLimit2',$this->refundRateBeforeCheckinLimit2,true);
		$criteria->compare('notRefundableNightLimit2',$this->notRefundableNightLimit2);
		$criteria->compare('daysBeforeCheckout',$this->daysBeforeCheckout);
		$criteria->compare('refundRateBeforeCheckout',$this->refundRateBeforeCheckout,true);
		$criteria->compare('staticPenaltyRate',$this->staticPenaltyRate);
		$criteria->compare('staticPenalty',$this->staticPenalty,true);
		$criteria->compare('fixedThresholdsHour',$this->fixedThresholdsHour);
		$criteria->compare('fixedThresholdsTimezone',$this->fixedThresholdsTimezone,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CancellationPolicy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
