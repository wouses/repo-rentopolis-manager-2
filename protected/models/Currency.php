<?php

/**
 * This is the model class for table "Currency".
 *
 * The followings are the available columns in table 'Currency':
 * @property integer $id_currency
 * @property string $name
 * @property string $iso
 * @property string $symbol
 *
 * The followings are the available model relations:
 * @property Commission[] $commissions
 * @property Reservation[] $reservations
 * @property Tranches[] $tranches
 */
class Currency extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Currency';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_currency', 'required'),
			array('id_currency', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>20),
			array('iso', 'length', 'max'=>3),
			array('symbol', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_currency, name, iso, symbol', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'commissions' => array(self::HAS_MANY, 'Commission', 'id_currency'),
			'reservations' => array(self::HAS_MANY, 'Reservation', 'id_currency'),
			'tranches' => array(self::HAS_MANY, 'Tranches', 'id_currency'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_currency' => 'Id Currency',
			'name' => 'Name',
			'iso' => 'Iso',
			'symbol' => 'Symbol',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_currency',$this->id_currency);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('iso',$this->iso,true);
		$criteria->compare('symbol',$this->symbol,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Currency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
