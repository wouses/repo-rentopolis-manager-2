<?php

/**
 * This is the model class for table "Commission".
 *
 * The followings are the available columns in table 'Commission':
 * @property integer $id_commission
 * @property integer $id_partner
 * @property integer $id_contract
 * @property integer $id_currency
 * @property integer $id_commissionStatus
 * @property integer $id_service
 * @property integer $id_reservation
 * @property string $percentage
 * @property string $staticCommission
 *
 * The followings are the available model relations:
 * @property BusinessPartner $idPartner
 * @property CommissionStatus $idCommissionStatus
 * @property Contract $idContract
 * @property Currency $idCurrency
 * @property Reservation $idReservation
 * @property Service $idService
 */
class Commission extends CActiveRecord{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'Commission';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_commission', 'required'),
			array('id_commission, id_partner, id_contract, id_currency, id_commissionStatus, id_service, id_reservation', 'numerical', 'integerOnly'=>true),
			array('percentage, staticCommission', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_commission, id_partner, id_contract, id_currency, id_commissionStatus, id_service, id_reservation, percentage, staticCommission', 'safe', 'on'=>'search'),
		);
	}

  public function getCommissionInstance($reservation){
    $instances = $this->commissionInstances();
    if(count($instances) == 1) return $instances[0];
    
    for($i = 0; $i < count($instances); $i++){
      $instance = $instances[$i];
      if($instance->id_reservation == "null"){
        $ruleInstance = $instance; 
      }
    }
  }
  
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'partner' => array(self::BELONGS_TO, 'BusinessPartner', 'id_partner'),
			'commissionInstances' => array(self::HAS_MANY, 'CommissionInstance', 'id_commission'),
			'contract' => array(self::BELONGS_TO, 'Contract', 'id_contract'),
			'currency' => array(self::BELONGS_TO, 'Currency', 'id_currency'),
			'reservation' => array(self::BELONGS_TO, 'Reservation', 'id_reservation'),
			'service' => array(self::BELONGS_TO, 'Service', 'id_service'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_commission' => 'Id Commission',
			'id_partner' => 'Id Partner',
			'id_contract' => 'Id Contract',
			'id_currency' => 'Id Currency',
			'id_commissionStatus' => 'Id Commission Status',
			'id_service' => 'Id Service',
			'id_reservation' => 'Id Reservation',
			'percentage' => 'Percentage',
			'staticCommission' => 'Static Commission',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_commission',$this->id_commission);
		$criteria->compare('id_partner',$this->id_partner);
		$criteria->compare('id_contract',$this->id_contract);
		$criteria->compare('id_currency',$this->id_currency);
		$criteria->compare('id_commissionStatus',$this->id_commissionStatus);
		$criteria->compare('id_service',$this->id_service);
		$criteria->compare('id_reservation',$this->id_reservation);
		$criteria->compare('percentage',$this->percentage,true);
		$criteria->compare('staticCommission',$this->staticCommission,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Commission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
