<?php

/**
 * This is the model class for table "ChannelCommission".
 *
 * The followings are the available columns in table 'ChannelCommission':
 * @property integer $id_channelCommission
 * @property integer $id_business
 * @property integer $id_channel
 * @property string $rateCommission
 * @property string $staticCommission
 * @property string $validFrom
 *
 * The followings are the available model relations:
 * @property Business $idBusiness
 * @property Channel $idChannel
 */
class ChannelCommission extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ChannelCommission';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_channelCommission', 'required'),
			array('id_channelCommission, id_business, id_channel', 'numerical', 'integerOnly'=>true),
			array('rateCommission', 'length', 'max'=>2),
			array('staticCommission', 'length', 'max'=>4),
			array('validFrom', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_channelCommission, id_business, id_channel, rateCommission, staticCommission, validFrom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBusiness' => array(self::BELONGS_TO, 'Business', 'id_business'),
			'idChannel' => array(self::BELONGS_TO, 'Channel', 'id_channel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_channelCommission' => 'Id Channel Commission',
			'id_business' => 'Id Business',
			'id_channel' => 'Id Channel',
			'rateCommission' => 'Rate Commission',
			'staticCommission' => 'Static Commission',
			'validFrom' => 'Valid From',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_channelCommission',$this->id_channelCommission);
		$criteria->compare('id_business',$this->id_business);
		$criteria->compare('id_channel',$this->id_channel);
		$criteria->compare('rateCommission',$this->rateCommission,true);
		$criteria->compare('staticCommission',$this->staticCommission,true);
		$criteria->compare('validFrom',$this->validFrom,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChannelCommission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
