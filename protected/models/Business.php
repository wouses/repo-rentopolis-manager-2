<?php

/**
 * This is the model class for table "Business".
 *
 * The followings are the available columns in table 'Business':
 * @property integer $id_business
 * @property string $name
 * @property string $logoUri
 *
 * The followings are the available model relations:
 * @property Announcement[] $announcements
 * @property BankAccount[] $bankAccounts
 * @property BusinessChannelManager[] $businessChannelManagers
 * @property BusinessPartner[] $businessPartners
 * @property ChannelCommission[] $channelCommissions
 * @property PaymentChannelAccount[] $paymentChannelAccounts
 * @property User[] $users
 */
class Business extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Business';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_business', 'required'),
			array('id_business', 'numerical', 'integerOnly'=>true),
			array('name, logoUri', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_business, name, logoUri', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'announcements' => array(self::HAS_MANY, 'Announcement', 'id_business'),
			'bankAccounts' => array(self::HAS_MANY, 'BankAccount', 'id_business'),
			'businessChannelManagers' => array(self::HAS_MANY, 'BusinessChannelManager', 'id_business'),
			'businessPartners' => array(self::HAS_MANY, 'BusinessPartner', 'id_business'),
			'channelCommissions' => array(self::HAS_MANY, 'ChannelCommission', 'id_business'),
			'paymentChannelAccounts' => array(self::HAS_MANY, 'PaymentChannelAccount', 'id_business'),
			'users' => array(self::HAS_MANY, 'User', 'id_business'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_business' => 'Id Business',
			'name' => 'Name',
			'logoUri' => 'Logo Uri',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_business',$this->id_business);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('logoUri',$this->logoUri,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Business the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
