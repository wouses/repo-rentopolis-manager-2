<?php

/**
 * This is the model class for table "Tranches".
 *
 * The followings are the available columns in table 'Tranches':
 * @property integer $id_tranche
 * @property integer $id_reservation
 * @property integer $id_pos
 * @property integer $id_bank
 * @property integer $id_paymentChannelAccount
 * @property integer $id_currency
 * @property integer $id_manager
 * @property string $amount
 * @property string $paymentChannelReferement
 *
 * The followings are the available model relations:
 * @property BankAccount $idBank
 * @property Currency $idCurrency
 * @property PaymentChannelAccount $idPaymentChannelAccount
 * @property Pos $idPos
 * @property Reservation $idReservation
 * @property User $idManager
 */
class Tranches extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Tranches';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tranche', 'required'),
			array('id_tranche, id_reservation, id_pos, id_bank, id_paymentChannelAccount, id_currency, id_manager', 'numerical', 'integerOnly'=>true),
			array('amount', 'length', 'max'=>10),
			array('paymentChannelReferement', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tranche, id_reservation, id_pos, id_bank, id_paymentChannelAccount, id_currency, id_manager, amount, paymentChannelReferement', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBank' => array(self::BELONGS_TO, 'BankAccount', 'id_bank'),
			'idCurrency' => array(self::BELONGS_TO, 'Currency', 'id_currency'),
			'idPaymentChannelAccount' => array(self::BELONGS_TO, 'PaymentChannelAccount', 'id_paymentChannelAccount'),
			'idPos' => array(self::BELONGS_TO, 'Pos', 'id_pos'),
			'idReservation' => array(self::BELONGS_TO, 'Reservation', 'id_reservation'),
			'idManager' => array(self::BELONGS_TO, 'User', 'id_manager'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tranche' => 'Id Tranche',
			'id_reservation' => 'Id Reservation',
			'id_pos' => 'Id Pos',
			'id_bank' => 'Id Bank',
			'id_paymentChannelAccount' => 'Id Payment Channel Account',
			'id_currency' => 'Id Currency',
			'id_manager' => 'Id Manager',
			'amount' => 'Amount',
			'paymentChannelReferement' => 'Payment Channel Referement',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tranche',$this->id_tranche);
		$criteria->compare('id_reservation',$this->id_reservation);
		$criteria->compare('id_pos',$this->id_pos);
		$criteria->compare('id_bank',$this->id_bank);
		$criteria->compare('id_paymentChannelAccount',$this->id_paymentChannelAccount);
		$criteria->compare('id_currency',$this->id_currency);
		$criteria->compare('id_manager',$this->id_manager);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('paymentChannelReferement',$this->paymentChannelReferement,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tranches the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
