<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
    'application.extensions.ChannelManagers.*',
    'application.extensions.IcsParser.*',
    
    /*library*/
    //'application.extensions.libs.phpexcel.Classes.*'
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'giiAdmin',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1', $_SERVER['REMOTE_ADDR']),
		),
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
      'showScriptName' => false,
			'caseSensitive'=>false,
      'urlFormat'=>'path',
      
			'rules'=>array(
        'announcement/<listing:\d+>/<action:\w+>/'=>'announcement/<action>',
        'reservation/<reservation:\d+>/<action:\w+>/'=>'reservation/<action>',
        'channels/<channelId:\d+>/<action:\w+>/'=>'channels/<action>',
        
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		// database settings are configured in database.php
		//'db'=>require(dirname(__FILE__).'/database.php'),
    
    'db'=>array(
      'connectionString' => 'mysql:host=localhost;dbname=manager',
      'emulatePrepare' => true,
      'username' => 'marco.distrutti',
      'password' => '!blowFish[1a]',
      'charset' => 'utf8',
    ),
    
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail' => 'webmaster@example.com',
    'googleApiKey' => 'AIzaSyAJCLVAVsad9opOHZEb1W19Ic0_yjtDuro',
    'jsVersion' => '1.2.20',
    'jsViewsVersion' => '1.1.23',
	),
);
