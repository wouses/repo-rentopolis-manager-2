<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
    'application.extensions.ChannelManagers.*',
    'application.extensions.IcsParser.*',
	),
	// application components
	'components'=>array(

		// database settings are configured in database.php
		//'db'=>require(dirname(__FILE__).'/database.php'),

    'db'=>array(
      'connectionString' => 'mysql:host=localhost;dbname=manager',
      'emulatePrepare' => true,
      'username' => 'marco.distrutti',
      'password' => '!blowFish[1a]',
      'charset' => 'utf8',
    ),
    
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

	),
);
