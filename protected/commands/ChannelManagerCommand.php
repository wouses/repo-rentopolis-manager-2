<?php

class ChannelManagerCommand extends CConsoleCommand{
  
    public function actionIndex($type, $limit=5) {
      
    }
  
    public function actionImport() {
      set_time_limit(900);
      $time_start = microtime(TRUE);
      
      /*
       * WuBook Import (first direction)
       */
      
      $wuBook = new WuBook();
      $criteria = new CDbCriteria();
      
      $criteria->addInCondition("id_announcement", array(
        549, 547, 538, 551, 548, 540, 541, 550,

        466, 467, 511, 539, 542, 544, 462, 495, 546,
        493, 492, 471, 463, 510, 474, 478, 527, 505, 506,
        513, 500, 502, 475, 519, 497, 498, 484, 468, 476,
        467, 469, 479, 545, 473, 508, 501, 481, 512, 520
      ));
      $wuAnnouncements = Announcement::model()->findAll($criteria);
      foreach($wuAnnouncements as $wuannouncement){
        $json["result"] = $wuBook->import(array("lcode"=>$wuannouncement->wubook_lcode));
        $result = json_encode($json);
        $log = new LogImportOperations();
        $log->result = $result;
        $log->save();
      }
      
      $endTime = microtime(TRUE);
      $sessionTime = $endTime - $time_start;

      echo "WUBOOK STEP DONE. ".number_format($sessionTime, 2)." seconds\n";
      
      /*
       * VREasy import
       * Second direction
       */
      $time_start = microtime(TRUE);
      
      $vreasy = new VREasy();
      
      /*Clean the database*/
      $json["result"] = $vreasy->import(array("cleaningMode"=>1));
      $result = json_encode($json);
      $log = new LogImportOperations();
      $log->result = $result;
      $log->save();
      
      /*Import new reservations*/
      $json["result"] = $vreasy->import();
      $result = json_encode($json);
      $log = new LogImportOperations();
      $log->result = $result;
      $log->save();
      
      $endTime = microtime(TRUE);
      $sessionTime = $endTime - $time_start;
      
      echo "VREASY STEP DONE. ".number_format($sessionTime, 2)." seconds";
    }
  
    public function actionImportNew(){
      set_time_limit(900);
      $vreasy = new VREasy(5);
      
      /*Clean the database*/
      $json["result"] = $vreasy->import(array("cleaningMode"=>1));
      $result = json_encode($json);
      $log = new LogImportOperations();
      $log->result = $result;
      $log->save();
      
      /*Import new reservations*/
      $json["result"] = $vreasy->import();
      $result = json_encode($json);
      $log = new LogImportOperations();
      $log->result = $result;
      $log->save();
    }

    public function actionWuBookImport(){
      
      /*$wuBook = new WuBook();      
      $criteria = new CDbCriteria();
      $criteria->condition = "t.wubook_lcode <> ''";
      $wuAnnouncements = Announcement::model()->findAll($criteria);

      foreach($wuAnnouncements as $wuannouncement){
        $json["result"] = $wuBook->import(array("lcode"=>$wuannouncement->wubook_lcode));
        $result = json_encode($json);
        $log = new LogImportOperations();
        $log->result = $result;
        $log->save();
      }*/
    }

    public function actionClearBp(){
      $businessPartners = BusinessPartner::model()->findAll();
      foreach($businessPartners as $bp){
        $reservations = $bp->guestReservation();
        $user = $bp->user();
        $announcements = $bp->announcementsOwned();
        if(empty($reservations) && empty($user) && empty($announcements)){
          $bp->delete();
        }
      }
    }

    public function actionTest(){
      $vreasy = new VREasy();
      $result = $vreasy->retrieveReservations();
      echo json_encode($result);
    }
  
    public function actionBusinessStockChart(){

      $criteria = new CDbCriteria();
      $criteria->condition = "isBlockedDates = 0 AND id_reservationStatus = 1";
      $criteria->order = "checkinDate ASC";
      $reservations = Reservation::model()->findAll($criteria);

      $plot = array();

      foreach($reservations as $rs){
        $consideredDate = new DateTime($rs->checkinDate);
        $announcement = $rs->announcement();
        if(!empty($rs->bookingTime)){
          $announcementRule = $announcement->getChannelsRule($rs->bookingTime);
        }else{
          $announcementRule = $announcement->getChannelsRule($rs->checkinDate);
        }

        if(empty($announcementRule)) die($rs->checkinDate.", ".$rs->id_reservation);

        $rentopolisCommission = $announcementRule->rentopolisCommission;

        $channelCommission = $rs->channelCommission;
        $net = $rs->net;

        $businessEarnings = bcadd(bcmul(bcdiv($rentopolisCommission, "100", "2"), $net, "2"), "24.40", "2");

        $plotNet[] = array($net, $consideredDate->format("Y-m-d"));
        $plotBusiness[] = array($businessEarnings, $consideredDate->format("Y-m-d"));
      }

      $plots = array($plotNet, $plotBusiness);

      $bc = BusinessChart::model()->findByAttributes(array("description" => "STOCK_GROSS_NET"));
      if($bc !== null)
        $bc->delete();
      
      $bc = new BusinessChart();
      $bc->description = "STOCK_GROSS_NET";
      $bc->datas = json_encode($plots);
      $bc->save();
    }
  
    public function actionBuildMapSynch(){

      $criteria = new CDbCriteria();
      $criteria->condition = "checkinDate >= '2016-01-01 00:00:00'"; //for those reservation managed with channelManagers
      $reservations = Reservation::model()->findAll($criteria);
      
      foreach($reservations as $reservation){
        $channelAssociation = $reservation->channel();
        $channelManager  = $channelAssociation->channelManager();
        $cName = $channelManager->getAttribute("chmName");
        $instance = $channelManager->id_channelManager;
        
        $mapSynch = json_decode($reservation->mapSynch);
        
        $mapSynchRecord = ReservationMapSynch::model()->findByAttributes(array("id_reservation" => $reservation->id_reservation));
        if($mapSynchRecord === null){
          $mapSynchRecord = new ReservationMapSynch();
          $mapSynchRecord->id_reservation = $reservation->id_reservation;
        }
        
        foreach($mapSynch as $chm => $chmSynched){
          $channelManager = ChannelManager::model()->findByAttributes(array("id_channelManager" => $chm));
          
          $chmName = $channelManager->getAttribute("chmName");
          $attribute = $chmName."_".$chm;
          
          $mapSynchRecord->$attribute = $chmSynched->code;
        }
        
        if(!empty($reservation->channelManagerReferement)){
          $attribute = $cName."_".$instance;
          $mapSynchRecord->$attribute = $reservation->channelManagerReferement;
        }
        
        $mapSynchRecord->save();
        //$id_channel = $channelAssociation->id_channel;
      }
      
      //$reservations = Reservations::model()->findAllByAttributes();
    }
  
    public function actionFixReservations(){
      $criteria = new CDbCriteria();
      $criteria->condition = "id_announcement = 544 AND checkinDate >= '2016-02-01 00:00:00'"; //for those reservation managed with channelManagers
      $reservations = Reservation::model()->findAll($criteria);
      
      $counter = 0;
      $result = array();
      
      foreach($reservations as $reservation){
        $checkin = new DateTime($reservation->checkinDate);
        $checkout = new DateTime($reservation->checkoutDate);
        $toUpdate = false;
        
        if($checkin->format('H') == '00'){
          $reservation->checkinDate = $checkin->format("Y-m-d 15:00:00");
          $toUpdate = true;
        }
        
        if($checkout->format('H') == '00'){
          $reservation->checkoutDate = $checkout->format("Y-m-d 10:00:00");
          $toUpdate = true;
        }
        
        if($toUpdate = true)
          $reservation->update();
      }
      /*$logMessage =  json_encode($result);
      $log = new LogErrorOperationSynch();
      $generalError = true;
      $log->sended = $logMessage;
      $log->id_reservation = 7694;

      $log->save();*/
    }
}

?>