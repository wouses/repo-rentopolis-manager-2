<?php

class ChartsCommand extends CConsoleCommand{
    /*
     * - actionHostsCharts: create datas for multiple drill down bars 
     */
    public function actionIndex($type, $limit=5){
      
    }
  
    public function actionHostsCharts() {
      $user = User::model()->findByAttributes(array("id_user" => 9));

      $users = User::model()->findAllByAttributes(array("role"=>"homeowner"));
      foreach($users as $user){
        
        $announcements = Announcement::model()->findAllByAttributes(array("id_host" => $user->id_businessPartner));
        if(empty($announcements)){ continue; }

        $titles = array();
        foreach($announcements as $announcement){
          $annTitle = $announcement->title;
          $titles[] = str_replace(" ", "_", $annTitle);
        }

        $criteria = new CDbCriteria();
        $criteria->with = array(
          'announcement' => array('alias' => 'a'),
          'announcement.host' => array('alias' => 'h')
        );
        $criteria->condition = "a.id_host = :userId AND id_reservationStatus = 1 AND isBlockedDates = 0";
        $criteria->params = array(":userId" => $user->id_businessPartner);
        $criteria->order = "t.checkinDate ASC";

        $months = array();
        $reservations = Reservation::model()->with()->findAll($criteria);

        foreach($reservations as $reservation){

          $announcementTitle = str_replace(" ", "_", $reservation->announcement()->title);

          $earnings = $reservation->getHostEarnings();
          $nights = $reservation->getTotalNights();
          $avg = bcdiv($earnings, $nights, 2);

          $dtStart = new DateTime($reservation->checkinDate);
          $dtEnd = new DateTime($reservation->checkoutDate);

          $interval = new DateInterval('P1D');
          $daterange = new DatePeriod($dtStart, $interval ,$dtEnd);

          /*for each interessed month we have to initialize an array of values*/
          foreach($daterange as $dtPointer){

            $Ym = $dtPointer->format("Y-m");

            if(empty($months[$Ym])){
            //init month
              $initialStart = new DateTime($dtPointer->format("Y-m-01"));
              $days = cal_days_in_month(CAL_GREGORIAN, $dtPointer->format("m"), $dtPointer->format("y"));

              $months[$Ym] = array();
              foreach($titles as $title){
                $months[$Ym][$title] = array("total"=>0, "monthDetail"=>array());
                for($i = 0; $i < $days; $i++){
                  $day = $i+1;
                  $months[$Ym][$title]["monthDetail"][] = array($dtPointer->format("m")."-".strval($day), 0);
                }
              }
            }
            //set day value
            $months[$Ym]
              [$announcementTitle]["monthDetail"]
              [intval($dtPointer->format("d"))-1]
              = array($dtPointer->format("m")."-".strval(intval($dtPointer->format("d"))),
                      floatval($avg));
          }
        }

        //recalculate all total values.
        foreach($months as $Ym => $aRecords){
          foreach($aRecords as $aTitle => $aRecord){
            $total = "0";
            foreach($aRecord["monthDetail"] as $arrDay){
              $total = bcadd($total, strval($arrDay[1]), 2);
            }
            $months[$Ym][$aTitle]["total"] = floatval($total);
          }
        }

        $bhc = BusinessHostsCharts::model()->findByAttributes(array(
          "id_businessPartner" => $user->id_businessPartner,
          "description" => "BH"
        ));

        if(!empty($bhc)) $bhc->delete();

        $encoded = json_encode($months);
        $istance = new BusinessHostsCharts();
        $istance->id_businessPartner = $user->id_businessPartner;
        $istance->datas = $encoded;
        $istance->description = "DRILLDOWN_APARTMENTS";
        $istance->save();
      }//end foreach users
    }
}

?>