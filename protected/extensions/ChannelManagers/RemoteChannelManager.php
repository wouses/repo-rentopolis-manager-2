<?php

abstract class RemoteChannelManager{

  protected $channelManagerId = null;
  protected $channelManager = null;

  abstract protected function connect();

  /* Abstract methods for data importation.
   * The flow to be respect: 
   * - etablish a connection with the channel manager
   * - retrieve the data from this 
   */
  abstract protected function retrieveReservations();
  abstract protected function retrieveClient();
  abstract protected function retrieveAnnouncements();

  /* 
   * Abstract methods for the control of the data existence
   */
  abstract protected function clientExists();

  /* getReservationDifference()
   *
   * Check if the reservation is changhed in some parts and return the difference.
   * The difference can be:
   * - status.
   * - pricing details.
   */
  abstract protected function getReservationDifference();

  /* $comparedAttributes
   * represents the set of comparing attributes
   * between the istance in the channel manager and the instance in the Manager.
   * If not specified, all attributes are compared.
   * In VREasy, for example, the attribute remain like that. In WuBook we are only interested to check
   * the reservation status.
   */
  protected $comparedAttributes = array();

  function __construct($istance = 2) {
    $this->channelManagerId = $istance;
    $this->channelManager = ChannelManager::model()->findByAttributes(array("id_channelManager" => $this->channelManagerId));
  }

  public function getChannelManagerId(){
    return $this->channelManagerId;
  }

  public function isConnectedChannel($channelId, $time = null, $announcement = null){

    if(empty($time)){
      $time = new DateTime();
      $time = $time->format("Y-m-d");
    }
    
    if(intval($this->channelManagerId) == 2 && !empty($announcement)){
      //Cristoforo, Mac Mahon, Vicolo, Via della Giuliana, Aosta, Villa l'Avv. T, Villa l'Avv. P
      
      //Argelati, Ponti, Baracca, Bertani (B), Bertani (A), Bettinetti, Bolivar, Buonarroti, Castelfidardo
      //Cavaleri, Comelico, Da Procida, Foro, Gandhi, Garibaldi 108, Garibaldi 72, Lanzone, Marco Polo 1, Marco Polo 2,
      //Mazzo, Melchiorre Gioia, Montebello, Novati, Pace, Paoli A, Paoli B, Parini, Piazza V Giornate, Plinio,
      //Ponti A, San Maurilio, Sannio, Sansovino,  Sforza A, Sforza B, Torino, Turati, Virgilio, Volta
      
      $exceptions = array(
        549, 547, 538, 551, 548, 540, 541, 550,
        
        466, 467, 511, 539, 542, 544, 462, 495, 546,
        493, 492, 471, 463, 510, 474, 478, 527, 505, 506,
        513, 500, 502, 475, 519, 497, 498, 484, 468, 476,
        467, 469, 479, 545, 473, 508, 501, 481, 512, 520
      );
      
      
      //from 2016-02-08 some apartments doesn't accept booking.com or airbnb reservations from old VReasy 
      //this is not elegant but absolute temporary...
      if(intval($channelId) == 7 || intval($channelId) == 9){
        if(in_array(intval($announcement), $exceptions)){
          return false;
        }
      }
      //fake, from the old VREasy chm all channels are importable.
      return true;
    }

    $criteria = new CDbCriteria();
    $criteria->condition = "id_channelManager = :chmId AND id_channel = :channelId AND validFrom <= :time";
    $criteria->params = array(":chmId" => $this->channelManagerId, ":channelId" => $channelId, "time" => $time);
    $criteria->order = "validFrom DESC";
    
    $connectionRules = ChannelManagerChannelConnection::model()->findAll($criteria);
    
    /*echo " ".$this->channelManagerId." ".$channelId." ".$time;
    print_r($connectionRules);
    die();*/
    
    return (count($connectionRules) > 0 && intval($connectionRules[0]->connected) == 1);
  }

  protected function canIContinueToImport($remoteReservation){
    return true;
  }

  protected function startXmlRpc($url){
    //YiiBase::import("application.extensions.libs.xmlrpc.lib.*");
    $GLOBALS['xmlrpc_internalencoding'] = 'UTF-8';
    $client = new xmlrpc_client($url);
    return $client;
  }
  
  /* import($params)
   *
   * Reservations importation method: this is the abstract flow for each importation flow
   * implemented by his childrens.
   *
   */
  public function import($params = array()){
    YiiBase::import("application.controllers.AnnouncementController");
    $connection = $this->connect();
    $retrievedReservations = $this->retrieveReservations($params, $connection);

    //echo " uhm ";
    
    $reservations = $retrievedReservations["reservations"];
    $operationResults = array(
      "chm" => $retrievedReservations["chm"],
      "serverTime" => $retrievedReservations["serverTime"],
      "currentSessionTime" => $retrievedReservations["currentSessionTime"],
      "requestedStatuses" => $retrievedReservations["requestedStatuses"],
      "importedReservations" => array(), 
      "updatedReservations" => array(),
      "cancelledReservations" => array(),
      "errorImport" => array(), 
      "errorUpdate" => array(),
      "errorCancel" => array()
    );
    
    foreach($reservations as $rs){

      if(!$this->canIContinueToImport($rs)){
        continue;
      }
      
      $reservation = $this->convertRetrievedReservation($rs);
      
      $channelId = $reservation->channel()->id_channel;
      if(!$this->isConnectedChannel($channelId, $reservation->bookingTime, $reservation->id_announcement)){
        continue;
      }

      $reservationExists = $this->reservationExists($reservation, $rs);
      $clientExists = $this->clientExists();

      if(!empty($reservationExists)){
        if($reservationExists->hasChanges($this->comparedAttributes)){  
          $reservationOldValues = new Reservation();
          $reservationOldValues->setAttributes($reservationExists->getAttributes());

          $reservationExists->updateNewAttributes();
          $reservationExists->lastModified = (new DateTime())->format("Y-m-d H:i:s");

          $synch = false;
          if($reservationExists->update() && $reservationExists->guest()->update()){
            $operationResults["updatedReservations"][] = $rs;
            $synch = true;
          }else
            $operationResults["errorUpdate"][] = array("id" => $rs->id, "instance" => $rs);

          if($synch){
            //if the price is manual updated then there will not be synchronization fot that
            $synchPricing = $reservationExists->flagPu != 1;
            $reservationExists->synchReservation($reservationOldValues, $synchPricing, array($this->channelManagerId));
          }
        }else if($reservationExists->hasChanges($this->comparedAttributes) && !$reservation->isImportable()){
          $operationResults["cancelledReservations"][] = $rs;
          //if($reservationExists->delete()){
          /*}else{
            $operationResults["errorCancel"][] = array("id" => $rs->id, "instance" => $rs);
          }*/
        }
        //the reservation exists, now I check if the guest exists and has changes
      }

      if(empty($reservationExists) && $reservation->isImportable()){
        if($reservation->save()){
          $operationResults["importedReservations"][] = $rs;
          $reservation->saveGuest();
          
          //var_dump("ok");
          
          $synchPricing = false;
          $chmName = $reservation->getChannelManagerName();
          if($chmName == "WUBOOK")
            $synchPricing = true;
          $reservation->setIsNewReservation(true);
          $reservation->synchReservation(null, $synchPricing, array($this->channelManagerId));
          
        }else
          $operationResults["errorImport"][] = array("id" => $rs->id, "instance" => $rs);
      }
    }

    return $operationResults;
  }

  protected function reservationExists($reservation, $remoteReservation){
    $tblRs = Reservation::model()->tableName();
    $tblChmCh = ChannelManagerChannelAssociation::model()->tableName();

    $criteria = new CDbCriteria();
    $criteria->join = ' LEFT JOIN `'.$tblChmCh.'` AS `chmch` ON t.id_channel = chmch.id_channelAssociation';
    $criteria->addCondition("channelManagerReferement = '".$reservation->channelManagerReferement."' AND chmch.id_channelManager = '".$reservation->getChmId()."'");
    $returned = Reservation::model()->find($criteria);
    
    if($returned !== null) $returned->setNewInstance($reservation);
    
    return $returned;
  }
}

?>