<?php

class VREasy extends RemoteChannelManager{
  
  private $username = "4y6c4z2gb6kgwkkgsw888css8cs88wg4w4o08";
  private $password = "";

  //eyildi.111522@guest.booking.com

  public function __construct($istance = 2) {
    parent::__construct($istance);
    
    $this->username = $this->channelManager->apiKey;
    $this->password = $this->channelManager->password;
  }
  
  public function retrieveReservations($params = null, $connection = null){
    $cleaningMode = 0; //default value
    
    if(!empty($params) && !empty($params["cleaningMode"]))
      $cleaningMode = intval($params["cleaningMode"]);
    
    if($cleaningMode == 0) //importation status mode
      $statuses = array("CONFIRMED", "ENQUIRY", "UNAVAILABLE");
    else //cleaning status mode
      $statuses = array("CANCELLATIONREQUESTED", "CANCELLED", "UNCONFIRMED");
    
    $startTime = microtime(TRUE);

    $reservations = array();
    $serverTime = 0;

    for($s = 0; $s < count($statuses); $s++){
      $statusReservation = $this->retrieveReservationsByStatus($statuses[$s]);
      usleep(1000000);
      $reservations = array_merge($reservations, $statusReservation["reservations"]);
      $serverTime += floatval($statusReservation["time"]);
    }

    $endTime = microtime(TRUE);
    $sessionTime = $endTime - $startTime;

    $json = array(
      "chm" => "VREasy",
      "serverTime" => $serverTime,
      "currentSessionTime" => $sessionTime,
      "requestedStatuses" => $statuses,
      "reservations" => $reservations
    );

    return $json;
  }
  
  public function retrieveChannels($business = 1){
    set_time_limit(300);
    $time_start = microtime(TRUE);

    $channels = array();
    
    $after = "";
    $lastAfter = "";
    
    for($counter = 0; $counter < 100; $counter++){
      $url = "https://www.vreasy.com/api/channels?expand=logo,logo_image"; 

      if(!empty($after))
        $url .= "&after=".$after;

      $ch=curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300); //five minutes

      $result = curl_exec($ch);

      if($result === false){
        $error = curl_error($ch);
        curl_close ($ch);
        throw new Exception($error);
      }

      $results = json_decode($result);      
      if(count($results) == 0) break;
      
      $after = $results[count($results)-1]->id;
      if($after == $lastAfter) break;
      
      $lastAfter = $after;
      $channels = array_merge($channels, $results);
    }
    
    $endTime = microtime(TRUE);
    $sessionTime = $endTime - $time_start;
    
    $json = array(
      "serverTime" => $sessionTime,
      "currentSessionTime" => $sessionTime,
      "channels" => $channels
    );
    
    return $json;
  }
  
  public function retrieveReservationsByStatus($status, $business = 1){    
    set_time_limit(300);
    $time_start = microtime(TRUE);

    $reservations = array();

    $after = "";
    $lastAfter = "";

    for($counter = 0; $counter < 100; $counter++){
      $url = "https://www.vreasy.com/api/reservations?status=$status&checkin=>2016-02-01&expand=guest,listor,payments,payments/payment_type"; 

      if(!empty($after))
        $url .= "&after=".$after;

      $ch=curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300); //five minutes

      $result = curl_exec($ch);

      if($result === false){
        $error = curl_error($ch);
        curl_close ($ch);
        throw new Exception($error);
      }

      $results = json_decode($result);      
      if(count($results) == 0) break;
      
      $after = $results[count($results)-1]->id;
      if($after == $lastAfter) break;
      
      $lastAfter = $after;
      $reservations = array_merge($reservations, $results);
    }

    $tmp = 0;

    $portals = array(); $toImport = array(); $blocks = array();
    $statuses = array("BLOCKEDCANCELLED" => 0);
    $nullEmailGuests = array(); $noneGuests = array();

    foreach($reservations as $reservation){
      $checkin = new DateTime($reservation->checkin);
      $compare = new DateTime("2015-12-31");
      $today = new DateTime("2015-11-04");

      $association = AnnouncementChannelManager::model()->findByAttributes(array(
        "id_channelManager" => $this->channelManagerId, 
        "alias"=>$reservation->property_id
      ));
      if($association === null) continue;

      $exceptions = array(
        "block", "hold", "owner", "propr", "blocc", "close", "chius", " end", "end ", "fine ", " fine", 
        "rentopolis", "retopolis", /*"amic", "friend",*/ "contratt", "contract"
      );
      $reservation->title = $association->announcement()->title;
      $reservation->id_announcement = $association->announcement()->id_announcement;
      
      if(!isset($statuses[$reservation->status]))
        $statuses[$reservation->status] = 0;
      $statuses[$reservation->status]++;

      if(!isset($portals[$reservation->listor->name]))
        $portals[$reservation->listor->name] = 0;
      $portals[$reservation->listor->name]++;

      if(!$this->contains($reservation->guest->fname, $exceptions) && 
         !$this->contains($reservation->guest->lname, $exceptions) && 
         !$this->contains($reservation->guest->email, $exceptions) &&
         strtolower($reservation->status) != "unavailable"
        ){
        $reservation->isBlockedDates = 0;
      }else{
        $reservation->isBlockedDates = 1;
        $blocks[] = $reservation;
      }
      $toImport[] = $reservation;
      
      $tmp++;
    }

    $time_end = microtime(TRUE);
    $time = $time_end - $time_start;

    return array("time" => $time, 
                 "reservations" => $toImport,
                 "blocks" => $blocks,
                );
  }

  /* retrieveAnnouncement(title)
   * 
   * Get remote announcement given a title.
   *
   */
  
  public function retrieveAnnouncement($title){
    //$title = urlencode($title);
    $url = "https://www.vreasy.com/api/properties"; 

    $apiKey = $this->channelManager->apiKey;
    
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, "$apiKey:");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300); //five minutes

    $result = curl_exec($ch);
    
    if($result === false){
      $error = curl_error($ch);
      curl_close ($ch);
      throw new Exception($error);
    }

    $results = json_decode($result); 

    $found = array();
    foreach($results as $result){
      if($result->title == $title){
        $found[] = $result;
      }/*else{
        var_dump($title);
        var_dump($result->title);
      }*/
    }
    
    //if(count($results) == 0) return false;
    return $found;
  }

  /*
   * updateRemoteReservation
   *
   * Update an existing reservation in VReasy server.
   * See Api for the allowable object: https://www.vreasy.com/docs/public-api/#!/Reservations/updates_reservation
   *
   * reservationOldValues is not needed in vreasy communication.
   */

  public function updateRemoteReservation($data, $reservationOldValues){
    if(!isset($data["id"])){
      print_r($data);
      print_r($reservationOldValues->attributes);
      die();
    }
    $id = $data["id"];
    $url = "https://www.vreasy.com/api/reservations/$id"; 
    
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300); //five minutes
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));

    $result = curl_exec($ch);

    if($result === false){
      $error = curl_error($ch);
      curl_close ($ch);
      throw new Exception($error);
    }
    
    $results = array();
    $results["sended"] = json_decode(json_encode($data));
    $results["received"] = json_decode($result);
    
    return $results;
  }
  
  public function createRemoteReservation($data){
    $url = "https://www.vreasy.com/api/reservations/"; 

    if(isset($data->id))
      unset($data->id);
    
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300); //five minutes
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));

    $result = curl_exec($ch);

    if($result === false){
      $error = curl_error($ch);
      curl_close ($ch);
      throw new Exception($error);
    }

    $results = array();
    $results["sended"] = json_decode(json_encode($data));
    $results["received"] = json_decode($result);
    return $results;
  }

  public function retrievePayments(){
    set_time_limit(300);
    $time_start = microtime(TRUE);
    
    $after = "";
    $lastAfter = "";
    $payments = array();

    for($counter = 0; $counter < 100; $counter++){
      $url = "https://www.vreasy.com/api/payments?expand=payment_type,payee,reservation"; 
      if(!empty($after))
        $url .= "&after=".$after; 

      $ch=curl_init();
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300); //five minutes

      $results = curl_exec($ch);
      if($results === false){
        $error = curl_error($ch);
        curl_close ($ch);
        throw new Exception($error);
      }
      curl_close ($ch);

      $results = json_decode($results);
      if(count($results) == 0) break;

      $after = $results[count($results)-1]->id;
      if($after == $lastAfter) break;

      $lastAfter = $after;
      $payments = array_merge($payments, $results);
    }
    
    $time_end = microtime(TRUE);
    $time = $time_end - $time_start;
    
    return array("serverTime" => $time, "payments" => $payments);
  }

  public function contains($str, array $arr){
    foreach($arr as $a) {
      if (stripos($str,$a) !== false) return true;
    }
    return false;
  }

  public function convertRetrievedReservation($toConvert){
    $reservation = new Reservation();

    $status = ReservationStatus::model()->findByAttributes(array("name" => $toConvert->status));

    /*$channel = Channel::model()->findByAttributes(array("channelName" => $toConvert->listor->name));
    if($channel === null){
      $channel = new Channel();
      $channel->channelName = $toConvert->listor->name;
      $channel->save();
    }

    $channelId = $channel->id_channel;
    $listorId = $toConvert->listor->id;
    $channelChannelManager = ChannelManagerChannelAssociation::model()->findByAttributes(array(
      "id_channelManager" => 2,
      "id_channel" => $channelId,
      "referement" => $listorId
    ));

    if($channelChannelManager === null){
      $channelChannelManager = new ChannelManagerChannelAssociation();
      $channelChannelManager->id_channel = $channelId;
      $channelChannelManager->id_channelManager = $this->channelManagerId;
      $channelChannelManager->referement = $listorId;
      $channelChannelManager->save();
    }*/

    $channel = Channel::model()->findByAttributes(array("channelName" => $toConvert->listor->name));
    $channelId = $channel->id_channel;
    $listorId = $toConvert->listor->id;
    $channelChannelManager = ChannelManagerChannelAssociation::model()->findByAttributes(array(
      "id_channelManager" => $this->channelManagerId,
      "id_channel" => $channelId,
      "referement" => $listorId
    ));
    
    if($channelChannelManager->referement == "129"){ //BookingHomie...In this block I try to understand the source...
      $info = strtolower($toConvert->additional_description);
      /*looking for main channels*/
      if(strpos($info, "booking.com") !== false) 
        //we assume that Booking.com for this Channel Manager Account is Valid. Bookinghomie is only in one account
        $channelChannelManager = ChannelManagerChannelAssociation::model()->findByAttributes(array(
          'id_channel' => '9', //Booking system code, force to associate this reservation to Booking.com
          'id_channelManager' => $this->channelManagerId
        ));
      
      if(strpos($info, "expedia") !== false || strpos($info, "expidia") !== false) 
        //we assume that Expedia for this Channel Manager Account is Valid. Bookinghomie is only in one account 
        $channelChannelManager = ChannelManagerChannelAssociation::model()->findByAttributes(array(
          'id_channel' => '10', //Expedia system code, force to associate this reservation to Booking.com
          'id_channelManager' => $this->channelManagerId
        ));
    }

    $reservation->id_announcement = $toConvert->id_announcement;
    $reservation->id_reservationStatus = $status->id_reservationStatus;
    $reservation->id_channel = $channelChannelManager->id_channelAssociation;
    $reservation->id_currency = 1; //EUR

    $reservation->channelManagerReferement = $toConvert->id;
    $reservation->checkinDate = $toConvert->checkin;
    $reservation->checkoutDate = $toConvert->checkout;
    $reservation->bookingTime = $toConvert->created_at;
    $reservation->isBlockedDates = $toConvert->isBlockedDates;
    
    $reservation->note = CHtml::encode($toConvert->additional_description);
    
    //set net, cleaningFee and channelCommission in respect to rules definition.
    $gross = $this->getRemoteGrossPayments($toConvert);
    $reservation->updateReservationPaymentsInRulesRespect($gross);

    $guestInfos = array("adultsNumber"=> $toConvert->adults, "childsNumber" => $toConvert->kids);
    $reservation->setGuestInfos($guestInfos);

    /*create temporary business partner*/
    $businessPartner = new BusinessPartner();
    $businessPartner->name = CHtml::encode($toConvert->guest->fname);
    $businessPartner->surname = CHtml::encode($toConvert->guest->lname);
    $businessPartner->email = CHtml::encode($toConvert->guest->email);
    $businessPartner->telephone = CHtml::encode($toConvert->guest->phone);
    $reservation->setGuestInstance($businessPartner);

    if(empty($toConvert->guest->email) && empty($toConvert->guest->fname) && empty($toConvert->guest->lname))
      $reservation->noGuestWarning = 1;

    $reservation->setChmId($this->channelManagerId);
    return $reservation;
  }

  public function getRemoteGrossPayments($remote){//$announcementId, $channel){
    //get payments    
    $payments = array();
    foreach($remote->payments as $payment){
      $payments[strtolower($payment->payment_type->name)] = $payment;
    }

    $rentPayment = isset($payments["rent payment"]) ? $payments["rent payment"]->total : '0.00' ;
    $accomodationCost = $remote->accommodation_cost;

    if(floatval($accomodationCost) > floatval($rentPayment)){
      $gross = $accomodationCost;
    }else{
      $gross = $rentPayment;
    }

    return floatval($gross);
  }

  public function convertToRemoteReservation($reservation, $synchPricing){
    $remote = array();
    
    $association = AnnouncementChannelManager::model()->findByAttributes(array(
      "id_announcement" => $reservation->announcement()->id_announcement,
      "id_channelManager" => $this->channelManagerId
    ));

    if(!empty($reservation->channelManagerReferement)){
      if($reservation->channel()->id_channelManager == $this->channelManagerId)
        $remote["id"] = $reservation->channelManagerReferement;
      else{
        $channelManagerId = $this->channelManagerId;
        $decoded = json_decode($reservation->mapSynch);
        if(isset($decoded->$channelManagerId)){
          $remote["id"] = $decoded->$channelManagerId->code;
        }
      }
    }
    
    $remote["property_id"] = $association->alias;
    $channelId = $reservation->channel()->id_channel;
    
    $assoc = ChannelManagerChannelAssociation::model()->findByAttributes(array(
      "id_channelManager" => $this->channelManagerId,
      "id_channel" => $channelId
    ));
    
    $referement = "1"; //in VREasy stay for 'Not Specified' listor
    if($assoc !== null){
      $referement = $assoc->referement;
    }
    
    $remote["listor_id"] = $referement;
    $remote["checkin"] = $reservation->checkinDate;
    $remote["checkout"] = $reservation->checkoutDate;
    if($synchPricing){
      $remote["accommodation_cost"] = $reservation->net;
    }
    
    $remote["additional_description"] = CHtml::decode($reservation->note);
    
    $remote["status"] = $reservation->reservationStatus()->name;

    $remote["adults"] = $reservation->resGuestInfos()->adultsNumber;
    $remote["kids"] = $reservation->resGuestInfos()->childsNumber;

    $remote["guest"] = array();
    $remote["guest"]["fname"] = CHtml::decode($reservation->guest()->name);
    $remote["guest"]["lname"] = CHtml::decode($reservation->guest()->surname);
    if(!empty($reservation->guest()->email)) $remote["guest"]["email"] = CHtml::decode($reservation->guest()->email);
    if(!empty($reservation->guest()->telephone)) $remote["guest"]["phone"] = CHtml::decode($reservation->guest()->telephone);
    
    return $remote;
  }

  public function retrieveAnnouncements(){
    $url = "https://www.vreasy.com/api/properties";

    $ch=curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");

    $result = curl_exec($ch);
    if($result === false){
      $error = curl_error($ch);
      curl_close ($ch);
      throw new Exception($error);
    }
    curl_close ($ch);
    $results = json_decode($result);

    return $results;
  }

  public function importAnnouncements(){
    $retrieved = $this->retrieveAnnouncements();
    if(count($retrieved) == 0) return false;

    $announcements = Announcement::model()->findAll();
    foreach($retrieved as $single){
      $found = false;
      $insertAssociaton = false;
      $retrievedTitle = strtolower($single->title);

      foreach($announcements as $announcement){
        $announcementTitle = strtolower($announcement->title);
        if($retrievedTitle == $announcementTitle){
          $found = true;

          $announcement->VREasyKey = $single->id;
          $announcement->update();
          
          $instance = AnnouncementChannelManager::model()->findByAttributes(array(
            "id_announcement" => $announcement->id_announcement,
            "id_channelManager" => $this->channelManagerId,
            "alias" => $single->id
          ));
          
          if($instance === null){
            $instance = new AnnouncementChannelManager();
            $instance->id_announcement = $announcement->id_announcement;
            $instance->id_channelManager = $this->channelManagerId;
            $instance->alias = $single->id;
            $instance->aliasDescription = $single->title;
            $instance->save();
          }else{
            $instance->aliasDescription = $single->title;
            $instance->update();
          }
        }
      }
      
      if(!$found){
        $announcement = new Announcement();
        $announcement->title = $single->title;
        $announcement->VREasyKey = $single->id;
        $announcement->save();
        
        /*refactor this, replicated code*/
        $instance = new AnnouncementChannelManager();
        $instance->id_announcement = $announcement->id_announcement;
        $instance->id_channelManager = $this->channelManagerId;
        $instance->alias = $single->id;
        $instance->aliasDescription = $single->title;
        $instance->save();
      }
      
    }//end foreach
    
    return true;
  }
  
  protected function retrieveClient(){}

  protected function clientExists(){}
  //protected function reservationExists(){}

  protected function connect(){}
  protected function getReservationDifference(){}
}

?>