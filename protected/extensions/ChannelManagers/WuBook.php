<?php

class WuBook extends RemoteChannelManager{
  private $username = "";
  private $password = "";

  private $xmlApiUrl = "https://wubook.net/xrws/";
  private $token = "";
  
  private $lastSendedXml = "";
  private $lastReceivedXml = "";
  
  protected $comparedAttributes = array("id_reservationStatus");
  
  public $errorMessage = "";
  //eyildi.111522@guest.booking.com

  private $wubookStatuses = array("1" => "CONFIRMED", "2" => "ENQUIRY", "5" => "CANCELLED");

  public function __construct($istance = 3) {
    parent::__construct($istance);
    
    $this->username = $this->channelManager->apiKey;
    $this->password = $this->channelManager->password;
    
    /*var_dump($this->username);
    var_dump($this->password);
    die();*/
  }
  
  //'acquire_token'
  protected function connect(){
    //include xmlrpc library
    spl_autoload_unregister(array('YiiBase','autoload'));
    require_once(dirname(__FILE__).'/../libs/xmlrpc/lib/xmlrpc.inc');
    spl_autoload_register(array('YiiBase','autoload'));
    
    $pkey = 'f$#jigfdhsan#@$Huhajka2';
    //$pkey = 'rentopolis2016';
    
    $args= array(new xmlrpcval($this->username, 'string'), new xmlrpcval($this->password, 'string'), new xmlrpcval($pkey, 'string'));
    $response = $this->execute('acquire_token', $args);
    if($response !== false) $this->token = $response;
    
    return $response;
  }
  
  protected function disconnect(){
    $args= array(new xmlrpcval($this->token, 'string'));
    $response = $this->execute('release_token', $args);
    return $response;
  }
  
  //protected function retrieveReservations(){}
  protected function retrieveClient(){}
  protected function clientExists(){}
  protected function getReservationDifference(){}
  
  public function execute($remoteFunction, $args, $debug = false){
    $client = parent::startXmlRpc($this->xmlApiUrl);

    $message = new xmlrpcmsg($remoteFunction, $args);

    if($debug){
      echo "<pre>";
      echo CHtml::encode($message->serialize());
      echo "</pre>";
    }
    
    $response = $client->send($message);
    
    $struct = $response->value();
    $responseCode = $struct->scalarval()[0]->scalarval();
    $responseMessage = $struct->scalarval()[1]->scalarval(); //message type is managed by client methods.
    
    $this->lastSendedXml = $message->serialize();
    $this->lastReceivedXml = $response->serialize();
    
    if($responseCode < 0){
      $this->errorMessage = $responseMessage; //in error case, the message type is String.
      return false;
    }
    
    $this->errorMessage = "";
    return $responseMessage;
  }
  
  public function retrieveAnnouncements(){}
  
  public function retrieveAnnouncement($title, $extra = array()){
    $return = array();
    
    if(!isset($extra["lcode"])) return;
    $lcode = $extra["lcode"];
    
    $response = $this->connect();
    if($response !== false){
      
      $args= array(new xmlrpcval($this->token, 'string'), new xmlrpcval($lcode, 'int'));
      $response = $this->execute("fetch_rooms", $args);
      $copy = $response;
      if(!$response){
        var_dump($this->errorMessage);
      }else{
        foreach($response as $room){
          $titleFetched = $room->scalarval()["name"]->scalarval();
          if($titleFetched === $title){
            $parent = $room;
            $parentId = $parent->scalarval()["id"]->scalarval();
            $return = array(json_decode(json_encode(array("id" => $parentId, "title" => $title." - WuBook Parent Room"))));
            foreach($response as $rm){
              if($rm->scalarval()["subroom"]->scalarval() == $parentId){
                $return[] = json_decode(
                              json_encode(array(
                                "id" => $rm->scalarval()["id"]->scalarval(), 
                                "title" => $rm->scalarval()["name"]->scalarval()
                              ))
                            );
              }
            }
            break;
          }
        }
      }
      $response = $this->disconnect();
    }else{
      var_dump($this->errorMessage);
    }
    
    return $return;
  }
  
  public function retrieveReservations($extra = array()){
    $startTime = microtime(TRUE);
    $reservations = array();
    $serverTime = 0;
    $statuses = array("CONFIRMED", "CANCELLED", "ENQUIRY");
    $methodResponse = false;
    
    if(!isset($extra["lcode"])) return;
    $lcode = $extra["lcode"];
    
    $response = $this->connect();

    if($response !== false){
      $args = array(
        new xmlrpcval($this->token, 'string'), //token acquired
        new xmlrpcval($lcode, 'int'), //lcode, structure code
        new xmlrpcval("0", 'int'), // ancillary = 0
        new xmlrpcval("0", 'int') // mark = 0
      );
      $methodResponse = $this->execute("fetch_new_bookings", $args);
      $response = $this->disconnect();
    }else{
      echo $this->errorMessage;
    }
    
    $endTime = microtime(TRUE);
    $sessionTime = $endTime - $startTime;

    $json = array(
      "serverTime" => $sessionTime,
      "currentSessionTime" => $sessionTime,
      "requestedStatuses" => $statuses,
      "reservations" => $reservations
    );

    if($methodResponse !== false){
      $retrieved = array();
      foreach($methodResponse as $single){
        $res = $single->scalarval();
        $retrieved["reservation_code"] = $res["reservation_code"]->scalarval();
        $retrieved["id_channel"] = $res["id_channel"]->scalarval();
        $retrieved["rooms"] = explode(",", $res["rooms"]->scalarval());

        $retrieved["status"] = $res["status"]->scalarval();
        $retrieved["was_modified"] = $res["was_modified"]->scalarval();
        $retrieved["modified_reservations"] = $res["modified_reservations"]->scalarval();
        /*if(count($retrieved["modified_reservations"]) > 0){
          $converted = array();
          foreach($retrieved["modified_reservations"] as $singleMod){
            $converted[] = $singleMod->scalarval();
          }
          $retrieved["modified_reservations"] = $converted;
        }*/

        $retrieved["date_arrival"] = $res["date_arrival"]->scalarval();
        $retrieved["date_departure"] = $res["date_departure"]->scalarval();
        $retrieved["date_received"] = $res["date_received"]->scalarval();
        
        $retrieved["amount"] = $res["amount"]->scalarval();
        $retrieved["orig_amount"] = $res["orig_amount"]->scalarval();
        $retrieved["men"] = $res["men"]->scalarval();
        $retrieved["children"] = $res["children"]->scalarval();

        /*guest infos*/
        $retrieved["customer_name"] = $res["customer_name"]->scalarval();
        $retrieved["customer_surname"] = $res["customer_surname"]->scalarval();
        $retrieved["customer_phone"] = $res["customer_phone"]->scalarval();
        $retrieved["customer_mail"] = $res["customer_mail"]->scalarval();
        $retrieved["customer_notes"] = $res["customer_notes"]->scalarval();
        
        $reservations[] = json_decode(json_encode($retrieved));
      }
    }

    $json = array(
      "chm" => "WuBook",
      "serverTime" => $serverTime,
      "currentSessionTime" => $sessionTime,
      "requestedStatuses" => $statuses,
      "reservations" => $reservations
    );

    //die(json_encode($json));
    
    return $json;
  }

  protected function canIContinueToImport($remoteReservation){
    $parentFlag = parent::canIContinueToImport($remoteReservation);
    //in wubook when was_modified == 1 then this is an old copy of a reservation and we cannot proceed with the algorithm
    //because would cancel the reservation. Even if the modify doesn't that.
    return $parentFlag && intval($remoteReservation->was_modified) == 0;
  }

  protected function reservationExists($reservation, $remoteReservation){
    $tblChmCh = ChannelManagerChannelAssociation::model()->tableName();
    if(!empty($remoteReservation->modified_reservations)){
      $criteria = new CDbCriteria();
      $criteria->join = ' LEFT JOIN `'.$tblChmCh.'` AS `chmch` ON t.id_channel = chmch.id_channelAssociation';
      $criteria->addCondition(
        "channelManagerReferement = '".$remoteReservation->modified_reservations[0]->me->int."' AND chmch.id_channelManager = '".$reservation->getChmId()."'"
      );
      $oldCopy = Reservation::model()->find($criteria);
      if($oldCopy !== null){
        $oldCopy->channelManagerReferement = $remoteReservation->reservation_code;
        $oldCopy->update();
      }
    }
    
    return parent::reservationExists($reservation, $remoteReservation);
  }

  public function convertRetrievedReservation($toConvert){
    $reservation = new Reservation();
    $status = ReservationStatus::model()->findByAttributes(array("name" => $this->wubookStatuses[$toConvert->status]));
    $announcement = null;
    
    $room = $toConvert->rooms[0];
    $announcementAssoc = AnnouncementChannelManager::model()->findByAttributes(array(
      "alias" => $room, 
      "id_channelManager" => $this->channelManagerId
    ));
    
    if($announcementAssoc !== null){
      $announcement = $announcementAssoc->announcement();
    }
    
    $channelChannelManager = ChannelManagerChannelAssociation::model()->findByAttributes(array(
      "id_channelManager" => $this->channelManagerId, 
      "referement" => $toConvert->id_channel
    ));
    
    $reservation->id_currency = 1; //EUR
    
    $checkinHour = "15:00:00";
    $checkoutHour = "11:00:00";
    
    if($announcement !== null){
      $reservation->id_announcement = $announcement->id_announcement;
      $checkinHour = $announcement->checkin;
      $checkoutHour = $announcement->checkout;
    }
    
    $reservation->id_reservationStatus = $status->id_reservationStatus;
    $reservation->id_channel = $channelChannelManager->id_channelAssociation;
    
    $reservation->channelManagerReferement = $toConvert->reservation_code;
    $reservation->checkinDate = date_create_from_format("d/m/Y", $toConvert->date_arrival)->format("Y-m-d ".$checkinHour);
    $reservation->checkoutDate = date_create_from_format("d/m/Y", $toConvert->date_departure)->format("Y-m-d ".$checkoutHour);
    $reservation->bookingTime = date_create_from_format("d/m/Y", $toConvert->date_received)->format("Y-m-d");
    
    //update net, cleaningFee and channelCommission from gross value.
    $reservation->updateReservationPaymentsInRulesRespect($toConvert->amount);
    $reservation->isBlockedDates = 0; //in Wubook doesn't exists the unavailable status concept in reservation

    $guestInfos = array("adultsNumber"=> $toConvert->men, "childsNumber" => $toConvert->children);
    $reservation->setGuestInfos($guestInfos);

    $businessPartner = new BusinessPartner();
    $businessPartner->name = CHtml::encode($toConvert->customer_name);
    $businessPartner->surname = CHtml::encode($toConvert->customer_surname);
    $businessPartner->email = CHtml::encode($toConvert->customer_mail);
    $businessPartner->telephone = CHtml::encode($toConvert->customer_phone);
    
    $reservation->setGuestInstance($businessPartner);
    $reservation->noGuestWarning = 0;
    
    $reservation->setChmId($this->channelManagerId);
    
    return $reservation;
  }

  public function retrieveChannelSymbols(){
    $response = $this->connect();
    if($response !== false){
      
      $args = array(new xmlrpcval($this->token, 'string'));//, new xmlrpcval("1447171815", 'string'));
      $client = parent::startXmlRpc($this->xmlApiUrl);
      $message = new xmlrpcmsg("get_channel_symbols", $args);
      $methodResponse = $client->send($message)->value();
      //$methodResponse = $this->execute("get_channel_symbols", $args);
      
      $response = $this->disconnect();
    }
    
    return $methodResponse;
  }
  
  //fake conversion for internal use
  public function convertToRemoteReservation($reservation){
    return $reservation->attributes;
  }
  
  public function updateRemoteReservation($data, $reservationOldValues){
    /* If (status1, checkin1, checkout1) != (status2, checkin2, checkout2) then we have to call
     * set_avail 1 for the old checkin checkout values, if the old status is changed and was confirmed, 
     * and set avail 0 for the new values.*/
    
    /*var_dump($data);
    var_dump($reservationOldValues->attributes);
    
    die();*/    
    
    $response = $this->connect();
    
    $sended = array("message" => "");
    
    $lcri = $this->getLCodeAndParentRoomId($data["id_announcement"]);
    $lcriOld = $this->getLCodeAndParentRoomId($reservationOldValues->id_announcement);
    
    $lcode = $lcri[0];
    $lcodeOld = $lcri[0];
    
    $roomId = $lcri[1];

    if($response !== false){
      
      $newCheckin = new DateTime($data["checkinDate"]);
      $newCheckout = new DateTime($data["checkoutDate"]);
      
      $oldCheckin = new DateTime($reservationOldValues->checkinDate);
      $oldCheckout = new DateTime($reservationOldValues->checkoutDate);

      $oldStatus = $reservationOldValues->id_reservationStatus;
      $newStatus = $data["id_reservationStatus"];
      
      if($newCheckin->format("Y-m-d") != $oldCheckin->format("Y-m-d") || 
         $newCheckout->format("Y-m-d") != $oldCheckout->format("Y-m-d")){

        $fromDt = new DateTime($reservationOldValues->checkinDate);
        $now = new DateTime();

        $toDt = new DateTime($reservationOldValues->checkoutDate);

        $checkoutIsPast = (intval($toDt->format("Ymd")) - intval($now->format("Ymd")) < 0);
        
        $isPast = (intval($fromDt->format("Ymd")) - intval($now->format("Ymd")) < 0);
        if($isPast){
          $fromDt = $now;
        }

        $dfrom = $fromDt->format("d/m/Y");
        
        if(!$checkoutIsPast){
          $room = $this->convertRangeInWubookRoomDays($roomId, $fromDt->format("Y-m-d"), $reservationOldValues->checkoutDate, 1);
          $this->updateAvailability($lcodeOld, $dfrom, array($room));
        }
      }

      $avail = ReservationStatus::isBookableStatus($newStatus);
      
      $fromDt = new DateTime($data["checkinDate"]);
      $now = new DateTime();
      
      $toDt = new DateTime($data["checkoutDate"]);

      $checkoutIsPast = (intval($toDt->format("Ymd")) - intval($now->format("Ymd")) < 0);      
      
      $isPast = (intval($fromDt->format("Ymd")) - intval($now->format("Ymd")) < 0);
      if($isPast){
        $fromDt = $now;
      }
      $dfrom = $fromDt->format("d/m/Y");
      
      if(!$checkoutIsPast){
        $room = $this->convertRangeInWubookRoomDays($roomId, $fromDt->format("Y-m-d"), $data["checkoutDate"], $avail);
        $methodResponse = $this->updateAvailability($lcode, $dfrom, array($room));
      }
      $remoteId = "no_code";
      
      if(!empty($data["channelManagerReferement"])){
        $channelInstance = ChannelManagerChannelAssociation::model()->findByAttributes(array("id_channelAssociation" => $data["id_channel"]));
        if($channelInstance !== null){
          $channelManager = ChannelManager::model()->findByAttributes(array("id_channelManager" => $channelInstance->id_channelManager));
          if($channelManager !== null && $channelManager->chmName == "WUBOOK"){
            $remoteId = $data["channelManagerReferement"];
          }
        }
      }
      
      if(!$checkoutIsPast){
        $result = array("id" => $remoteId, "message" => $this->lastReceivedXml);
        $sended = array("id" => $remoteId, "message" => $this->lastSendedXml);
      }else{
        $result = array("id" => $remoteId, "message" => "{}");
        $sended = array("id" => $remoteId, "message" => "{}");
      }
      
      return array("received" => json_decode(json_encode($result)),
                   "sended" => json_decode(json_encode($sended)));
      $response = $this->disconnect();
    }
    return array("received" => json_decode(json_encode(array())),
                   "sended" => json_decode(json_encode($sended)));
  }

  public function createRemoteReservation($data){
    /* With wubook we can only set the availability to 0 for the specified dates
     * only if the status is confirmed. We skip other statuses because they doesn't alter the calendar availability
     * and I'dont want create create in external servers cancelled reservations. */
    
    $sended = array("message" => "");
    $toDt = new DateTime($data["checkoutDate"]);
    $now = new DateTime();
    
    if(intval($toDt->format("Ymd")) - intval($now->format("Ymd")) < 0)
      return array("received" => json_decode(json_encode(array("id" => "no_code"))),
                   "sended" => json_decode(json_encode($sended)));
    
    /*die("id_reservation = ".$data["id_reservation"].", "."id_status = ".$data["id_reservationStatus"].", isBlockedDates = ".$data["isBlockedDates"]);*/
    
    if(ReservationStatus::isBookableStatus($data["id_reservationStatus"]) && intval($data["isBlockedDates"]) == 0)
      return array("received" => json_decode(json_encode(array("id" => "no_code"))),
                   "sended" => json_decode(json_encode($sended)));
    
    $response = $this->connect();
    if($response !== false){
      $lcri = $this->getLCodeAndParentRoomId($data["id_announcement"]);
      $lcode = $lcri[0];
      $roomId = $lcri[1];

      $fromDt = new DateTime($data["checkinDate"]);
      
      $isPast = (intval($fromDt->format("Ymd")) - intval($now->format("Ymd")) < 0);
      if($isPast){
        $fromDt = $now;
      }
      
      $dfrom = $fromDt->format("d/m/Y");
      $room = $this->convertRangeInWubookRoomDays($roomId, $fromDt->format("Y-m-d"), $data["checkoutDate"], 0);
      
      $methodResponse = $this->updateAvailability($lcode, $dfrom, array($room));
        
      $result = array("id" => "no_code", "message" => $this->lastReceivedXml);
      $sended = array("id" => "no_code", "message" => $this->lastSendedXml);
      return array("received" => json_decode(json_encode($result)),
                   "sended" => json_decode(json_encode($sended))); 
    }
    
    $response = $this->disconnect();
    return array("received" => json_decode(json_encode(array())),
                   "sended" => json_decode(json_encode($sended)));
  }

  private function updateAvailability($lcode, $dfrom, $rooms){
    $args = array(
      new xmlrpcval($this->token, 'string'), 
      new xmlrpcval($lcode, 'int'), 
      new xmlrpcval($dfrom, 'string'),
      php_xmlrpc_encode($rooms)
    );
    $methodResponse = $this->execute('update_avail', $args);
    return $methodResponse;
  }
  
  private function getLCodeAndParentRoomId($listingId){
      $announcement = Announcement::model()->findByAttributes(array("id_announcement" => $listingId));
      if($announcement === null || empty($announcement->wubook_lcode))
        return null;
    
      $lcode = $announcement->wubook_lcode;
  
      $criteria = new CDbCriteria();
      $criteria->condition = "id_announcement = :listingId AND id_channelManager = :channelManagerId AND aliasDescription REGEXP '.* Parent Room'";
      $criteria->params = array(
        ":listingId" => $listingId,
        ":channelManagerId" => $this->channelManagerId);
      $parentRoom = AnnouncementChannelManager::model()->find($criteria);
      if($parentRoom === null)
        return null;

      $roomId = $parentRoom->alias;
        
      return array($lcode, $roomId);
  }
  
  /*public function getRoomParameterForAvailMethod($roomId, $checkin, $checkout, $availability){
    $fromDt = new DateTime($checkin);
    $dfrom = $fromDt->format("d/m/Y");
  }*/
  
  private function convertRangeInWubookRoomDays($roomId, $checkin, $checkout, $availability){
    $fromDt = new DateTime($checkin);
    $toDt = new DateTime($checkout);
    
    $now = new DateTime();
    
    $now->setTime(12, 0, 0);
    //force same hours
    $fromDt->setTime(12, 0, 0); 
    $toDt->setTime(12, 0, 0);
    
    //get the day difference
    $days = $toDt->diff($fromDt)->days;
    
    $room = array(
      "id" => $roomId,
      "days" => array()
    );
    for($count = 0; $count < $days; $count++){
      $room["days"][] = array(
        "avail" => $availability
      );
    }
    
    return $room;
  }
}

?>