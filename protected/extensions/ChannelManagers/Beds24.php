<?php

class Beds24 extends RemoteChannelManager{
  public function importAnnouncements($business = 1){
    
    $json = array("authentication" => array("apiKey" => "=W6YmGU'^#[k5WRQ"));
    $url = "https://api.beds24.com/json/getProperties";
    
    $json = json_encode($json);
    
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_POST, 1) ;
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $result = curl_exec($ch);

    if($result === false){
      $error = curl_error($ch);
      curl_close ($ch);
      throw new Exception($error);
    }
    
    curl_close ($ch);

    $results = json_decode($result);
    
    $properties = $results->getProperties;
    foreach($properties as $property){
      $announcement = new Announcement();
      $announcement->id_business = $business;
      $announcement->title = $property->name;
      $announcement->beds24ApiKey = $property->propKey;
      if(!$announcement->save()){
        echo $property->name."<br/>";
        print_r($announcement->getErrors());
        echo "<hr/>";
      }else{
        $rooms = $property->roomTypes;
        $association = new AnnouncementChannelManager();
        $association->id_channelManager = 4;
        $association->id_announcement = $announcement->id_announcement;
        $association->alias = $property->propId;
        $association->aliasDescription = $announcement->title;
        if(!$association->save()){
          print_r($association->getErrors());
        }
        
        foreach($rooms as $room){
          $association = new AnnouncementChannelManager();
          $association->id_channelManager = 4;
          $association->id_announcement = $announcement->id_announcement;
          $association->alias = $room->roomId;
          $association->aliasDescription = $room->name;
          if(!$association->save()){
            print_r($association->getErrors());
          }
        }
      }
    }
  }
  
  public function remoteReservations($propKey, $title){
    $url = "https://api.beds24.com/json/getBookings";
    $jsonReservations = array();
    $jsonZeroWithInvoices = array();
    $jsonZeroPriceReservations = array();
    $jsonAvailabilities = array();
    
    $json = array("authentication" => array("apiKey" => "=W6YmGU'^#[k5WRQ", "propKey" => $propKey));
    //$json["bookingTime"] = "20151010 01:00:00";
    $json = json_encode($json);
    
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_POST, 1) ;
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $result = curl_exec($ch);
    
    if($result === false){
      $error = curl_error($ch);
      curl_close ($ch);
      throw new Exception($error);
    }
    
    curl_close ($ch);
    $results = json_decode($result);
    
    if(isset($results->error)){
      //$announcement = Announcement::model()->findByAttributes(array("propKey" => $propKey));
      //propKey does not match.
      $results->propKey = $propKey;
      $results->announcement = $title;
      return $results;
    }
    
    $exceptions = array("block", "hold", "owner", "propr", "blocc", "close", "chius", "rentopolis", "retopolis");
    
    foreach($results as $retrieved){
      
      $alias = AnnouncementChannelManager::model()->findByAttributes(array("alias" => $retrieved->roomId));
      if($alias !== null){
        $retrieved->title = $alias->aliasDescription;
      }
      
      $retrieved->referer = strtoupper($retrieved->referer);
      $retrieved->parsedApiMessage = $this->parseApiMessage($retrieved);
      /*if($retrieved->bookingTime > "2015-10-10 01:00:00")
        continue;*/
      
      /*prevent the empty needle warning*/
      if(empty($retrieved->guestName)) $retrieved->guestName = " ";
      if(empty($retrieved->guestFirstName)) $retrieved->guestFirstName = " ";
      if(empty($retrieved->guestTitle)) $retrieved->guestTitle = " ";
      
      if($this->contains($retrieved->guestName, $exceptions) || $this->contains($retrieved->guestFirstName, $exceptions) ||
         $this->contains($retrieved->guestTitle, $exceptions)
        || (!empty($retrieved->parsedApiMessage) && !empty($retrieved->parsedApiMessage["PORTAL"]) && 
             (strpos($retrieved->parsedApiMessage["PORTAL"], "not specified") !== false)
           )
        ){
        $jsonAvailabilities[] = $retrieved;
      }else{
        if(intval($retrieved->price) == 0 /*&& intval($retrieved->status) == 2*/
           && (empty($retrieved->parsedApiMessage) || (intval($retrieved->parsedApiMessage["PRICE"]) == 0))){
          $jsonZeroPriceReservations[] = $retrieved;
       }else
          $jsonReservations[] = $retrieved;
      }
    }
    
    return array(
      "reservations" => $jsonReservations, 
      "reservationsZeroPrice" => $jsonZeroPriceReservations,  
      "blocks" => $jsonAvailabilities
    );
  }
  
  public function contains($str, array $arr){
    foreach($arr as $a) {
      if (stripos($str,$a) !== false) return true;
    }
    return false;
  }
  
  public function containsFirst($str, array $arr, $index = true){
    $i = 0;
    foreach($arr as $a) {
      if (stripos($str,$a) !== false){
        if($index === true)
          return $i;
        else
          return $a;
      } 
      $i++;
    }
    return false;
  }
  
  public function parseApiMessage($retrieved){
    $referer = $retrieved->referer;
    $apiMessage = $retrieved->apiMessage;
    
    $parsed = array();
    
    if($apiMessage != "" && strpos(strtolower($referer), "ical import") !== false){
      $posDes = strpos(strtoupper($apiMessage), "DESCRIPTION:");
      $posStatus = strpos($apiMessage, "STATUS:");
      //$description = substr($apiMessage, $posDes+12, $posStatus - $posDes);
      $description = substr($apiMessage, $posDes+12, $posStatus - $posDes);
      $description = preg_replace("/STATUS:(.*)/", "", $description);

      $parsed["DESCRIPTION"] = $description;

      $searchPortal = preg_replace("/\s*\\\\n\s*/", "", $apiMessage);
      $searchPortal = preg_replace("/\s*\n\s*/", "", $searchPortal);
      $searchPortal = preg_replace("/\s*\r\s*/", "", $searchPortal);
      $posPortal = strpos(strtoupper($searchPortal), "PORTAL:");
      $posStatus = strpos($searchPortal, "STATUS:");
      //$portal = trim(substr($apiMessage, $posPortal+7, $posStatus - $posPortal));
      $portal = trim(preg_replace("/(.*):\s*/", "", substr($searchPortal, $posPortal, $posStatus - $posPortal)));
      $parsed["PORTAL"] = $portal;
      
      $parsed["PRICE"] = "";
 
      $searchInDescription = $this->removeFormattingCharacter($description);
      $searchInDescription = strtolower($searchInDescription);
      
      $tokensCommission = array("total price::", "total confirmed to guest", "total:: eur", "amount for stay::", "price::");
      $tokensPostCommission = array("commission", "excluded", "history", "euros", "arrival");
      
      if(intval($retrieved->price) == 0 && $this->contains($searchInDescription, $tokensCommission)){
        
        $index = $this->containsFirst($searchInDescription, $tokensCommission);
        $token = $tokensCommission[$index];
        $posPrice = strpos($searchInDescription, $token);
        $posPostPrice = strpos($searchInDescription, $tokensPostCommission[$index], $posPrice);
        $offset = strlen($token);
        
        $price = substr($searchInDescription, $posPrice+$offset, $posPostPrice-$posPrice);

        foreach($tokensPostCommission as $token){
          $price = trim(preg_replace("/".$token."(.*)/", "", $price));
        }

        $parsed["PRICE"] = $price;
        if(intval($price) == 0) $parsed["uhm"] = "errore";
      }
      
      /*check the portal in detail in case of generic portals was retrieved from informations*/
      $genericPortal = array("Kigo", "BookingHomie");
      $tokenReservationSource = array("reservation source::", "reservation source::");
      $tokenPostSource = array("reservation #", "reservation #");

      if($this->contains($portal, $genericPortal)){
        $index = $this->containsFirst($portal, $genericPortal);
        $token = $tokenReservationSource[$index];
        $posToken = strpos($searchInDescription, $token);
        $posPostToken = strpos($searchInDescription, $tokenPostSource[$index], $posToken);
        $offset = strlen($token);
        if($posToken !== false && $posPostToken !== false && $posToken < $posPostToken){
          $p = substr($searchInDescription, $posToken+$offset, $posPostToken-$posToken);
          foreach($tokenPostSource as $token){
            $p = trim(preg_replace("/".$token."(.*)/", "", $p));
          }
          
          $parsed["PORTAL"] = $p;
        }else if(strpos($searchInDescription, "@guest.booking")){
          $parsed["PORTAL"] = "BOOKING.COM";
        }
      }
      
      $parsed["PORTAL"] = strtoupper($parsed["PORTAL"]);
      $parsed["PORTAL"] = $this->removeFormattingCharacter($parsed["PORTAL"]);
      
      $apiMessageWithoutDescription = str_replace("DESCRIPTION:".$parsed["DESCRIPTION"], "", $apiMessage);
      /*$apiMessageToExplode = str_replace("\\\\n", "\r\n", $apiMessageWithoutDescription);
      $apiMessageToExplode = str_replace("\\n", "\r\n", $apiMessageWithoutDescription);*/
      $exploded = explode("\r\n",$apiMessageWithoutDescription);

      foreach($exploded as $token){
        $length = strlen($token);
        $i = strpos($token, ":");
        if($i !== false){
          $parsed[strtoupper(substr($token, 0, $i))] = substr($token, $i+1); 
        }
      }
    }

    return $parsed;
  }
  
  public function removeFormattingCharacter($string){
    $string2 = preg_replace("/\\\\n\s*/", "", $string);
    $string2 = preg_replace("/\s*\r\n\s*/", "", $string2);
    $string2 = preg_replace("/\n\s*/", "", $string2);
    $string2 = preg_replace("/'/", "", $string2);
    $string2 = preg_replace("/,/", "", $string2);
    $string2 = preg_replace("/\t/", "", $string2);
    $string2 = preg_replace("/\\\\/", "", $string2);
    $string2 = preg_replace("/€/", "", $string2);
    return $string2;
  }
  
  public function retrieveReservations($business = 1){    
    $announcements = Announcement::model()->findAllByAttributes(array("id_business" => $business/*, "beds24ApiKey" => "=Y7,03$5&/s+*|vW"*/));
    $reservations = array();
    
    $i = 0; $numRes = 0; $numBlocks = 0;
    $retrieved = array(); $reservations = array(); $reservationsZeroPrice = array(); $blocks = array(); $errors = array();
    
    $time_start = microtime(TRUE);
    
    foreach($announcements as $announcement){
      $remoteRetrieved = $this->remoteReservations($announcement->beds24ApiKey, $announcement->title);
            
      if(isset($remoteRetrieved->error)){
        $errors[] = $remoteRetrieved;
        continue;
      }
      
      $reservations = array_merge($reservations, $remoteRetrieved["reservations"]);
      $reservationsZeroPrice = array_merge($reservationsZeroPrice, $remoteRetrieved["reservationsZeroPrice"]);
      $blocks = array_merge($blocks, $remoteRetrieved["blocks"]);
      
      $numRes += count($remoteRetrieved["reservations"]);
      $numBlocks += count($remoteRetrieved["blocks"]);
      $retrieved[] = $remoteRetrieved;
    }
    
    $time_end = microtime(TRUE);
    $time = $time_end - $time_start;
    
    $portals = $this->getPortals($reservations);
    $portals = $this->getPortals($reservationsZeroPrice, $portals);
    
    $zeroPortals = $this->getPortals($reservationsZeroPrice);
    $zeroPortalsBeforeOctober = $this->getZeroPortalsBeforeOctober($reservationsZeroPrice);
    $zeroPortalsAfterOctober = $this->getZeroPortalsAfterOctober($reservationsZeroPrice);

    return array(
      "time" => round($time, 3), 
      "reservations" => $reservations,  
      "blocks" => $blocks, 
      "errors" => $errors, 
      "numRes" => $numRes, 
      "numBlocks" => $numBlocks,
      "portals" => $portals,
      "zeroPortals" => $zeroPortals,
      "zeroPortalsBefore5-October" => $zeroPortalsBeforeOctober,
      "zeroPortalsAfter5-October" => $zeroPortalsAfterOctober
    );
  }
  
  protected function getZeroPortalsBeforeOctober($reservations){
    $beforeOctober = array();
    foreach($reservations as $reservation){
      $date1 = new DateTime($reservation->bookingTime);
      $date2 = new DateTime("2015-10-05");
      if($date1 < $date2)
        $beforeOctober[] = $reservation;
    }
    return $beforeOctober;
  }
  
  protected function getZeroPortalsAfterOctober($reservations){
    $afterOctober = array();
    foreach($reservations as $reservation){
      $date1 = new DateTime($reservation->bookingTime);
      $date2 = new DateTime("2015-10-05");
      if($date1 >= $date2)
        $afterOctober[] = $reservation;
    }
    return $afterOctober;
  }
  
  protected function getPortals($sources, $portals = array()){
    foreach($sources as $source){
      $referer = $source->referer;
      if(strtolower($referer) == "ical import 1")
        if(!empty($source->parsedApiMessage["PORTAL"]))
          $referer = $source->parsedApiMessage["PORTAL"];
        else
          $referer = "undefined";
        
        if(empty($portals[$referer])) $portals[$referer] = 0;
        $portals[$referer]++;
    }
    return $portals;
  }
  
  
  protected function retrieveClient(){}
  protected function retrieveAnnouncements(){}

  protected function clientExists(){}
  protected function reservationExists(){}

  protected function connect(){}
  protected function getReservationDifference(){}
}

?>