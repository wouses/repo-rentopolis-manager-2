

/* There are three fundamentals classes in this file.
 * 
 * Day: represents a single day in a particular month.
 * Line: one line can represents a set of disjoint date ranges. All of this ranges are composed by a set of days.
 * Calendar: this class manages the layout representation and all entities in the calendar/s envirioment.
 *
 * Author: Marco Distrutti.
 *
 */


/******************************
 *  Day Class
 ******************************/

function Day(date, selectedDate){
	this.date = new Date(date);
	this.description = "";
  this.price = "";
	this.extraClassName = "single-day";
	this.dayName = this.getDayNames()[this.getDay()];
	this.datas = [];

	if(this.isInWeekend()) this.extraClassName += " weekend-date";
	if(this.isToday()) this.setExtra("today-date");
  if((selectedDate !== undefined || selectedDate != null) && this.isSameDate(selectedDate))
    this.setExtra("selected-date");
  
  this.setData("date", Day.prototype.toISONoTz(this.date));
  //default value for reservationdetail
  this.setData('reservationdetail', '{"checkin": "' + Day.prototype.toISONoTz(this.date) + '"}');
}

Day.prototype.today = new Date();

Day.prototype.isToday = function(){
	var isoToday = Day.prototype.toISONoTz(this.today);//.toISOString().substr(0, 10);
	var isoDate = Day.prototype.toISONoTz(this.date);//.toISOString().substr(0, 10);

	return isoToday == isoDate;
}

Day.prototype.isSameDate = function(selectedDate){
  var compareWith = new Date(selectedDate);
  return this.toISONoTz(this.date) == this.toISONoTz(compareWith);
}

Day.prototype.setDescription = function(description){
	this.description = description;
}

Day.prototype.setExtra = function(extra){
	this.extraClassName += " " + extra;
}

Day.prototype.setPrice = function(price){
  this.price = price;
}

Day.prototype.removeExtra = function(extra){
	this.extraClassName = this.extraClassName.replace("start-description", "");
}

Day.prototype.hasExtra = function(extra){
	return this.extraClassName.indexOf(extra) > -1;
}

Day.prototype.setData = function(data, value){
	this.datas[data] = value;
}

Day.prototype.getData = function(data){
	return this.datas[data];
}

Day.prototype.getDatas = function(){
	return this.datas;
}

Day.prototype.getHtmlDatas = function(){
	var htmlAttributes = "";
	for(d in this.datas){
		htmlAttributes += 'data-' + d + "='" + this.datas[d] + "' ";
	}
	return htmlAttributes;
}

Day.prototype.getDay = function(extra){
	return this.date.getDay();
}

Day.prototype.isInWeekend = function(){
	var day = this.date.getDay();
	return (day == 0 || day == 6);
}

Day.prototype.getDayNames = function(show){
	return CConfigurator.getDayNames(show);
}

Day.prototype.toISONoTz = function(date){
	var dt = new Date(date);
	var m = dt.getMonth()+1;
	var d = dt.getDate();
	var string = dt.getFullYear() + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
	//console.log(string);
	return string;
}

/******************************
 *  Line Class
 ******************************/

function Line(config){
	var date;
	this.days = [];
	this.startFrom = config.startFrom;
	this.ranges = config.ranges;
	this.label = config.label;
	this.id = ++Line.prototype.count;
	this.calendarCells = config.calendarCells;
  this.calendarWidth = config.calendarWidth;
  this.showCurrentDate = config.showCurrentDate;
  this.showToday = config.showToday;
  
	//console.log(this.startFrom + " " + this.label);
	this.extra = config.extra ? config.extra : "";

  this.customFields = config.customFields;
  
	//this.dateStartVisibilityRange.setDate(this.dateStartVisibilityRange.getDate()-1);
	this.dateStartVisibilityRange = new Date(this.startFrom);
	this.dateStartVisibilityRange = new Date(this.dateStartVisibilityRange.getFullYear(), this.dateStartVisibilityRange.getMonth(), this.dateStartVisibilityRange.getDate()-1);

  this.selectedDate = config.selectedDate || null;
  
	this.days = this.addDays(this.dateStartVisibilityRange, config.cells+1, this.selectedDate);
	
	date = new Date(this.startFrom);
	this.dateEndVisibilityRange = new Date(date.getFullYear(), date.getMonth(), date.getDate() + config.cells-1);
	this.endTo = Day.prototype.toISONoTz(this.dateEndVisibilityRange);//.toISOString().substring(0, 10);

	this.dateTo = new Date(this.endTo);
	/*without timezone, adeguated to this.days*/
	this.dateFrom = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	this.dateTo = new Date(this.dateTo.getFullYear(), this.dateTo.getMonth(), this.dateTo.getDate());

	if(config.ranges != undefined && config.ranges != null)
		this.initRanges(this.ranges);
}

Line.prototype.count = -1;

Line.prototype.addDays = function(start, distance, selectedDate){
	var days = [];
	//date.setDate(date.getDate()+1) inc by 24h, 
	//there are some problem with the timezone with this method
	//so I prefere inc with date = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1).
	start = new Date(start.getFullYear(), start.getMonth(), start.getDate());
	for(var c = 0; c < distance; c++, start = new Date(start.getFullYear(), start.getMonth(), start.getDate()+1)){
		days.push(new Day(start, selectedDate));
	}
	return days;
}

Line.prototype.cloneLine = function(line, ranges, extraParameters){
	var line2 = {startFrom: line.startFrom, ranges: line.ranges, label: line.label, calendarCells: line.calendarCells, cells: line.cells};
	if(ranges)
		line2.ranges = ranges;

  line2.showCurrentDate = line.showCurrentDate;
  line2.showToday = line.showToday;
  line2.calendarWidth = line.calendarWidth;
  
	for(p in extraParameters){
		param = extraParameters[p];
		line2[p] = param;
	}

	return line2;
}

Line.prototype.setRange = function(range){
	
	var startRange = new Date(range.start);
	startRange = new Date(startRange.getFullYear(), startRange.getMonth(), startRange.getDate());
	var endRange = new Date(range.end);
	endRange = new Date(endRange.getFullYear(), endRange.getMonth(), endRange.getDate());
	
	var startVisibilityISO = Day.prototype.toISONoTz(this.dateStartVisibilityRange);//.toISOString().substring(0, 10);
	var startVisibility = new Date(startVisibilityISO);
	
	var endVisibilityISO = Day.prototype.toISONoTz(this.dateEndVisibilityRange);//.toISOString().substring(0, 10);
	var endVisibility = new Date(endVisibilityISO);

	var startFromDate = new Date(this.startFrom);
	var start = this.getDistance(startVisibilityISO, range.start);

	if(startRange > endRange) return; //invalid range.
	if(endRange < this.dateStartVisibilityRange) return;
	if(startRange > this.dateEndVisibilityRange) return;

	if(startRange < this.dateStartVisibilityRange /*&& endRange >= this.dateStartVisibilityRange*/){
		var startVisibilityDateString = Day.prototype.toISONoTz(this.dateStartVisibilityRange);//.toISOString().substring(0, 10);
		var distance = start; //the calculated start is the negative distance form the actual start point
							  //we need to change the start point.
		var days = [];
		var date = new Date(range.start);
		
		days = this.addDays(date, distance, this.selectedDate);

		/*for(var i = 0; i < distance; i++, date.setDate(date.getDate()+1)){
			days.push(new Day(date));
		}*/
		this.days = days.concat(this.days);
		start = 0; //reset start to zero offset
		startVisibilityDateString = range.start; //change the new start point.
		this.dateStartVisibilityRange = new Date(startVisibilityDateString);

		/*console.log("START " + this.label);
		console.log(days);*/
	}

	if(/*startRange <= this.dateEndVisibilityRange &&*/ endRange > this.dateEndVisibilityRange){
		var days = [], distance = this.getDistance(range.end, endVisibilityISO), date = new Date(endVisibilityISO);
		date.setDate(date.getDate()+1);
		
		days = this.addDays(date, distance, this.selectedDate);

		/*for(var i = 0; i < distance; i++, date.setDate(date.getDate()+1)){
			days.push(new Day(date));
		}*/
		this.days = this.days.concat(days);
		this.dateEndVisibilityRange = new Date(range.end);
		this.dateEndVisibilityRange = 
      new Date(this.dateEndVisibilityRange.getFullYear(), this.dateEndVisibilityRange.getMonth(), this.dateEndVisibilityRange.getDate());

		/*console.log("END  " + this.label);
		console.log(days);*/
	}

	length = this.getDistance(range.start, range.end) + 1;
	for (var i = start; i < start + length; i++) {
		if(this.days[i] != undefined){
			this.days[i].setDescription(range.description);
			this.days[i].setPrice(range.price);
			this.days[i].setExtra(range.extra);
      this.days[i].setData('reservationdetail', JSON.stringify(range));
			/*this.days[i].setData(
        'reservationdetail', 
        '{"price": '+range.price+', "description": "'+range.description+'", "checkin" : "'+range.start+'", "checkout":"' + range.end + '"}');*/
      this.days[i].setExtra("in-range");
			this.days[i].source = (range.source ? range.source : null);
		}
	}
	this.days[start].isRangeStart = true;
	//this.days[start]
	this.days[start].extraClassName += " start-range";
	this.days[start].rangeLength = length;
	
	var hasDescriptor = this.days[start].hasExtra("INQUIRY") || this.days[start].hasExtra("ENQUIRY") 
                      || this.days[start].hasExtra("CONFIRMED");
	var descriptionIndex = start;
	var descriptionLength = length;

	if(hasDescriptor){
		this.days[start].setExtra("start-description");
		this.days[start].descriptionLength = descriptionLength;
		this.days[start].setData("description-length", descriptionLength);
	}

	if(this.days[start+length-1] != undefined) this.days[start+length-1].isRangeEnd = true;
	if(this.days[start+length-1] != undefined) this.days[start+length-1].extraClassName += " end-range";

	if(hasDescriptor && this.days[start].date < this.dateFrom && this.days[start+length-1].date >= this.dateFrom){ 
		var distance = this.getDistance(this.days[start].date, this.dateFrom);
		var day = new Date(this.days[start].date.getFullYear(), this.days[start].date.getMonth(), this.days[start].date.getDate()+distance);

		this.days[start].removeExtra("start-description");
		delete this.days[start].descriptionLength;

		descriptionIndex = start+distance;
		descriptionLength = descriptionLength - distance;

		this.days[descriptionIndex].setExtra("start-description");
		this.days[descriptionIndex].setData("description-length", length - distance);
		this.days[descriptionIndex].descriptionLength = descriptionLength;

		//console.log(this.label + " range(" + range.start + ", " + range.end + ") ; dateFrom = " + Day.prototype.toISONoTz(this.dateFrom) + " ; distance = " + this.getDistance(this.days[start].date, this.dateFrom));
	}

	if(hasDescriptor && this.days[start+length-1].date > this.dateTo){
		var descriptionLength = descriptionLength - this.getDistance(this.dateTo, this.days[start+length-1].date);
		this.days[descriptionIndex].setData("description-length", descriptionLength);
		this.days[descriptionIndex].descriptionLength = descriptionLength;
	}
}

Line.prototype.initRanges = function(ranges){
	for(var i = 0; i < ranges.length; i++){
		range = ranges[i];
		this.setRange(range);
	}
}

Line.prototype.getDistance = function(start, end){
	d1 = new Date(start); d2 = new Date(end);
  return Math.abs(Math.round( (d2 - d1) / (1000 * 60 * 60 * 24)));
	//return Math.ceil(Math.abs(d2.getTime() - d1.getTime()) / (1000 * 3600 * 24))
}

Line.prototype.getHtml = function(){
  var showToday = (this.showToday ? "showToday" : "");
  var showCurrentDate = (this.showCurrentDate ? "showCurrentDate" : "");
  
  var container = '<div class="line-container ' + showToday + ' ' + showCurrentDate + '"><div class="line-calendar-first-column ' + this.extra + '" style="text-align:right;padding-right:3px;width:150px;">'+
						'<small style="display:block;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">' + this.label + '</small></div>{line}</div>',
		line = '<div class="line-calendar-second-column" style="width:' + window.lineContentWidth + 'px;"><ul class="line">',
		isNoBorderRight = false,
		startFromDate = new Date(this.startFrom);
		startFromDate = new Date(startFromDate.getFullYear(), startFromDate.getMonth(), startFromDate.getDate());
		endToDate = new Date(this.endTo);
		endToDate = new Date(endToDate.getFullYear(), endToDate.getMonth(), endToDate.getDate());

	for(var i = 0; i < this.days.length; i++){ 

		var day = this.days[i],
			isStartRange = day.extraClassName.search("start-range") > -1, 
			isConfirmed = day.extraClassName.search("CONFIRMED") > -1,
			isInquiry = day.extraClassName.search("INQUIRY") > -1 || day.extraClassName.search("ENQUIRY") > -1,
			hasSource = day.source ? true : false,
			showDescriptor = isConfirmed || isInquiry,
			isVisible = startFromDate <= day.date && endToDate >= day.date;

		isNoBorderRight = (i == this.days.length-1 && this.calendarCells > this.days.length ? true : false);
		dataRangeLength = day.rangeLength;

		/*if(isStartRange){
			rangeEnd = new Date(day.date.getFullYear(),day.date.getMonth(), day.date.getDate()+day.rangeLength-1);
			if(rangeEnd >  endToDate){
				dataRangeLength = day.rangeLength - this.getDistance(endToDate, rangeEnd);
			}
		}*/

		line += '<li id="' + this.id + '-' + Day.prototype.toISONoTz(day.date) + '" ' 
		//line += '<li id="' + this.id + '-' + day.date.toISOString().substr(0, 10) + '" ' 
					+ 'class="' + day.extraClassName + ' ' 
								+ (isStartRange && showDescriptor && isVisible ? 'description-start' : '') + ' '
								+ (!isVisible ? 'invisible-date' : '') + ' '
                + (day.hasExtra("start-description") && !hasSource ? 'no-source' : '')
								//+ (isNoBorderRight ? 'no-border-right' : '')
								+ '" '
					+ day.getHtmlDatas() + ' '
					+ 'data-description="' + day.description + '" '
					+ 'data-price="' + day.price + '" '
					+ ( isStartRange && showDescriptor ? 'data-length="' + dataRangeLength + '"' : '')
          + ' style="width:' + ((1/this.calendarCells)*100) + '%"'
				+'>' 
				//+ ( isStartRange && hasSource ? 
				+ ( day.hasExtra("start-description") && hasSource ?
					'<img class="source-image" src="' + day.source.url + '">' 
					: ''
				  )
				+'<div class="overlay-date"></div>'
				+'</li>';
	}

	if((this.getDistance(endToDate, startFromDate)+1) < this.calendarCells){
		var difference = this.calendarCells - (this.getDistance(endToDate, startFromDate)+1);
		for(var i = 0; i < difference; i++){
			line += '<li class="other-month-date"></li>';
		}
	}

	line += '</ul></div>';

	container = container.replace('{line}', line);

	return container;
}

/******************************
 *  Calendar Class
 ******************************/

function Calendar(id, calendarConfig){

	lines = calendarConfig.lines;
	this.lines = [];
	this.count = ++Calendar.prototype.count;
	
	this.aroundSelectedDate = (calendarConfig.aroundSelectedDate ? calendarConfig.aroundSelectedDate : false);
	this.beforeSelectedDate = false;
	this.highLightSelectedDate = false;

  this.calendarWidth = calendarConfig.width;
  
  this.fixedHeader = (calendarConfig.fixedHeader ? calendarConfig.fixedHeader : false);
  
	this.noOverbookings = (calendarConfig.noOverbookings ? true : false);

  this.onDateClick = calendarConfig.onDateClick || (function(){});
  this.onAfterRender = calendarConfig.onAfterRender || (function(){});
  
	if(this.aroundSelectedDate){
		this.beforeSelectedDate = (calendarConfig.beforeSelectedDate ? calendarConfig.beforeSelectedDate : 5);
		this.highLightSelectedDate = (calendarConfig.highLightSelectedDate ? calendarConfig.highLightSelectedDate : false);
    
    this.selectedDate = this.aroundSelectedDate;
		this.aroundSelectedDate = new Date(this.aroundSelectedDate);
		this.aroundSelectedDate.setDate(this.aroundSelectedDate.getDate() - this.beforeSelectedDate);
		this.startFrom = Day.prototype.toISONoTz(this.aroundSelectedDate);
	}else
		this.startFrom = calendarConfig.startFrom;
	

	this.label = (calendarConfig.label ? calendarConfig.label : "&nbsp;");
	this.cells = calendarConfig.cells;

	this.id = id;

	this.showWeekends = (calendarConfig.showWeekends ? true : false);
	this.showCurrentDate = (calendarConfig.showCurrentDate ? true : false);
	this.showToday = (calendarConfig.showToday ? true : false);

	for(var l = 0; l < lines.length; l++){
		line = lines[l];
		line.showCurrentDate = this.showCurrentDate;
		line.showToday = this.showToday;
    line.calendarWidth = this.calendarWidth;
    
		if(this.aroundSelectedDate){
			line.cells = this.cells;
			line.startFrom = this.startFrom;
      line.selectedDate = this.selectedDate;
		}

		line.calendarCells = calendarConfig.cells;

		line = this.sanitazeLine(line);
		this.lines.push(new Line(line));

		if(!this.noOverbookings)
			for(var i in line.overbookings){
        line.overbookings[i].selectedDate = this.selectedDate;
				this.lines.push(new Line(line.overbookings[i]));
			}
	}
  
  this.linesById = {};
  
  for(var counter = 0; counter < this.lines.length; counter++){
    this.linesById[this.lines[counter].id] = this.lines[counter];
  }

	this.render(id);
}

Calendar.prototype.getLineById = function(id){
  return this.linesById[id];
}

Calendar.prototype.count = 0;

Calendar.prototype.sanitazeLine = function(line){
	for(var r1 = 0; r1 < line.ranges.length; r1++){
		var range1 = line.ranges[r1];
		for(var r2 = r1+1; r2 < line.ranges.length; r2++){
			var range2 = line.ranges[r2];
			if(this.overlaps(range1, range2)){
				var found;
				if(!line.overbookings){
					line.overbookings = [];
					line.overbookings.push(
						Line.prototype.cloneLine(line, [line.ranges.splice(r2, 1)[0]], {extra: "overbook"})
					);
					r2--;
				}else{
					for(var o1 = 0; o1 < line.overbookings.length; o1++){
						found = false;
						for(var o2 = 0; o2 < line.overbookings[o1].ranges.length; o2++){
							if(this.overlaps(range2, line.overbookings[o1].ranges[o2])){
								found = true;
							}
						}

						if(!found){
							line.overbookings[o1].ranges.push(
								line.ranges.splice(r2, 1)[0]
							);
							r2--;
							break;
						}
					}
					if(found){
						line.overbookings.push(
							Line.prototype.cloneLine(line, [line.ranges.splice(r2, 1)[0]], {extra: "overbook"})
							//{ranges: [line.ranges.splice(r2, 1)[0]]}
						);
						r2--;
					}
				}
			}
		}
	}
	return line;
}

Calendar.prototype.overlaps = function(range1, range2){
	var start1 = new Date(range1.start), end1 = new Date(range1.end),
		start2 = new Date(range2.start), end2 = new Date(range2.end);

	if((start1 <= end1 && start2 <= end2 && end1 < start2) ||
	   (start2 <= end2 && start1 <= end1 && end2 < start1)) {
		return false;
	}
	return true;
}

Calendar.prototype.renderHeader = function(){
	/*<span class="day-name"><small>{dayName}</small></span>*/
	var container = '<div class="line-container line-container-days-header"><div class="line-calendar-first-column" style="width:150px"><small style="display:block;text-transform:uppercase;font-weight:bold;color:#62C1D3;width:150px;">' + this.label + '</small></div>{days}</div>',
	 	firstDayElement = '<li class="line-calendar-1thcolumn"></li>';
	var dayElement = '<li style="width:' + ((1/this.cells)*100) + '%"><div class="day-date">{dayDate}</div></li>', daysString = '<div id="' + this.id + '-days" class="line-calendar-second-column" style="width:' + window.lineContentWidth + 'px;"><ul class="days">', day = new Date(this.startFrom);

	//daysString += firstDayElement;
	for(var i = 0; i < this.cells; i++, day.setDate(day.getDate()+1)){
		dayName = CConfigurator.getDayNames()[day.getDay()];
		currentElement = dayElement.replace("{dayName}", dayName).replace("{dayDate}", (day.getDate() < 10 ? "0"+day.getDate() : day.getDate()));
		daysString += currentElement;
	}
	daysString += "</ul></div>";

	container = container.replace('{days}', daysString);
	return container;
}

Calendar.prototype.getDividerLine = function(extra){
	var container = '<div class="line-container line-divider ' + extra + '" style="height: 5px;margin-top: -12px;"><div class="line-calendar-first-column" style="height: 5px;top: 0;width:150px;">&nbsp;</div>{divider}</div>';
	lis = "";
	for(var i = 0; i < this.cells; i++){
		lis += '<li style="width:' + ((1/this.cells)*100) + '%">&nbsp;</li>';
	}
	divider = '<ul class="line line-calendar-second-column divider" style="width:'+window.lineContentWidth+'px">' + lis + '</ul>';
	container = container.replace('{divider}', divider);
	return container;
}

Calendar.prototype.render = function(id){
	var self = this;
  var html
	var overbook;
  window.daysWidth = 150;
  window.lineContentWidth = this.calendarWidth - 150;
  
  $("#"+this.id).css("width", this.calendarWidth);
  
  html = this.renderHeader();
	html += this.getDividerLine();
	
	for(var i = 0; i < this.lines.length; i++){
		lineHtml = '{lineHtml}';
		lineHtml = lineHtml.replace("{lineHtml}", this.lines[i].getHtml());
		html += lineHtml;
		overbook = (this.lines[i].extra.indexOf("overbook") > -1 || (this.lines[i+1] && this.lines[i+1].extra.indexOf("overbook") > -1)
					? "overbook"
					: "");
		html += this.getDividerLine(overbook);
	}

	$("#"+id).html(html);
	$("#"+id).append('<div class="lines-calendar-descriptions-'+this.count+'"></div>');
  
  //window.a = $("#" + id);
  this.initialCalendarTop = $("#" + id).offset().top;
  //console.log(this.initialCalendarTop);
  
	window.setTimeout(function(){
		
		$("#"+id + " .single-day:not(.invisible-date)").each(function(index, obj){
			$(obj).mouseover(function() {
				if(!$(this).hasClass("hover-single-day")){
					$(this).addClass("hover-single-day");
				}
			});

			$(obj).mouseout(function() {
				$(this).removeClass("hover-single-day");
			});

			$(obj).click(function(){
        var params = [];
        var reservationDetail;
        
        reservationDetail = $(this).data("reservationdetail");
        
        lineId = obj.id.substr(0, $(this).attr("id").indexOf("-"));
        line = self.getLineById(lineId);
        
        params = [reservationDetail, line, self];
        
        self.onDateClick.apply(self, params);
			});
		});

		self.showAllTexts(id);
	}, 300);	
}

Calendar.prototype.showAllTexts = function(idp){
	var id = (idp ? idp : self.id);
	var singleDayElement = $('#'+id+" .single-day:not(.invisible-date)")[0];
	var baseWidth = singleDayElement.getBoundingClientRect().right - singleDayElement.getBoundingClientRect().left;
	var self = this;
  
	if(this.aroundSelectedDate){
		var startFromDate = new Date(this.startFrom);
		var startMonth = startFromDate.getMonth();
		var startYear = startFromDate.getFullYear();
		var endDate, endMonth;
		var monthNames = CConfigurator.getMonthNames("long");
		var monthNamesS = CConfigurator.getMonthNames("min");
		var str = "";

		startFromDate.setDate(startFromDate.getDate() + (this.cells-1))
		endDate = new Date(startFromDate);
		endMonth = startFromDate.getMonth();
		startFromDate = new Date(this.startFrom);

    if(startMonth < endMonth)
		  totalMonths = Math.abs(endMonth - startMonth) + 1;
		else if(startMonth > endMonth)
      totalMonths = Math.abs(11 - startMonth + 1) + Math.abs(endMonth+1);
    else
      totalMonths = 1;
      //totalMonths = 0;
    
    div = '<div class="months">{months}</div>';
		monthDivs = '';
    
    iterations = (startMonth + totalMonths) - startFromDate.getMonth();
    
		for(var d = new Date(startFromDate), currentIteration = 0; 
        currentIteration < iterations; 
        d = new Date(d.getFullYear(), d.getMonth() + 1, 1), currentIteration++){
			if(d.getMonth() == startMonth){
				dayNums = (new Date(d.getFullYear(), d.getMonth()+1, 0).getDate()) - startFromDate.getDate() + 1;
			}else if(d.getMonth() == endMonth){
				dayNums = endDate.getDate();
			}else{
				dayNums = (new Date(d.getFullYear(), d.getMonth()+1, 0)).getDate();
			}
			
			witdh = dayNums * baseWidth;
			monthDivs += 
                '<div class="month" style="display:inline-block;width:'+
                 witdh
                +'px;box-sizing:border-box;border-right:1px solid lightgray;text-align:center;font-weight:bold;height:23px;">' 
						 	+ '<span style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;display:block;">' 
						 		+ ( dayNums >= 5 ? monthNames[d.getMonth()] : monthNamesS[d.getMonth()]) + ' ' + d.getFullYear()
						 	+'</span>' 
						 + '</div>';
		}
		div = div.replace('{months}', monthDivs);

		$('#' + this.id + '-days').prepend(div);
	}
	window.setTimeout(function(){
    self.showAllDescriptors(idp, baseWidth);
    self.afterRender();
  }, 300);
}

Calendar.prototype.showAllDescriptors = function(id, baseWidth){
	var self = this;
	var calendarOffset = $('#'+id).offset();
  
	//$("#"+id + " .description-start").each(function(index, obj){
	$("#"+id + " .start-description").each(function(index, obj){
    var description = $(obj).data("description").toLowerCase();
    var offset = $(obj).offset(), element, top, left;
		//var numberOfCells = parseInt($(obj).data("length"));
		var numberOfCells = parseInt($(obj).data("description-length"));
		var noSource = $(obj).hasClass("no-source"), noSourceCell = 0,
        noSourceLeft = 0;
    
    if(noSource){
      noSourceCell = 0.5;
      noSourceLeft = 10;
    }
    
    width = (baseWidth * (numberOfCells + noSourceCell)) - (baseWidth - 0.797);
    description = decodeURI(description);
    description = description.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    top = (offset.top - calendarOffset.top);
    left = (offset.left - calendarOffset.left + 25) - noSourceLeft;
		
    if(top > 0 && left > 0){
      element = jQuery('<div/>', {
        id: 'description-' + obj.id,
        style: 'position:absolute;width:'+width+'px;top:' + top + 'px;left:' + left + 'px;cursor:pointer;min-width:8px;',
        class: 'calendar-description',
        //data:{: "prova"},
        html: "<span style='text-transform:capitalize;'>" + description + "</span>",
      })

      //console.log($(obj));
      element.data("reservationdetail", $(obj).data("reservationdetail"));
      element.attr("tooltips", "");
      element.attr("title", description/* + " : "
                             + new Number($(obj).data("price")).toFixed(2) + "€"*/);
      //element.attr("tooltip-content", "Ciao");
      element.attr("tooltip-side", "top");

      element.appendTo("#"+id + " .lines-calendar-descriptions-"+self.count);

      element.on("click", function(){
        var reservationDetail, line = {}, calendar = self, params = [];
        reservationDetail = $(this).data("reservationdetail");

        lineId = obj.id.substr(0, obj.id.indexOf("-"));
        line = self.getLineById(lineId);

        params = [reservationDetail, line, calendar];
        self.onDateClick.apply(self,params);
      });
    }
	});
}

Calendar.prototype.afterRender = function(){
  this.initialDaysLeft = $("#"+this.id+"-days").offset().left;
  this.initialDaysWidth = parseFloat($("#" + this.id + "-days").width());
  console.log(this.initialDaysLeft);
  if(this.fixedHeader)
    this.manageScrollBehaviour();
  this.onAfterRender(); //custom event handler specified by the user
}

Calendar.prototype.manageScrollBehaviour = function(){
  var self = this;
  $("#main-scrollable-view").scroll(function(){
    var calendarId = this.id;//"calendar-around-current-date";
    var scrollable = "main-scrollable-view";
    var scrollableTop = $("#" + scrollable).scrollTop();
    var calendarTop = self.initialCalendarTop;
    
    if(scrollableTop > calendarTop){
      self.fixedMonthsHeader();
    }else{
      self.normalMonthsHeader();
    }
    
  });
}

Calendar.prototype.fixedMonthsHeader = function(){
  var calendarId = this.id;
	var offset = $("#" + calendarId + "-days").offset();
	var width = ($("#" + calendarId + "-days").width())+1;
	var top = offset.top;
	/*var left = $("#calendar-around-current-date-days").css("left", 
    parseFloat(parseFloat(this.initialDaysLeft) - $("#main-scrollable-view").scrollLeft()) + "px"
  );*/
	var left = offset.left;
	var element = jQuery('<div/>', {
	    style: 'width:'+(width-1)+'px;height:44px;',
	    class: 'line-calendar-second-column fake-months-header'
	});

	var jqueryMonthsHeader = $($("#" + calendarId + "-days")[0]);
	
  if($("#" + calendarId + " .fake-months-header").length > 0) return;

  jqueryMonthsHeader.css("position", "fixed");
	jqueryMonthsHeader.css("top", "0px");
	jqueryMonthsHeader.css("left", left+"px");
	jqueryMonthsHeader.css("height", 44+"px");
	jqueryMonthsHeader.css("width", width+"px");
	jqueryMonthsHeader.css("background", "white");
	jqueryMonthsHeader.css("z-index", "1999");

	element.appendTo("#" + calendarId + " .line-container-days-header");
  
  this.displayedFixedHeader = true;
}

Calendar.prototype.normalMonthsHeader = function(){
  var calendarId = this.id;//"calendar-around-current-date";
  var jqueryMonthsHeader = $($("#" + calendarId + "-days")[0]);

  var width = jqueryMonthsHeader.width();
  
  if($("#" + calendarId + " .fake-months-header").length == 0) return;
  
  $("#" + calendarId + " .fake-months-header").remove();
  jqueryMonthsHeader.css("position", "relative");
  jqueryMonthsHeader.css("width", this.initialDaysWidth+"px");
  jqueryMonthsHeader.css("top", "0px");
  jqueryMonthsHeader.css("left", "0px");
  
  this.displayedFixedHeader = false;
}

/*******************************
 * Calendar Singleton configuration Class
 *
 * this singleton represent a configuration accessible from all components of all calendar instances
 *******************************/

cConfiguration = { locate: "en" }

CConfigurator = function(){
	cfg = cConfiguration;
	locate = cfg.locate || i18n["loc"] || "en";	
	i18nCCongif = i18n[locate];
	return {
		locate: locate,
		i18nCCongif: i18nCCongif,

		getDayNames: function(show){
			if(show == "min"){ names = i18nCCongif["dayNamesMin"]; }
			else if(show == "long"){ names = i18nCCongif["dayNames"]; }
			else names = i18nCCongif["dayNamesShort"];

			return names;
		},

		getMonthNames : function(show){
			if(show == "min"){ names = i18nCCongif["monthNamesShort"]; }
			else if(show == "long"){ names = i18nCCongif["monthNames"]; }
			else names = i18nCCongif["monthNamesShort"];

			return names;
		}
	}
}();