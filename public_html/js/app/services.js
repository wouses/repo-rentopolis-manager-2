app
    .service('GMapService', ['$http', function($http){
      var self = this;
      
      self.map = { center: { latitude: 0.0, longitude: 0.0 }, zoom: 14, control: {}};
      
      self.setCenter = function(coord, zoom){
        self.map.center.latitude = coord.lat;
        self.map.center.longitude = coord.lon;
        if(zoom !== undefined && zoom > 1 && zoom < 16)
          self.map.zoom = zoom;
      }
      
      self.resizeMap = function(size){
        resizeHeight(size);
      }
      
      function resizeHeight(size){
        $(".auto-height").height(size);
        $('.angular-google-map-container').height(size);
      }
      
    }])
    
    .service('AsyncRequestsService', ['$http', function($http){
      var self = this,
          csfr = {
            name: $('meta[name="csrf-token-name"]').attr("content"),
            value: $('meta[name="csrf-token-value"]').attr("content")
          };

      self.get = function(url, data, decode){
        var urlQuery = $.param( data );
        if(decode)
          urlQuery = decodeURIComponent( urlQuery );
        return $http.get(url+'?'+urlQuery, { ignoreLoadingBar: true });
      }

      self.post = function(url, data, decode){
        data[csfr.name] = csfr.value;
        return $http.post(url, data, { ignoreLoadingBar: true });  
      }
    }])
    
    .service('AnnouncementsService', ['$http', 'AsyncRequestsService', function($http, AsyncRequestsService){
      var self = this,
          AR = AsyncRequestsService;
      
      self.getProperties = function(json){
        return AR.get('/announcement/businessAnnouncements', json);
      }
      
      self.getChannelManagersConfiguration = function(id, json){
        return AR.get('/announcement/' + id + '/channelManagersConfiguration', json);
      }
      
      self.getSettingFields = function(json){
        return AR.get('/announcement/getSettingsLayoutForm', json);
      }
      
      self.getAnnouncementSettings = function(id, json){
        return AR.get('/announcement/' + id + '/getAnnouncementSettings', json);
      }
      
      self.getAnnouncementChannelRules = function(id, json){
        return AR.get('/announcement/' + id + '/getAnnouncementChannelRules', json);
      }
      
      self.setSettingFields = function(json){
        return AR.post('/announcement/updateSettingsLayoutForm', json);
      }
      
      self.updateChannelManagersConfiguration = function(id, json){
        return AR.post('/announcement/' + id + '/setChannelManagersConfiguration', json);
      }
      
      self.updateAddress = function(id, json){
        return AR.post('/announcement/' + id + '/updateAddress', json);
      }
      
      self.updateAnnouncementSettings = function(id, json){
        return AR.post('/announcement/' + id + '/updateAnnouncementSettings', json);
      }
      
      self.deleteLastAnnouncementChannelSettings = function(id, json){
        return AR.post('/announcement/' + id + '/deleteLastAnnouncementChannelRule', json);
      }
      
      self.saveAnnouncementChannelSettings = function(id, json){
        return AR.post('/announcement/' + id + '/saveLastAnnouncementChannelRule', json);
      }
    }])

    .service('UsersService', ['$http', 'AsyncRequestsService', function($http, AsyncRequestsService){
      var self = this,
          AR = AsyncRequestsService;

      self.signIn = function(json){
        return AR.post('/user/signIn', json);
      }
    }])

    .service('ReservationsService', ['$http', 'AsyncRequestsService', function($http, AsyncRequestsService){
      var self = this,
          AR = AsyncRequestsService;

      self.getAll = function(json){
        return AR.post('/getAll/signIn', json);
      }
    }])

    .service('HomeownerService', ['$http', 'AsyncRequestsService', function($http, AsyncRequestsService){
      var self = this,
          AR = AsyncRequestsService;

      self.getAllAnnouncements = function(json){
        return AR.get('/announcement/homeowner', json);
      }
      
      self.getAnnouncementReservations = function(json){
        return AR.get('/reservations/homeowner', json);
      }
    }])

    .service('PropertyManagerService', ['$http', 'AsyncRequestsService', function($http, AsyncRequestsService){
      var self = this,
          AR = AsyncRequestsService;

      self.getAllAnnouncementReservations = function(json){
        return AR.get('/reservations/propertymanager', json);
      }
      
      /*update or create new reservation*/
      self.save = function(json){
        return AR.post('/reservations/saveReservation', json);
      }
      
    }])

    .service('ChannelsService', ['$http', 'AsyncRequestsService', function($http, AsyncRequestsService){
      var self = this,
          AR = AsyncRequestsService;

      self.getAll = function(json){
        return AR.get('/channels/all', json);
      }
      
      self.getRules = function(id, json){
        return AR.get('/channels/' + id + '/rules', json);
      }
      
      self.saveRule = function(id, json){
        return AR.post('/channels/' + id + '/saveChannelRule', json);
      }
      
      self.deleteLastRule = function(id, json){
        return AR.post('/channels/' + id + '/deleteLastRule', json);
      }

    }])

    .service('BusinessChartsService', ['$http', 'AsyncRequestsService', function($http, AsyncRequestsService){
      var self = this,
          AR = AsyncRequestsService;

      self.getStockChart = function(json){
        return AR.get('/reservations/businessStockChart', json);
      }
      
      self.getHomeowner = function(json){
        return AR.get('/chart/homeownerchart', json);
      }

    }])
;