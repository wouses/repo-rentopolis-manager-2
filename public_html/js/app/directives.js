app.directive('menuToggle', [ '$timeout', function($timeout){
      return {
        scope: {
          section: '='
        },
        templateUrl: 'ng-views/menu-toggle.html',
        link: function($scope, $element) {
          var controller = $element.parent().controller();
          $scope.isOpen = function() {
            return controller.isOpen($scope.section);
          };
          $scope.toggle = function() {
            controller.toggleOpen($scope.section);
          };
        }
      };
    }]);

    app.directive('menuLink', ['$location', function ($location) {
      return {
        scope: {
          section: '='
        },
        templateUrl: 'ng-views/menu-link.html',
        link: function ($scope, $element) {
          var controller = $element.parent().controller();

          $scope.focusSection = function (path) {
            // set flag to be used later when
            // $locationChangeSuccess calls openPage()
            /*controller.autoFocusContent = true;*/
            $location.path( "/" + path );
          };
        }
      };
    }]);