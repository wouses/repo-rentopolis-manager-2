app = angular.module('managerApp', ['ngMaterial', 'ngSanitize', 'ngRoute', 'ngMessages', 'smartTime', 
                                    'angularGrid', 'uiGmapgoogle-maps']);

app.config(['$routeProvider', 'uiGmapGoogleMapApiProvider', function($routeProvider, uiGmapGoogleMapApiProvider){
  //app.config(function($routeProvider) {

  $routeProvider
  // route for the home page
    .when('/', {
    templateUrl : 'ng-views/dashboard.html?v='+jsViewsVersion,
    controller  : 'DashboardController as ctrl'
  })

  // route for the admin page
  .when('/admin', {
    templateUrl : 'ng-views/admin.html?v='+jsViewsVersion,
    controller  : 'PropertyManagerController as ctrl'
  })

  // route for the about page
    .when('/announcements', {
    templateUrl : 'ng-views/announcements.html?v='+jsViewsVersion,
    controller  : 'AnnouncementController as ctrl'
  })

  // route for the contact page
    .when('/reservations', {
    templateUrl : 'ng-views/reservations.html?v='+jsViewsVersion,
    controller  : 'ReservationController as ctrl'
  })

  // route for the contact page
    .when('/calendars', {
    templateUrl : 'ng-views/calendars.html?v='+jsViewsVersion,
    controller  : 'CalendarsController as ctrl'
  })

  // route for the contact page
    .when('/channels', {
    templateUrl : 'ng-views/channels.html?v='+jsViewsVersion,
    controller  : 'ChannelController as ctrl'
  })

  // route for the contact page
    .when('/channel-managers', {
    templateUrl : 'ng-views/channel-managers.html?v='+jsViewsVersion,
    controller  : 'ChannelManagerController as ctrl'
  })

    .when('/business-partners', {
    templateUrl : 'ng-views/business-partners.html?v='+jsViewsVersion,
    controller  : 'BusinessPartnerController as ctrl'
  })
  
  .when('/business-charts', {
    templateUrl : 'ng-views/business-charts.html?v='+jsViewsVersion,
    controller  : 'BusinessChartsController as ctrl'
  })
  
  .when('/homeowner', {
    templateUrl : 'ng-views/homeowner.html?v='+jsViewsVersion,
    controller  : 'HomeOwnerController as ctrl'
  })
  
  .when('/homeowner-charts', {
    templateUrl : 'ng-views/homeowner-charts.html?v='+jsViewsVersion,
    controller  : 'HomeOwnerChartsController as ctrl'
  })
  
  .when('/homeowner-stats', {
    templateUrl : 'ng-views/homeowner-stats.html?v='+jsViewsVersion,
    controller  : 'HomeOwnerStatsController as ctrl'
  });
  
  

  uiGmapGoogleMapApiProvider.configure({
    key: window.googleApiKey,
    v: '3.17',
    libraries: 'weather,geometry,visualization'
  });
}]);

app.config(['$httpProvider', function($httpProvider){

      // Use x-www-form-urlencoded Content-Type
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

      /**
      * The workhorse; converts an object to x-www-form-urlencoded serialization.
      * @param {Object} obj
      * @return {String}
      */ 
      var param = function(obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj) {
          value = obj[name];

          if(value instanceof Array) {
            for(i=0; i<value.length; ++i) {
              subValue = value[i];
              fullSubName = name + '[' + i + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value instanceof Object) {
            for(subName in value) {
              subValue = value[subName];
              fullSubName = name + '[' + subName + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value !== undefined && value !== null)
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
      };

      // Override $http service's default transformRequest
      $httpProvider.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
      }];

    }]);