app.controller('UserController', ['UsersService', '$scope', function(UsersService, $scope) {
  var self = this;
  
  $scope.message = 'Reservation...work in progress';
  
  this.user = {username: '', password: ''};
  this.ajaxCalling = false;
  
  this.signIn = function(){
      var p = UsersService.signIn({username: this.user.username, password: this.user.password});
      self.ajaxCalling = true;  
      p.success(function(data){
        //console.log(data);
        self.ajaxCalling = false;
        if(data.success){
          location.href = location.protocol + '//' + location.host + "#/" + data.role;
        }else{
          //(data.error);
        }
      });
  }
  
}]);