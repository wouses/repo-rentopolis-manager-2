//app.controller('ChannelController', function($scope) {

app.controller('ChannelController', 
                   ['$scope', '$mdDialog', '$templateCache', 'ChannelsService',
function($scope, $mdDialog, $templateCache, ChannelsService){

  var channels = ChannelsService.getAll({});
  var self = this;

  self.channelsInReservations = [];
  self.channelsWithoutReservations = [];

  channels.success(function(data){
    var channel;
    self.channels = data.result;
    for(c in self.channels){
      channel = self.channels[c];
      //channel.rules = [{code:"213", validFrom: "2016-10-03", channelCommissionIncluded: false, channelCommissionIncluded: true}]
      
      if(parseInt(channel["total"]) > 0){
        self.channelsInReservations.push(channel);
      }else{
        self.channelsWithoutReservations.push(channel);
      }
    }
  });

  self.rules = function(channel){
    var cs = ChannelsService.getRules(channel.id, {});
    cs.success(function(data){
      if(data.success){
        rules = data["result"];
        self.showRulesModal(channel, rules);
      }
    });
  }
  
  self.showRulesModal = function(channel, rules){
    self.channel = channel;
    self.channelRules = rules;
    
    $mdDialog.show({
      clickOutsideToClose: false,
      scope: $scope,        // use parent scope in template
      preserveScope: true,  // do not forget this if use parent scope
      template: $templateCache.get("ChannelsModal.html"),
      controller: function ChannelsController($scope, $mdDialog) {
        $scope.channel = self.channel;
        $scope.channelRules = self.channelRules;
        
        $scope.channel.isNewRule = false;
        $scope.faseAdd = true;
        
        $scope.addRule = function(){
          $scope.channel.isNewRule = true;
          $scope.faseAdd = false;
          $scope.channel.newRule = {cleaningIncluded: false, channelCommissionIncluded: true, defaultCommission:'', validFrom: ''};
        }
        
        $scope.saveNewRule = function(){
          var saveRule = ChannelsService.saveRule($scope.channel.id, { channel: JSON.stringify($scope.channel) });
          saveRule.success(function(data){
            if(data.success) location.reload();
          });
        }
        
        $scope.deleteLastRule = function(){
          var deleteLastRule = ChannelsService.deleteLastRule($scope.channel.id, {});
          deleteLastRule.success(function(data){
            if(data.success){
              $mdDialog.hide();
              location.reload();
            }
          });
        }

        $scope.closeDialog = function(){
          //$scope.channel.isNewRule = false;
          $mdDialog.hide();
        }
      }
    });
  }

}]);