
app.controller('HomeOwnerStatsController',  ['$scope', 'HomeownerService', function($scope, HomeownerService) {

  var self = this;
  self.selectedYear = 2016;

  self.years = [{
    y: '2015',
  },{
    y: '2016',
  }];

  self.today = new Date();
  self.selectedDate = self.today;

  self.setSelectedDate = function(date){
    self.selectedDate = date;
  }

  self.dateToString = function(date){
    var dt = new Date(date);
    var m = dt.getMonth()+1;
    var d = dt.getDate();
    var string = dt.getFullYear() + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
    
    return string;
  }
  
  self.remoteData = function(){
    var data = {
      "from": self.dateToString(self.selectedDate),
      "months": self.selectedDate.getMonth()+1,
      "includeEnquiry": 0,
      "includePortal": 0,
      "listing": listing.id_announcement
    }, p;
    p = HomeownerService.getAnnouncementReservations(data);
    p.success(function(data){
      //here data for table view.
    });
  }
  
}]);