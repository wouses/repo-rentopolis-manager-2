
app.controller('HomeOwnerController', 
                   ['$scope', '$mdDialog', '$templateCache', 'PropertyManagerService', 'HomeownerService', 'BusinessChartsService',
function($scope, $mdDialog, $templateCache, PropertyManagerService, HomeownerService, BusinessChartsService) {
  var p, self = this;
  window.calendars = [];
  
  self.announcementCounter = 0;
  self.currentAnnouncementIndex = 0;
  self.orderedAnnouncement = {}
  self.calendars = [];
  
  self.yearsSelection = [{id: 2015, value: '2015'}, {id: 2016, value: '2016'}];
  self.monthsSelection = [
    {id: 1, value: 'Gennaio'}, {id: 2, value: 'Febbraio'}, {id: 3, value: 'Marzo'}, {id: 4, value: 'Aprile'}, 
    {id: 5, value: 'Maggio'}, {id: 6, value: 'Giugno'}, {id: 7, value: 'Luglio'}, {id: 8, value: 'Agosto'}, 
    {id: 9, value: 'Settembre'}, {id: 10, value: 'Ottobre'}, {id: 11, value: 'Novembre'}, {id: 12, value: 'Dicembre'}];
  self.totalMonthsSelection = [
    {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}, {id: 7}, {id: 8}, {id: 9}, {id: 10}, 
    {id: 11}, {id: 12}, {id: 13}, {id: 14}, {id: 15}, {id: 16}, {id: 17}, {id: 18}, {id: 19}, {id: 20}, 
    {id: 21}, {id: 22}, {id: 23}, {id: 24}
  ]
  self.apartments = [{id: -1, value : 'Tutti'}];

  self.selectedYear = 2016;
  self.selectedMonth = 1;
  self.selectedAnnouncement = null;
  self.totalMonths = 12;

  self.selectedApartment = null;//initialized by asynch request
  
  self.getSelectedDate = function(){
    return new Date(self.selectedYear, self.selectedMonth - 1, 1);
  }

  self.onChangeSelection = function(){
    $(".calendar-container").html('');
    self.showProgress = true;
    if(self.selectedApartment >= 0)
      window.setTimeout(function(){self.renderCalendar2(self.selectedApartment)}, 800);
    else
      window.setTimeout(function(){self.renderCalendars2()}, 800);
  }
  
  self.renderCalendar2 = function(code, redraw){
    if(typeof redraw == "undefined") redraw = true;
    
    apartment = self.searchInApartments(code);
    
    data = {
      "from": self.dateToISONoTz(self.getSelectedDate()),//"2016-01-01",//start,
      "months": self.totalMonths,
      "includeEnquiry": 0,
      "includePortal": 0,
      "listing": apartment.id
    };
    self.requestCalendarData2(data, redraw);
  }
  
  self.renderCalendars2 = function(i){
    if(typeof i == "undefined") i = 1;
    if(i < self.apartments.length){
      self.renderCalendar2(self.apartments[i].id);
      window.setTimeout(function(){self.renderCalendars2(i+1, false)}, 600);
    }else return;
  }

  z = {success:function(){}}
  z = HomeownerService.getAllAnnouncements({});
  z.success(function(data){
    listings = data.result;
    for(var i = 0; i < listings.length; i++){
      listing = listings[i];
      self.apartments.push({id: parseInt(listing.id_announcement), value: listing.title});
    }
    self.selectedApartment = -1;
    self.onChangeSelection();
  });
  
  self.searchInApartments = function(code){
    for(apt in self.apartments){
      if(self.apartments[apt].id == code) return self.apartments[apt];
    }
    return null;
  }
  
  self.requestCalendarData2 = function(data, redraw){
    if(typeof redraw == "undefined") 
      self.redraw = true;
    else
      self.redraw = false;

    var p = HomeownerService.getAnnouncementReservations(data);
    p.success(self.drawCalendar);
  }

  self.drawCalendar = function(data){
    var monthReservations = data.result, months = [];
    var dt = new Date(self.getSelectedDate()), month = dt.getMonth();
    var listingCode = data.listingCode, listingTitle = data.listingTitle;
    var lines = [], calendar;
    
    if(self.showProgress){
      self.showProgress = false;
    }
    
    for(var i = 0; i < self.totalMonths; i++, month++, dt = new Date(dt.getFullYear(), dt.getMonth()+1, 1)){
      if(month == 12) month = 0;
      lines.push({
        label: i18n[locate]["monthNames"][month] + ' ' + dt.getFullYear(),
        startFrom: self.dateToISONoTz(dt),
        ranges: monthReservations[i],
        cells: (new Date(dt.getFullYear(), dt.getMonth()+1, 0)).getDate(),
        customFields: {
          propertyId: listingCode
        }
      });
    }

    calendar = {
      startFrom: '2015-10-01',
      showWeekends: true,
      showToday: true,
      showCurrentDate: true,
      label: listingTitle.substr(0, 15) + "...",
      cells: 31,
      noOverbookings: true,
      lines: lines,
      onDateClick: self.onDateClick,

      width: 900
    };
    
    fn = "";
    if(self.redraw) fn = "html"; else fn = "append";
    
    $(".calendar-container")[fn](
      '<div id="' + listingCode + '-calendar" class="line-calendar" style="display:block;margin:0 auto; margin-top:30px;"></div>'
    );
    
    window.setTimeout(function(){
      new Calendar(listingCode + "-calendar", calendar);
    }, 300);
  }
  
  p = {success: function(){}};
  //p = HomeownerService.getAllAnnouncements({});

  p.success(function(returnedData){
    var dt, m, d, start, data, counter;
    /*create start date*/
    window.asynchs = [];
    /*dt = new Date();
    dt = new Date(dt.getFullYear(), dt.getMonth()-1, 1);
    m = dt.getMonth()+1;
    d = dt.getDate();
    start = dt.getFullYear() + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);*/

    totalMonths = 12;

    counter = 0;
    for(l in returnedData.result){
      listing = returnedData.result[l];
      
      data = {
        "from": "2016-01-01",//start,
        "months": totalMonths,
        "includeEnquiry": 0,
        "includePortal": 0,
        "listing": listing.id_announcement
      };
      
      self.orderedAnnouncement[listing.title] = counter;
      self.requestCalendarData(data);
      counter++;
    }
    self.announcementCounter = counter;
  });
  
  self.dateToISONoTz = function(date){
    var dt = new Date(date);
    var m = dt.getMonth()+1;
    var d = dt.getDate();
    var string = dt.getFullYear() + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
    return string;
  }
  
  self.requestCalendarData = function(data){
    window.asynchs.push(HomeownerService.getAnnouncementReservations(data));
    window.asynchs[asynchs.length-1].success(self.createCalendar);
  }
  
  self.createCalendar = function(returnedData2){
    var monthReservations = returnedData2.result, months = [];
    var dt = new Date();
    var listingCode = returnedData2.listingCode;
    var listingTitle = returnedData2.listingTitle;
    
    //dt = new Date(dt.getFullYear(), dt.getMonth()-1, 1);
    dt = self.getSelectedDate();
    
    var month = dt.getMonth();
    var lines = [];
    var calendar;
    
    for(var i = 0; i < totalMonths; i++, month++, dt = new Date(dt.getFullYear(), dt.getMonth()+1, 1)){
      if(month == 12) month = 0;
      lines.push({
        label: i18n[locate]["monthNames"][month] + ' ' + dt.getFullYear(),
        startFrom: self.dateToISONoTz(dt),
        ranges: monthReservations[i],
        cells: (new Date(dt.getFullYear(), dt.getMonth()+1, 0)).getDate(),
        customFields: {
          propertyId: listingCode
        }
      });
    }

    calendar = {
      startFrom: '2015-10-01',
      showWeekends: true,
      showToday: true,
      showCurrentDate: true,
      label: listingTitle.substr(0, 15) + "...",
      cells: 31,
      noOverbookings: true,
      lines: lines,
      onDateClick: self.onDateClick,
      
      width: 900
    };
    //console.log(calendar);

    self.currentAnnouncementIndex++;
    self.calendars[self.orderedAnnouncement[listingTitle]] = {listingCode: listingCode, calendar: calendar};
    if(self.currentAnnouncementIndex == self.announcementCounter){
      self.renderAllCalendar();
    }
    
    //console.log($(".calendar-container").html());
    
    /*window.setTimeout(function(){
      
    }, 600)*/

  }
  
  self.renderAllCalendar = function(){
    var lastIndex = self.calendars.length-1;
    
    for(var c = 0; c < self.calendars.length; c++){
      var cal = self.calendars[c];
      $(".calendar-container").append(
        '<div id="' + cal.listingCode + '-calendar" class="line-calendar" style="display:block;margin-top:50px;"></div>'
      );
      self.renderCalendar(cal);
    }
  }
  
  self.renderCalendar = function(cal){
    window.setTimeout(function(){
      window.calendars.push(new Calendar(cal.listingCode + "-calendar", cal.calendar));
    }, 600);
  }

  //HomeownerReservationModal
  
  self.showReservation = function(reservationDetail){
    self.showReservationDetail = reservationDetail;
    
    $mdDialog.show({
      clickOutsideToClose: false,
      escapeToClose: false,
      scope: $scope,        // use parent scope in template
      preserveScope: true,  // do not forget this if use parent scope

      template: $templateCache.get("HomeownerReservationModal.html"),
      controller: function ShowReservationDialogController($scope, $mdDialog) {
        
        $scope.showReservationDetail = angular.copy(self.showReservationDetail);
        $scope.showReservationDetail.checkin = new Date(self.showReservationDetail.checkin);
        $scope.showReservationDetail.checkout = new Date(self.showReservationDetail.checkout);
        
        $scope.showReservationDetail.description = decodeURI($scope.showReservationDetail.description);
        $scope.showReservationDetail.propertyTitle = decodeURI($scope.showReservationDetail.propertyTitle);
        
        $scope.closeDialog = function(){
          $mdDialog.hide();
        }
        
        /* 
          Marco Distrutti bugfix.
          Some Chrome versions have a bug in angular material design md-datepicker (inside a popup)
          It doesn't add the md-input-has-value if the initial date is valid.
          Sometimes the envirioment doesn't add this class when from empty value is selected a valid date from the popup
        */
        
        $scope.fixDatePickerContent = function(){
          var checkin = $scope.showReservationDetail.checkin;
          var checkout = $scope.showReservationDetail.checkout;
          if(!(Object.prototype.toString.call(checkin) === "[object Date]" && isNaN( checkin.getTime() )) && 
             !$(".checkin-container").hasClass("md-input-has-value"))
            $(".checkin-container").addClass("md-input-has-value");
          if(!(Object.prototype.toString.call(checkout) === "[object Date]" && isNaN( checkout.getTime() )) && 
             !$(".checkout-container").hasClass("md-input-has-value"))
            $(".checkout-container").addClass("md-input-has-value");
          
          $(".checkin-container input, .checkout-container input").attr("readonly", "readonly");
          
        }
        
        window.setTimeout($scope.fixDatePickerContent, 600);
      }
    });
    
  }
  
  self.onDateClick = function(reservationDetail, line, calendar){

    if(!reservationDetail.isBlockedDates && 
        typeof reservationDetail.checkout !== "undefined"
      ) {
      self.showReservation(reservationDetail);
      return;
    }
    
    if(reservationDetail.extra == "BLOCKED_BY_AGENCY")
      return;
    
    self.reservationDetail = reservationDetail;
    self.currentLine = line;
    
    $mdDialog.show({
      clickOutsideToClose: false,
      escapeToClose: false,
      scope: $scope,        // use parent scope in template
      preserveScope: true,  // do not forget this if use parent scope

      template: $templateCache.get("HomeownerModal.html"),
      controller: function AdminDialogController($scope, $mdDialog) {
        $scope.reservationDetail = angular.copy(self.reservationDetail);
        
        $scope.line = self.currentLine;
        
        $scope.reservationDetail.description = decodeURI($scope.reservationDetail.description);
        $scope.reservationDetail.propertyTitle = decodeURI($scope.reservationDetail.propertyTitle);
        $scope.reservationDetail.checkin = self.reservationDetail.checkin ? new Date(self.reservationDetail.checkin) : "";
        $scope.reservationDetail.checkout = self.reservationDetail.checkout ? new Date(self.reservationDetail.checkout) : "";

        $scope.reservationDetail.id_reservationStatus = 6;

        $scope.reservationDetail.guest = {name: "Owner block", surname:""}
        $scope.reservationDetail.price = 0;
        $scope.reservationDetail.resGuestInfos = {adultsNumber:0, childsNumber:0, babiesNumber:0}
        $scope.waitingForSave = false;
        $scope.hasNotCode = typeof $scope.reservationDetail.code === 'undefined';
        
        checkin = $scope.reservationDetail.checkin;
        $scope.minDate = new Date(checkin.getFullYear(), checkin.getMonth(), checkin.getDate()+1);
        
        $scope.saveReservation = function(){
          var toSend = angular.copy($scope.reservationDetail);
          
          if(typeof toSend.propertyId === "undefined")
            toSend.propertyId = $scope.line.customFields.propertyId;
          
          if($(".checkin-container input")[0].value == "")
            toSend.checkin = "";
          else
            toSend.checkin = Day.prototype.toISONoTz(toSend.checkin);
          
          if($(".checkout-container input")[0].value == "")
            toSend.checkout = "";
          else
            toSend.checkout = Day.prototype.toISONoTz(toSend.checkout);
          
          $scope.waitingForSave = true;
          p = PropertyManagerService.save(toSend);
          p.success(function(data){
            console.log(data);
            $scope.waitingForSave = false;
            if(data.success){
              console.log("reload");
              $mdDialog.hide();
              location.reload();
            }
          });
        }
        
        $scope.cancelReservation = function(){
          $scope.reservationDetail.id_reservationStatus = 5;
          $scope.saveReservation();
        }
        
        $scope.closeDialog = function(){
          $mdDialog.hide();
        }
        
        /* 
          Marco Distrutti bugfix.
          Some Chrome versions have a bug in angular material design md-datepicker (inside a popup)
          It doesn't add the md-input-has-value if the initial date is valid.
          Sometimes the envirioment doesn't add this class when from empty value is selected a valid date from the popup
        */
        
        $scope.fixDatePickerContent = function(){
          var checkin = $scope.reservationDetail.checkin;
          var checkout = $scope.reservationDetail.checkout;
          if(!(Object.prototype.toString.call(checkin) === "[object Date]" && isNaN( checkin.getTime() )) && 
             !$(".checkin-container").hasClass("md-input-has-value"))
            $(".checkin-container").addClass("md-input-has-value");
          if(!(Object.prototype.toString.call(checkout) === "[object Date]" && isNaN( checkout.getTime() )) && 
             !$(".checkout-container").hasClass("md-input-has-value"))
            $(".checkout-container").addClass("md-input-has-value");
          
          $(".checkin-container input, .checkout-container input").attr("readonly", "readonly");
          
          checkin = $scope.reservationDetail.checkin;
          $scope.minDate = new Date(checkin.getFullYear(), checkin.getMonth(), checkin.getDate()+1);
        }
        
        $scope.checkinChanged = function(){$scope.fixDatePickerContent();}
        $scope.checkoutChanged = function(){$scope.fixDatePickerContent();}
        window.setTimeout($scope.fixDatePickerContent, 600);
      }
    });
  }

  /* Chart Management */

  self.init = function(){
    //3800
    
  }();
  
  function removeHighChartLink(){
    //$("g.highcharts-tooltip").next().remove();
    //window.setTimeout(removeHighChartLink, 6000);
  }
  
  //window.setTimeout(removeHighChartLink, 800);
}]);