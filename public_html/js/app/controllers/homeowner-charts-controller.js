// create the controller and inject Angular's $scope
app.controller('HomeOwnerChartsController',  ['$scope', 'BusinessChartsService', function($scope, BusinessChartsService) {

  var self = this;

  self.init = function(){
    var p = BusinessChartsService.getHomeowner({id: urCode, type: "DRILLDOWN_APARTMENTS"});
    var seriesOptions;
    var colors = ["#7cb5ec", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"];

    p.success(function(data){
      var result = JSON.parse(data.results);

      var categories = [];
      var series = {};
      var serieNames = [];
      var drilldown = [];
      var i = 0, j = 0;
      var occupancySeries = [];

      for(r in result){
        if(i >= 2) break;
        var record = result[r];
        categories.push(r);

        if(i == 0){
          j = 0;
          for(s in record){
            if(serieNames.indexOf(s) == -1){
              serieNames.push(s);
              series[s] = {
                name: s,
                color: colors[j],
                data : []
              };
              occupancySeries[s] = {
                name: s,
                color: colors[j],
                data : []
              };
            }
            j++;
          }
          i++;
        }
      }

      for(r in result){
        var record = result[r];
        for(s in record){
          series[s].data.push({
            name: r,
            y: record[s]["total"],
            drilldown: r + "-" + s
          });

          drilldown.push({
            name: r + "-" + s,
            id: r + "-" + s,
            data: record[s]["monthDetail"]
          });

          var occupancyRate = 0;
          for(md in record[s]["monthDetail"]){
            var arr = record[s]["monthDetail"][md];
            if(arr[1] > 0)
              occupancyRate += 1;
          }
          occupancyRate = parseFloat(parseFloat(occupancyRate / record[s]["monthDetail"].length * 100).toFixed(2));
          occupancySeries[s].data.push(occupancyRate);
        }
      }

      var chartSeries = [];
      for(c in series){
        chartSeries.push(series[c]);
      }

      var chartOccupancySeries = [];
      for(c in occupancySeries){
        chartOccupancySeries.push(occupancySeries[c]);
      }

      console.log(chartSeries);
      console.log(categories);
      console.log(drilldown);

      $('#business-stock').highcharts({
        chart: {
          type: 'column'
        },
        title: {
          text: 'Tasso dei guadagni.'
        },
        subtitle: {
          text: 'Clicca sulla barra per interagire con il grafico'
        },
        xAxis: {
          //categories:['Gennaio', 'Febbraio', 'Marzo'],
          type:'category'
          //crosshair: true
        },
        yAxis: {
          title: {
            text: 'Totale dei guadagni'
          }

        },

        legend: { enabled: true },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{point.y:.1f} €'
            }
          }
        },

        tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> <br/>'
        },

        series: chartSeries,
        drilldown: {
          drillUpButton: {
            relativeTo: 'plotBox',
            position: {
              align: 'center',
              verticalAlign: 'top'
            },
            theme: {
              fill: 'white',
              'stroke-width': 1,
              stroke: 'silver',
              r: 0,
              states: {
                hover: {
                  fill: '#bada55'
                },
                select: {
                  stroke: '#039',
                  fill: '#bada55'
                }
              }
            }
          },
          series: drilldown
        }
      });

      $('#container').highcharts({
        chart: {
          type: 'line'
        },
        title: {
          text: 'Tasso di soggiorno'
        },
        subtitle: {
          text: 'Andamento in percentuale del tasso di soggiorno.'
        },
        xAxis: {
          categories: categories
        },
        yAxis: {
          title: {
            text: 'Tasso'
          }
        },
        tooltip: {
            valueSuffix: ' %'
        },
        plotOptions: {
          line: {
            dataLabels: {
              enabled: true,
              format: '{point.y:.2f} %'
            },
            enableMouseTracking: true
          }
        },
        series: chartOccupancySeries
      });
      
    });
  }();

}]);