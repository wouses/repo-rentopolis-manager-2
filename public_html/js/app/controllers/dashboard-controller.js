    // create the controller and inject Angular's $scope
    app.controller('DashboardController', function($scope) {
      
      $scope.myDate = new Date("2015-11-10");
      
      var columnDefs = [
        {headerName: "Check-in Date", field: "checkIn"},
        {headerName: "Reservation Code", field: "reservationCode"},
        {headerName: "Announcement", field: "announcementCode"}
      ];

      var rowData = [
        {checkIn: "10/12/2015 14 PM", reservationCode: "EWQRTS", announcementCode: "1000"},
        {checkIn: "08/01/2016 12 PM", reservationCode: "AYUHJK", announcementCode: "1001"},
        {checkIn: "20/01/2016 10 AM", reservationCode: "GSHYDV", announcementCode: "1002"}
      ];

      $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: rowData,
        dontUseScrolls: true // because so little data, no need to use scroll bars
      };
      
      // create a message to display in our view
      $scope.message = 'The next check-ins';
      
      $('#pie-chart').highcharts({
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
        },
        title: {
          text: 'Arrivals in this month, 2015'
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: false
            },
            showInLegend: true
          }
        },
        series: [{
          name: "Your arrivals",
          colorByPoint: true,
          data: [{
            name: "Not managed",
            y: 3
          }, {
            name: "Managed with good satisfaction",
            y: 10
          }, {
            name: "Managed with excellent satisfaction",
            y: 24,
            sliced: true,
            selected: true
          }, {
            name: "Managed, the customer was arrived late",
            y: 6
          }]
        }]
      });
      
      
    });