//note that padString dependency is defined in smarttime.js file.
app.controller('PropertyManagerController', 
                   ['$scope', '$sce', '$mdDialog', '$templateCache', 'padString', 'PropertyManagerService',
function($scope, $sce, $mdDialog, $templateCache, padString, PropertyManagerService) {
  var self = this, p, today = Day.prototype.toISONoTz(new Date()), selectedDay;
  var fromRequest, toRequest, offsetDayBefore = 5;

  window.winzoz = self;
  
  self.datepickerDate = null;

  /*self.page = 1;
  self.pageSize = 20;
  self.setPage = function(page){
    self.page = page;
    self.refreshCalendar();
  }*/
  
  self.refreshCalendar = function(){
    if(self.datepickerDate != null)
      self.renderCalendar(true, false);
    else
      self.renderCalendar(false, true);
  }
  
  self.initialCalendar = function(){
    self.selectedDay = new Date();
    self.renderCalendar(false, true);
  }

  self.nextSelectedDay = function(){
    self.selectedDay.setDate(self.selectedDay.getDate()+20);
    self.renderCalendar(false, true);
    self.datepickerDate = null;
  }

  self.prevSelectedDay = function(){
    self.selectedDay.setDate(self.selectedDay.getDate()-20);
    self.renderCalendar(false, true);
    self.datepickerDate = null;
  }

  self.selectDate = function(){
    self.selectedDay = new Date(self.datepickerDate);
    self.renderCalendar(true, false);
  }

  self.calendarButtonsDisplayed = false;
  
  self.hideCalendarButtons = function(){
    self.calendarButtonsDisplayed = false;
    window.setTimeout(function(){$scope.$apply()}, 200);
  }

  self.displayCalendarButtons = function(){
    self.calendarButtonsDisplayed = true;
    window.setTimeout(function(){$scope.$apply()}, 200);
  }

  self.renderCalendar = function(showCurrentDate, showToday){
    var selectedDay = self.selectedDay;
    var fromRequest = new Date(selectedDay.getFullYear(), selectedDay.getMonth(), selectedDay.getDate() - offsetDayBefore);
    var toRequest = new Date(fromRequest.getFullYear(), fromRequest.getMonth(), fromRequest.getDate()+40);//+30 => 31 days.
    self.hideCalendarButtons();
    
    p = PropertyManagerService.getAllAnnouncementReservations({
      from: Day.prototype.toISONoTz(fromRequest),//"2015-11-01",
      to: Day.prototype.toISONoTz(toRequest),//"2015-11-30",
      includeEnquiry: 1,
      includePortal: 1
    });

    p.success(function(data){
      var announcementsReservations, announcementReservations,
          announcement, reservations, lines, calendar;
      lines = [];

      calendar = {
        aroundSelectedDate: selectedDay,//'2015-12-20',//today, //instead startFrom: 'yyyy-mm-dd',
        beforeSelectedDate: offsetDayBefore,
        highLightSelectedDate: true,
        fixedHeader:true,

        cells: 40,
        showWeekends: true,
        showToday: showToday,
        showCurrentDate: showCurrentDate,
        
        label: "General",
        lines: [],
        onDateClick: self.onDateClick,
        onAfterRender: self.displayCalendarButtons
      };

      if(data.success){
        announcementsReservations = data.result;

        //var i = 1;
        //var pend = (self.page * self.pageSize);
        //var pstart = pend - (self.pageSize);
        
        for(ar in announcementsReservations){
          //if(i < pstart) continue;
          //if(i > pend) break;
          announcementReservations = announcementsReservations[ar];
          announcement = announcementReservations["listing"];
          title = announcementReservations["title"];
          reservations = announcementReservations["reservations"];
          calendar.lines.push({
            label: title,
            ranges: reservations,
            customFields: {
              propertyId: announcement
            }
          });
          //i++;
        }

        delete window.c1b;
        
        window.c1b = new Calendar("calendar-around-current-date", calendar);
      }
    });
  }
  
  self.initialCalendar();
  
  self.onDateClick = function(reservationDetail, line, calendar){
    self.reservationDetail = reservationDetail;
    self.currentLine = line;

    $mdDialog.show({
      clickOutsideToClose: false,
      escapeToClose: false,
      scope: $scope,        // use parent scope in template
      preserveScope: true,  // do not forget this if use parent scope
      // Since GreetingController is instantiated with ControllerAs syntax
      // AND we are passing the parent '$scope' to the dialog, we MUST
      // use 'vm.<xxx>' in the template markup
      template: $templateCache.get("AdminModal.html"),
      controller: function AdminDialogController($scope, $mdDialog) {
        $scope.waitingForSave = false;
        $scope.line = self.currentLine = line;

        $scope.reservationStatuses = JSON.parse($("meta[name=reservationStatuses]").prop("content"));

        self.reservationDetail.description = self.reservationDetail.description || "";
        $scope.reservationDetail = angular.copy(self.reservationDetail);

        if(typeof $scope.reservationDetail.propertyTitle == "undefined")
          $scope.reservationDetail.propertyTitle = line.label;
        else
          $scope.reservationDetail.propertyTitle = decodeURIComponent($scope.reservationDetail.propertyTitle);
        
        $scope.reservationDetail.description = decodeURIComponent($scope.reservationDetail.description);
        
        if(typeof $scope.reservationDetail.note == "undefined") $scope.reservationDetail.note = "";
        $scope.reservationDetail.note = decodeURIComponent($scope.reservationDetail.note.replace("%", "").replace("\n", "%0A").replace("\t", "%20"));

        $sce.trustAsHtml($scope.reservationDetail.propertyTitle);
        $sce.trustAsHtml($scope.reservationDetail.description);
        //$scope.reservationDetail.note;
        
        $scope.propertyTitle = $scope.reservationDetail.propertyTitle;
        $sce.trustAsHtml($scope.propertyTitle);
        
        if(!$scope.reservationDetail.resGuestInfos) $scope.reservationDetail.resGuestInfos = {};
        if(!$scope.reservationDetail.guest) $scope.reservationDetail.guest = {};
        
        var zeroValueIfEmpty = [
          "resGuestInfos.adultsNumber", "resGuestInfos.childsNumber", "resGuestInfos.babiesNumber",
          "price", "cleaningFee", "channelCommission", "touristTax"
        ];

        var emptyStringValueIfEmpty = [
          "rrNote", "cfNote", "ccNote", "ttNote",
          "guest.name", "guest.surname", "guest.telephone", "guest.email"
        ];

        for(val in zeroValueIfEmpty){
          var value = zeroValueIfEmpty[val];
          var variable = "$scope.reservationDetail."+value;
          eval("if(" + variable + " == '' || typeof " + variable + " == 'undefined' || " + variable + " == null) " + variable + " = '0';");
        }

        for(val in emptyStringValueIfEmpty){
          var value = emptyStringValueIfEmpty[val];
          var variable = "$scope.reservationDetail."+value;
          eval("if(" + variable + " == '' || typeof " + variable + " == 'undefined' || " + variable + " == null) " + variable + " = '';");
        }

        /*if($scope.reservationDetail.resGuestInfos.adultsNumber == "" || 
           typeof $scope.reservationDetail.resGuestInfos.adultsNumber === "undefined"){
          $scope.reservationDetail.resGuestInfos.adultsNumber = "0";
        }

        if($scope.reservationDetail.resGuestInfos.childsNumber == "" || 
           typeof $scope.reservationDetail.resGuestInfos.childsNumber === "undefined"){
          $scope.reservationDetail.resGuestInfos.childsNumber = "0";
        }

        if($scope.reservationDetail.resGuestInfos.babiesNumber == "" || 
           typeof $scope.reservationDetail.resGuestInfos.babiesNumber === "undefined"){
          $scope.reservationDetail.resGuestInfos.babiesNumber = "0";
        }

        if($scope.reservationDetail.price == "" || 
           typeof $scope.reservationDetail.price === "undefined"){
          $scope.reservationDetail.price = "0";
        }
        
        if($scope.reservationDetail.cleaningFee == "" || 
           typeof $scope.reservationDetail.cleaningFee === "undefined"){
          $scope.reservationDetail.cleaningFee = "0";
        }
        
        if($scope.reservationDetail.channelCommission == "" || 
           typeof $scope.reservationDetail.channelCommission === "undefined"){
          $scope.reservationDetail.channelCommission = "0";
        }
        
        if($scope.reservationDetail.touristTax == "" || 
           typeof $scope.reservationDetail.touristTax === "undefined"){
          $scope.reservationDetail.touristTax = "0";
        }*/
        
        if(!self.reservationDetail.checkinTime){
          $scope.reservationDetail.checkinTime = "";
        }else{
          date = new Date(); t = self.reservationDetail.checkinTime;
          date.setHours(parseInt(t.substring(0, t.indexOf(":")))); 
          date.setMinutes(parseInt(t.substring(t.indexOf(":")+1)));
          date.setSeconds(0);
          $scope.reservationDetail.checkinTime = date;
        }

        if(!self.reservationDetail.checkoutTime){
          $scope.reservationDetail.checkoutTime = "";
        }else{
          date = new Date(); t = self.reservationDetail.checkoutTime;
          date.setHours(parseInt(t.substring(0, t.indexOf(":")))); 
          date.setMinutes(parseInt(t.substring(t.indexOf(":")+1)));
          date.setSeconds(0);
          $scope.reservationDetail.checkoutTime = date;
        }
        
        $scope.reservationDetail.checkin = self.reservationDetail.checkin ? new Date(self.reservationDetail.checkin) : "";
        $scope.reservationDetail.checkout = self.reservationDetail.checkout ? new Date(self.reservationDetail.checkout) : "";
        
        $scope.reservationDetail.id_reservationStatus = -1;
        if(typeof self.reservationDetail.id_reservationStatus !== "undefined")
          $scope.reservationDetail.id_reservationStatus = parseInt(self.reservationDetail.id_reservationStatus);
        
        $scope.hasDescription = $scope.reservationDetail.description.length > 0;
        $scope.hasPrice = parseInt($scope.reservationDetail.price) > 0;
        
        $scope.convertParameters = function(){
          var checkinTime, checkoutTime, isEmptyCheckin, isEmptyCheckout, params = angular.copy($scope.reservationDetail);
          /*check raw values*/
          var isEmptyCheckin = $(".checkin-container input")[0].value == "";
          var isEmptyCheckinTime = $(".checkin-time-container input")[0].value == "";
          var isEmptyCheckout = $(".checkout-container input")[0].value == "";
          var isEmptyCheckoutTime = $(".checkout-time-container input")[0].value == "";
          
          if(!isEmptyCheckinTime){
            checkinHours = params.checkinTime.getHours() < 10 ? "0" + params.checkinTime.getHours() : "" + params.checkinTime.getHours();
            checkinMinutes = params.checkinTime.getMinutes() < 10 ? "0" + params.checkinTime.getMinutes() : "" + params.checkinTime.getMinutes();
            checkinTime = checkinHours + ":" + checkinMinutes;
            params.checkinTime = checkinTime;
          }else params.checkinTime = "";
          
          if(!isEmptyCheckoutTime){
            checkoutHours = params.checkoutTime.getHours() < 10 ? "0" + params.checkoutTime.getHours() : "" + params.checkoutTime.getHours();
            checkoutMinutes = params.checkoutTime.getMinutes() < 10 ? "0" + params.checkoutTime.getMinutes() : "" + params.checkoutTime.getMinutes();
            checkoutTime = checkoutHours + ":" + checkoutMinutes;
            params.checkoutTime = checkoutTime;
          }else params.checkoutTime = "";

          params.checkin = !isEmptyCheckin ? Day.prototype.toISONoTz(params.checkin) : "";
          params.checkout = !isEmptyCheckout ? Day.prototype.toISONoTz(params.checkout) : "";

          return [params, $scope.reservationDetail];
        }
        
        $scope.leftPad = function leftPad(number, targetLength) {
          var output = number + '';
          while (output.length < targetLength) {
            output = '0' + output;
          }
          return output;
        }
        
        $scope.enablePricingUpdates = false;
        $scope.pricing = function(){
          $scope.enablePricingUpdates = !$scope.enablePricingUpdates;
        }
        
        $scope.saveReservation = function(){
          /*console.log($scope.reservationDetail);
          $mdDialog.hide();*/
          var toSend = angular.copy($scope.reservationDetail);

          toSend.enablePricingUpdates = $scope.enablePricingUpdates ? 1 : 0;
          if(typeof toSend.propertyId === "undefined")
            toSend.propertyId = $scope.line.customFields.propertyId;

          if($(".checkin-container input")[0].value == "")
            toSend.checkin = "";
          else
            toSend.checkin = Day.prototype.toISONoTz(toSend.checkin);

          if($(".checkout-container input")[0].value == "")
            toSend.checkout = "";
          else
            toSend.checkout = Day.prototype.toISONoTz(toSend.checkout);

          if($(".checkin-time-container input")[0].value == "")
            toSend.checkinTime = "";
          else
            toSend.checkinTime = $scope.leftPad(toSend.checkinTime.getHours(), 2) + ":" + $scope.leftPad(toSend.checkinTime.getMinutes(), 2) + ":00";

          if($(".checkout-time-container input")[0].value == "")
            toSend.checkoutTime = "";
          else
            toSend.checkoutTime = $scope.leftPad(toSend.checkoutTime.getHours(), 2) + ":" + $scope.leftPad(toSend.checkoutTime.getMinutes(), 2) + ":00";

          //console.log($scope.reservationDetail);
          //console.log(toSend);

          $scope.waitingForSave = true;
          p = PropertyManagerService.save(toSend);
          p.success(function(data){
            //console.log(data);
            $scope.waitingForSave = false;
            if(!data.success){
              var f = "field";
              if(data.errors && data.errors.length > 1)
                f = "fields"
              if(data.errorMessage) f += "\n" + data.errorMessage;
              
              alert("Error in " + data.errors.toString() + " " + f);
              $mdDialog.hide();
              location.reload();
            }else{
              $mdDialog.hide();
              self.refreshCalendar();
            }
          });
        }

        $scope.closeDialog = function(){
          $mdDialog.hide();
        }
        
        /* 
          Marco Distrutti bugfix.
          Some Chrome versions have a bug in angular material design md-datepicker (inside a popup)
          It doesn't add the md-input-has-value if the initial date is valid.
          Sometimes the envirioment doesn't add this class when from empty value is selected a valid date from the popup
        */
        
        $scope.fixDatePickerContent = function(){
          var checkin = self.reservationDetail.checkin;
          var checkout = self.reservationDetail.checkout;
          if(!(Object.prototype.toString.call(checkin) === "[object Date]" && isNaN( checkin.getTime() )) && 
             !$(".checkin-container").hasClass("md-input-has-value"))
            $(".checkin-container").addClass("md-input-has-value");
          if(!(Object.prototype.toString.call(checkout) === "[object Date]" && isNaN( checkout.getTime() )) && 
             !$(".checkout-container").hasClass("md-input-has-value"))
            $(".checkout-container").addClass("md-input-has-value");          
        }
        
        $scope.checkinChanged = function(){$scope.fixDatePickerContent();}
        $scope.checkoutChanged = function(){$scope.fixDatePickerContent();}
        
        window.setTimeout($scope.fixDatePickerContent, 600);
      }
    });
  }
  
}]);