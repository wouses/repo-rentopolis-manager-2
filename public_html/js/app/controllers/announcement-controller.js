app.controller('AnnouncementController', 
                   ['$scope', '$mdDialog', '$mdToast', '$templateCache', '$window', 'AnnouncementsService', 'GMapService', 
    function($scope, $mdDialog, $mdToast, $templateCache, $window, AnnouncementsService, GMapService) {
      var self = this, p;
      $scope.message = 'Announcement management page...work in progress';
      
      self.employee = "Marco";
      self.map = GMapService.map;
      self.markers = [];
      self.fullAddress = "";
      self.formBuilt = [];//[{label:"", description: "", name:""}];
      self.formBuiltDeleted = [];
      
      pSettings = AnnouncementsService.getSettingFields({});
      pSettings.success(function(data){
        if(data.success){
          var result = data.result;
          self.formBuilt = result;
          self.scrollBottomContent();
        }
      });

      self.addFieldInBuiltForm = function(){
        self.formBuilt.push({label:"", description: "", name:""});
        self.scrollBottomContent();
      }

      self.scrollBottomContent = function(){
        window.setTimeout(function(){
          /*scroll bottom*/
          var id = $("#content md-content:first-child");
          id.scrollTop(id[0].scrollHeight);
        }, 100);
      }

      self.removeFieldFromBuiltForm = function(index){
        var element = self.formBuilt.splice(index, 1)[0];
        self.formBuiltDeleted.push(element);
      }

      self.addFieldFromBuiltForm = function(index, flag){
        self.formBuilt.splice(index+1, 0, {label:"", description: "", name:""});
        if(flag){
          /*scroll bottom*/
          var id = $("#content md-content:first-child");
          id.scrollTop(id[0].scrollHeight);
        }
      }
      
      self.saveLayoutBuiltForm = function(){
        var saveLayout = AnnouncementsService.setSettingFields({
          "formLayout": JSON.stringify(self.formBuilt),
          "toCancel": JSON.stringify(self.formBuiltDeleted)
        });
        self.waitingForSave = true;
        saveLayout.success(function(data){
          //console.log(data);
          self.waitingForSave = false;
          if(data) location.reload();
        });
      }

      centerMilan();

      self.loadingAnnouncements = true;
      p = AnnouncementsService.getProperties({});
      p.success(function(data){
        self.properties = data.result;
        self.loadingAnnouncements = false;
        for(p in self.properties){
          property = self.properties[p];
          setMarker(property.id_announcement, {lat: property.address.lat, lng: property.address.lng}, property.title);
          //if(property.address.lat != undefined)
            /*markers.push({
              id: property.id_announcement,
              coords:{
                latitude: property.address.lat, 
                longitude: property.address.lng
              },
              options: { draggable: false  }
            });*/
        }
      });
      
      offset = 240;
      
      angular.element($window).bind('resize', function() {
        GMapService.resizeMap(getHeightOfMap());
        centerMilan();
      });

      function setMarker(announcementId, latLng, title, searchIcon){
        found = false;
        for(m in self.markers){
          marker = self.markers[m];
          if(marker.id)
            if(marker.id == announcementId){
              marker.coords.latitude = latLng.lat;
              marker.coords.longitude = latLng.lng; 
              found = true;
              break;
            }
        }
        
        if(!found){
          createNewMarker(announcementId, latLng, title, searchIcon);
        }
        /*$scope.$apply();*/
      }
      
      function createNewMarker(announcementId, latLng, title, searchIcon){
        html = (title != undefined && title != null ? title : "");
        i = findMarkerByCoords(latLng.lat, latLng.lng);
        if(i > -1){
          self.markers[i].title += "<br/>"+html;
          self.markers[i].aggregation += 1;
          self.markers[i].icon = "/images/icons/house-green-20.png";
        }else{
          markerObj = {
            id: announcementId,
            icon: (searchIcon == undefined ? "https://cdn0.iconfinder.com/data/icons/my-house-1/512/011-house-20.png" : searchIcon),
            coords:{
              latitude: latLng.lat, 
              longitude: latLng.lng
            },
            title: html,
            aggragation: 1,
            show: false,
            options: { draggable: false  },
            onClick: function(){
              marker = arguments[2];
              marker.show = !marker.show;
            }
          };
          if(title == undefined || title == null) delete markerObj.html;
          self.markers.push(markerObj);
        }
      }

      function removeMarker(announcementId){
        var markers = self.markers;
        for(m in markers){
          marker = markers[m];
          if(marker.id == announcementId){
            markers.splice(m, 1);
            break;
          }
        }
        console.log(markers);
      }
      
      /* 
       * Actual algorithm complexity: n(n+1)/2
       * Where n is the total apartment number. With 100 we obtain 5050 
       * comparisons in the wrost case.
       */
      function findMarkerByCoords(lat, lng){
        var markers = self.markers;
        for(m in markers){
          marker = markers[m];
          if(marker.coords.latitude == lat && marker.coords.longitude == lng){
            return m;
          }
        }
        return -1;
      }
      
      function getHeightOfMap(){
        return $(window).height() - offset;
      }
      
      function centerMilan(){
        GMapService.setCenter({lat:45.465422, lon:9.185924}, 12);
      }
      
      window.setTimeout(function(){
        GMapService.resizeMap(getHeightOfMap());
        google.maps.event.trigger(GMapService.map.control.getGMap(), 'resize');
      }, 1000);
      
      self.saveAddress = function(obj){
        var p, json, announcement = self.announcement;
        json = {
          "street": announcement.address.street,
          "houseNumber": announcement.address.houseNumber,
          "city": announcement.address.city,
          "state": announcement.address.state,
          "zip": announcement.address.zip,
          "country": announcement.address.country,
          "directions": announcement.address.directions
        };
        obj.loadingAddress = true;
        p = AnnouncementsService.updateAddress(announcement.id_announcement, json);
        p.success(function(data){
          if(data.success)
            setMarker(announcement.id_announcement, {lat: data.result.lat, lng: data.result.lng}, announcement.title);
          obj.loadingAddress = false;
          $scope.toastPosition = angular.extend({},{bottom: false, top: true, left: true, right: false});
          $mdToast.show(
            $mdToast.simple()
            .content('Address saved!')
            .position(
              Object.keys($scope.toastPosition).filter(function(pos) { return $scope.toastPosition[pos]; }).join(' ')
            )
            .hideDelay(2000)
          );
        });
      }
      
      self.getFormattedAddress = function(property){
        var address = property.address;
        if(address.street != "")
            return address.street + " " + address.houseNumber + ", " + address.zip + " " + address.city;
        return "";
      }
      
      var firstMapTabSelection = false;
      self.mapTabSelected = function(){
        if(!firstMapTabSelection){
          searchIcon = "https://cdn1.iconfinder.com/data/icons/hawcons/32/698838-icon-111-search-32.png";
          window.setTimeout(function(){
            searchAddressInput = document.getElementById("searchAnAddress");
            autocompleteSearchInput = new google.maps.places.Autocomplete( searchAddressInput, { types: ['geocode'] });
            searchAddressInput.setAttribute("placeholder", "");
            autocompleteSearchInput.addListener('place_changed', function() {
              var place = autocompleteSearchInput.getPlace();
              var zoom = (place.types.indexOf("route") || place.types.indexOf("route") > -1 ? 15 : 12);
              var coords = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};

              setMarker(-1, coords, null, searchIcon); //use the announcement list -1 code to represent the search behaviour
              GMapService.setCenter({lat: coords.lat, lon: coords.lng}, zoom);
              $scope.$apply();
            });

            /*$(searchAddressInput).keypress(function(e) {
              if (e.which == 13) {
                $("#uhm").trigger("focus");
              }
            });*/
            
          }, 500);
          
          firstMapTabSelection = true;
        }
      }
      
      self.resetFullAddress = function(){
        self.fullAddress = "";
        centerMilan();
        removeMarker(-1);
      }
      
      self.channelManagersConfiguration = function(announcement){
        var p = AnnouncementsService.getChannelManagersConfiguration(announcement.id_announcement, {});
        p.success(function(data){
          var configuration = data["result"];
          self.showChannelManagersModal(announcement, configuration);
        });
      }
      
      self.announcementSettings = function(announcement){
        var p = AnnouncementsService.getAnnouncementSettings(announcement.id_announcement, {});
        p.success(function(data){
          var result = data["result"];
          self.showAnnouncementSettingsModal(announcement, result);
        });
      }
      
      self.announcementChannelsSettings = function(announcement){
        var p = AnnouncementsService.getAnnouncementChannelRules(announcement.id_announcement, {});
        p.success(function(data){
          var result = data["result"]["rules"];
          self.showAnnouncementChannelSettingsModal(announcement, result);
        });
      }
      
      self.showAnnouncementChannelSettingsModal = function(announcement, announcementChannelSettings){
        self.announcement = announcement;
        self.announcementChannelSettings = announcementChannelSettings;

        $mdDialog.show({
          clickOutsideToClose: false,
          scope: $scope,        // use parent scope in template
          preserveScope: true,  // do not forget this if use parent scope
          // Since GreetingController is instantiated with ControllerAs syntax
          // AND we are passing the parent '$scope' to the dialog, we MUST
          // use 'vm.<xxx>' in the template markup
          template: $templateCache.get("AnnouncementChannelSettingsModal.html"),
          controller: function AnnouncementChannelSettingsDialogController($scope, $mdDialog) {
            $scope.announcement = self.announcement;
            $scope.announcementChannelSettings = self.announcementChannelSettings;

            $scope.announcement.isNewRule = false;
            $scope.faseAdd = true;
            
            $scope.saveRule = function(){
              var p = AnnouncementsService.saveAnnouncementChannelSettings($scope.announcement.id_announcement, {
                newRule:JSON.stringify($scope.announcement.newRule)
              });
              p.success(function(data){
                if(data.success){
                  $mdDialog.hide();
                }
              });
            }
            
            $scope.deleteLastRule = function(){
              var p = AnnouncementsService.deleteLastAnnouncementChannelSettings($scope.announcement.id_announcement, {});
              p.success(function(data){
                if(data.success)
                  $mdDialog.hide();
              });
            }
            
            $scope.addRule = function(){
              $scope.announcement.isNewRule = true;
              $scope.faseAdd = false;
              $scope.announcement.newRule = {cleaning: '', expediaCleaning: '', bookingcomCommission:'', validFrom: ''};
              //$mdDialog.hide();
            }
            
            $scope.closeDialog = function(){
              $mdDialog.hide();
            }
            
          }});
      }

      self.showAnnouncementSettingsModal = function(announcement, fields){
        self.announcement = announcement;
        self.settingsFields = fields;

        $mdDialog.show({
          clickOutsideToClose: false,
          scope: $scope,        // use parent scope in template
          preserveScope: true,  // do not forget this if use parent scope
          // Since GreetingController is instantiated with ControllerAs syntax
          // AND we are passing the parent '$scope' to the dialog, we MUST
          // use 'vm.<xxx>' in the template markup
          template: $templateCache.get("AnnouncementSettingsModal.html"),
          controller: function AdminDialogController($scope, $mdDialog) {
            $scope.fields = self.settingsFields;
            $scope.announcement = self.announcement;

            $scope.updateSettings = function(){
              var p, configutation;
              json = {"settings": JSON.stringify($scope.fields)};
              console.log(json);
              p = AnnouncementsService.updateAnnouncementSettings($scope.announcement.id_announcement, json);
              p.success(function(data){
                console.log(data.result);
                $scope.closeDialog();
              });
            }

            $scope.closeDialog = function(){
              $mdDialog.hide();
            }
          }});
      }

      self.showChannelManagersModal = function(announcement, configuration){
        self.announcement = announcement;
        self.channelManagerConfiguration = configuration;

        $mdDialog.show({
          clickOutsideToClose: false,
          scope: $scope,        // use parent scope in template
          preserveScope: true,  // do not forget this if use parent scope
          // Since GreetingController is instantiated with ControllerAs syntax
          // AND we are passing the parent '$scope' to the dialog, we MUST
          // use 'vm.<xxx>' in the template markup
          template: $templateCache.get("ChannelManagersModal.html"),
          controller: function AdminDialogController($scope, $mdDialog) {
            $scope.announcement = self.announcement;
            $scope.channelManagers = self.channelManagerConfiguration;

            $scope.updateConfiguration = function(){
              var p, configutation;
              json = {"configuration": JSON.stringify($scope.channelManagers)};
              p = AnnouncementsService.updateChannelManagersConfiguration($scope.announcement.id_announcement, json);
              p.success(function(data){
                console.log(data.result);
                $scope.closeDialog();
              });
            }

            $scope.closeDialog = function(){
              $mdDialog.hide();
            }
          }});
        console.log(announcement);
      }

      self.showAddressModal = function(announcement){
        self.announcement = announcement;
        $mdDialog.show({
          clickOutsideToClose: false,
          scope: $scope,        // use parent scope in template
          preserveScope: true,  // do not forget this if use parent scope
          // Since GreetingController is instantiated with ControllerAs syntax
          // AND we are passing the parent '$scope' to the dialog, we MUST
          // use 'vm.<xxx>' in the template markup
          template: 
          '<md-dialog>' +
          '  <md-dialog-content>' +
          '    <h3>{{announcement.title}}</h3>' +
          '    <h4>Address Detail</h4>' +
          '    <form>' + 
          '      <div layout layout-sm="column">' + 
          '        <md-input-container flex>' +
          '           <label>Route</label>' +
          '           <input ng-model="announcement.address.street" id="route" type="text" placeholder="">' +
          '         </md-input-container>' +
          '         <md-input-container flex>' +
          '           <label>House number</label>' +
          '           <input ng-model="announcement.address.houseNumber" type="text" id="street_number">' +
          '         </md-input-container>' +
          '      </div>' + 

          '      <div layout layout-sm="column">' + 
          '        <md-input-container flex>' +
          '           <label>City</label>' +
          '           <input ng-model="announcement.address.city" type="text" id="locality">' +
          '         </md-input-container>' +
          '         <md-input-container flex>' +
          '           <label>Zip</label>' +
          '           <input ng-model="announcement.address.zip" type="text" id="postal_code">' +
          '         </md-input-container>' +
          '      </div>' + 

          '      <div layout layout-sm="column">' + 
          '        <md-input-container flex>' +
          '           <label>State/Province</label>' +
          '           <input ng-model="announcement.address.state" type="text" id="administrative_area_level_1">' +
          '         </md-input-container>' +
          '         <md-input-container flex>' +
          '           <label>Country</label>' +
          '           <input ng-model="announcement.address.country" type="text" id="country">' +
          '         </md-input-container>' +
          '      </div>' + 

          '      <div layout layout-sm="column">' + 
          '        <md-input-container flex>' +
          '           <label>Directions</label>' +
          '           <textarea ng-model="announcement.address.directions" rows="2" id="addressDirections"></textarea>' +
          '         </md-input-container>' +
          '      </div>' + 
          
          '    </form>' + 
          '    <div class="md-actions">' +
          '       <md-progress-circular md-mode="indeterminate" md-diameter="20" ng-show="loadingAddress"></md-progress-circular>' +
          '       <md-button ng-click="saveAddress()" class="md-raised md-primary" ng-show="!loadingAddress">' +
          '          Save' +
          '       </md-button>' +
          '       <md-button ng-click="closeDialog()" class="md-raised">' +
          '          Close' +
          '       </md-button>' +
          '    </div>' +
          '  </md-dialog-content>' +
          '</md-dialog>',
          controller: function DialogController($scope, $mdDialog) {
            var autocomplete, input;
            var componentForm = {
              street_number: ['houseNumber', 'short_name'],
              route: ['street', 'long_name'],
              locality: ['city', 'long_name'],
              administrative_area_level_1: ['state', 'short_name'],
              country: ['country', 'long_name'],
              postal_code: ['zip', 'short_name']
            };
            $scope.announcement = self.announcement;

            window.setTimeout(function(){
              input = document.getElementById("route");
              autocomplete = new google.maps.places.Autocomplete( input, { types: ['geocode'] });
              input.setAttribute("placeholder", "");

              autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();

                /*for (var component in componentForm) {
                  document.getElementById(component[1]).value = '';
                  document.getElementById(component[1]).disabled = false;
                }*/

                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                  var addressType = place.address_components[i].types[0];
                  if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType][1]];
                    self.announcement.address[componentForm[addressType][0]] = val;
                  }
                }

                $(input).trigger("blur");
              });
            }, 300);

            $scope.saveAddress = function(){
              self.saveAddress($scope);
              $scope.closeDialog();
            }

            $scope.closeDialog = function(){
              $mdDialog.hide();
            }
          }
        });
      }
      //self.properties = [{"name": "Ciao 1"}, {"name": "Ciao 2"}, {"name": "Ciao 3"}, {"name": "Ciao 4"}];
    }]);